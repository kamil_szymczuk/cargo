package pl.ihh.cargo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by kuta on 21.02.2016.
 */
@Controller
public class HomeViewController {

    @RequestMapping("/")
    public @ResponseBody
    ModelAndView home() {

        ModelAndView model = new ModelAndView("/home");

        return model;
    }
}
