package pl.ihh.cargo;


import org.springframework.data.domain.Page;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.ihh.cargo.domain.PageRequestDto;

import javax.validation.Valid;

/**
 * Created by kuta on 20.02.2016.
 */

public interface BaseController<T, DTO, ID> {

    String PATH_LIST = "list";
    String PATH_DETAILS = "details";

    BaseService getService();

    String getResourceListPath();

    String getResourceDetailsPath();

    ModelAndView list();

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    ModelAndView details(@PathVariable("id") ID id);

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    ModelAndView edit(@PathVariable("id") ID id);

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    String edit(@Valid  T object, BindingResult result, ModelMap model);

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    ModelAndView add();

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    String add(@Valid @ModelAttribute T object, BindingResult result, ModelMap model);

    @RequestMapping(value = "/{id}/remove", method = RequestMethod.GET)
    String remove(@PathVariable("id") ID id);

    @RequestMapping(value = "/api/{id}", method = RequestMethod.GET)
    @ResponseBody
    DTO getOne(@PathVariable ID id);

    @RequestMapping(value = "/api/list", method = RequestMethod.POST)
    @ResponseBody
    Page<DTO> getPage(PageRequestDto pageRequest);
}
