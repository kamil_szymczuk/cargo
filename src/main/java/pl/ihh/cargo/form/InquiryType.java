package pl.ihh.cargo.form;

public enum InquiryType {
	comment, feedback, suggestion
}