/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.ihh.cargo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import pl.ihh.cargo.domain.lanes.Lane;

import java.util.List;

public interface BaseService<T, ID> {

	Page<T> findAll(Pageable pageable);

	Page<T> findAll(Pageable pageable, String search);

	List<T> findAll();

	List<T> findAll(String search);

	List<T> findAll(Specification<T> specification);

	T findById(ID id);

	T save(T object);

	void remove(T object);

	void removeById(ID id);

	Pageable createPageRequest(int page, int size, Sort.Direction direction, String... properties) ;

}
