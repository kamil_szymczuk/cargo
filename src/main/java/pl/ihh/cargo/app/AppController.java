package pl.ihh.cargo.app;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by kuta on 19.03.2016.
 */
@Slf4j
@Controller
@RequestMapping("/")
public class AppController {

    @Autowired(required = false)
    @Qualifier("authenticationManager")
    public AuthenticationManager authenticationManager;

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public ModelAndView login() {
        ModelAndView model = new ModelAndView("login");
        return model;
    }

    @RequestMapping(value = "login", method = {RequestMethod.POST})
    public ModelAndView authentication(@RequestParam("username") String userName, @RequestParam("password") String password,
                                       @RequestParam(required = false, defaultValue = "0") String timestamp, HttpServletRequest request) {

        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(userName, password);
        try {
            Authentication authentication = authenticationManager.authenticate(authenticationToken);

            SecurityContext securityContext = SecurityContextHolder.getContext();

            securityContext.setAuthentication(authentication);

            HttpSession session = request.getSession(true);
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
            
            return new ModelAndView("forward:/").addObject(timestamp);
        } catch (AuthenticationException ex) {
            return new ModelAndView("forward:/login?error");
        }
    }

    @RequestMapping(value = "403")
    public ModelAndView accessDenied() {
        ModelAndView model = new ModelAndView("views/403");
        return model;
    }
    @RequestMapping(value = "405")
    public ModelAndView notSupported() {
        ModelAndView model = new ModelAndView("views/405");
        return model;
    }
}
