package pl.ihh.cargo.app;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.springframework.stereotype.Component;

import pl.ihh.cargo.domain.TrackedEntity;
import pl.ihh.cargo.users.CustomUserDetailsService;
import pl.ihh.cargo.users.User;

@Component
public class ModificationListener {

    @PrePersist
    public void updateCreationData(TrackedEntity trackedEntity) {
        trackedEntity.setCreatedAt(new Date());
        User user = CustomUserDetailsService.getLoggedUser();
        if (user != null) {
            trackedEntity.setCreatedBy(user);
        }
    }

    @PreUpdate
    public void updateLastModificationData(TrackedEntity trackedEntity) {
        trackedEntity.setUpdatedAt(new Date());
        User user = CustomUserDetailsService.getLoggedUser();
        trackedEntity.setUpdatedBy(user);
    }
}