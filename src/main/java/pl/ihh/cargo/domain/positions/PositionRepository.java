package pl.ihh.cargo.domain.positions;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import javax.persistence.criteria.*;
import java.util.Set;

public interface PositionRepository extends JpaRepository<Position, Integer>, JpaSpecificationExecutor<Position> {

//    class Spec {
//        public static Specification<Position> globalSearch(final String search) {
//            return (root, query, cb) -> {
//                Path<String> namePath = root.get("name");
//                return cb.like(namePath, String.format("%%%s%%", search));
//            };
//        }
//        public static Specification<Position> nameLike(final String search) {
//            return (root, query, cb) -> {
//                Path<String> namePath = root.get("name");
//                return cb.like(namePath, String.format("%%%s%%", search));
//            };
//        }
//    }

    class Spec {
        public static Specification<Position> globalSearch(final String search) {
            return Specifications.where(nameLike(search));//.or(product(search));//.or(productType(search));
        }


        public static Specification<Position> nameLike(final String name) {
            return (root, query, cb) -> {
                Path<String> namePath = root.get("name");
                return cb.like(namePath, String.format("%%%s%%", name));
            };
        }

        public static Specification<Position> product(final String search) {
            return (root, query, cb) -> {
                Path<String> productCodePath = root.get("product").get("subcode");
                Path<String> productPath = root.get("product");
                return
                        cb.and(cb.isNotNull(productPath), cb.like(productCodePath, String.format("%%%s%%", search)));
            };
        }

        public static Specification<Position> product(final Set<Integer> products) {
            return (root, query, cb) -> {
                return root.get("product").get("id").in(products);

            };
        }

        public static Specification<Position> productType(final String search) {
            return (root, query, cb) -> {
                Path<String> productTypeCodePath = root.get("product").get("productType").get("baseCode");
                Path<String> productTypePath = root.get("product").get("productType");
                return cb.like(productTypeCodePath, String.format("%%%s%%", search));
            };
        }

        public static Specification<Position> products(final Set<Integer> productIds) {
            return new Specification<Position>() {
                @Override
                public Predicate toPredicate(Root<Position> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                    return root.get("product").get("id").in(productIds);
                }
            };
        }
    }
}
