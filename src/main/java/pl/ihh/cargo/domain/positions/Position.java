package pl.ihh.cargo.domain.positions;


import pl.ihh.cargo.domain.TrackedEntity;
import pl.ihh.cargo.domain.lanes.Lane;
import pl.ihh.cargo.domain.products.Product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Position extends TrackedEntity {

    @Column(nullable = false)
    private String name;

    @Column
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "lane_id")
    private Lane lane;


    public Position() {
    }

    public Position(Integer id) {
        this.id = id;
    }

    public Position(String name, Lane lane, Product product) {
        super();
        this.name = name;
        this.lane = lane;
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProducts(Product product) {
        this.product = product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Lane getLane() {
        return lane;
    }

    public void setLane(Lane lane) {
        this.lane = lane;
    }

    public String getFullName() {
        return (getLane() != null ? getLane().getName() + " " : "") + getName();
    }

    @Override
    public String toString() {
        return (getLane() != null ? getLane().getName() + " " : "") + getName();
    }
}
