package pl.ihh.cargo.domain.positions;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by kuta on 29.02.2016.
 */

@Getter
@Setter
public class PositionDto {

    private Integer id;
    private String name = "";
    private Integer quantity;
    private Integer productId;
    private String product = "";
    private String productType = "";
    private Integer laneId;
    private String laneName;

    public PositionDto(Position position) {
        this.id = position.getId();
        this.name = position.getName();
        this.quantity = position.getQuantity();
        if (position.getProduct() != null) {
            this.productId = position.getProduct().getId();
            this.productType = position.getProduct().getProductType() != null ? position.getProduct().getProductType().getBaseCode() : null;
            this.product = position.getProduct().getSubcode();
        }
        if (position.getLane() != null) {
            this.laneId = position.getLane().getId();
            this.laneName = position.getLane().getShortName();
        }
    }

    public PositionDto() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Integer getLaneId() {
        return laneId;
    }

    public void setLaneId(Integer laneId) {
        this.laneId = laneId;
    }

    public String getLaneName() {
        return laneName;
    }

    public void setLaneName(String laneName) {
        this.laneName = laneName;
    }

    public String getDisplayName() {
        return (laneName != null ? laneName : "") + " " + name;
    }


}
