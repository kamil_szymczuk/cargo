package pl.ihh.cargo.domain.positions;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.ihh.cargo.BaseController;
import pl.ihh.cargo.domain.PageRequestDto;
import pl.ihh.cargo.domain.lanes.Lane;
import pl.ihh.cargo.domain.lanes.LaneService;
import pl.ihh.cargo.domain.products.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping("/position")
public class PositionController implements BaseController<Position, PositionDto, Integer> {

    private static final String BASE_URL = "/position";
    private static final String RESOURCE_PATH = "positions/";

    //region base methods
    @Autowired
    private PositionService positionService;

    @Autowired
    private LaneService laneService;

    @Autowired
    private ProductService productService;

    @Override
    public PositionService getService() {
        return positionService;
    }

    @Override
    public String getResourceListPath() {
        return RESOURCE_PATH + PATH_LIST;
    }

    @Override
    public String getResourceDetailsPath() {
        return RESOURCE_PATH + PATH_DETAILS + '2';
    }
    //endregion base methods


    @Override
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView list() {

        List positions = new ArrayList<>(getService().findAll(new PageRequest(0, 100), "").getContent());

        ModelAndView model = new ModelAndView(getResourceListPath());
        model.addObject("positions", positions);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView details(@PathVariable("id") Integer id) {

        Position position = getService().findById(id);

        ModelAndView model = new ModelAndView(getResourceDetailsPath());
        model.addObject("position", position);
        model.addObject("readonly", true);

        return model;
    }

    @RequestMapping(value = "api/i", method = RequestMethod.POST)
    public
    @ResponseBody
    PositionDto increment(@RequestBody Sum sum) {

        Position position = getService().findById(sum.id);
        position.setQuantity(position.getQuantity() + sum.getValue());
        getService().save(position);

        return new PositionDto(position);
    }



    @Override
    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("id") Integer id) {

        Position position = getService().findById(id);

        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("position", position);
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/" + id + "/edit");

        return view;
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    public String edit(@ModelAttribute Position position, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "error";
        }

        getService().save(position);

        return "redirect:/position/" + position.getId();
    }

    @Override
    public ModelAndView add() {
        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("position", new Position());
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/add");

        return view;
    }

    @RequestMapping(value = "/add", params = {"lane"}, method = RequestMethod.GET)
    public ModelAndView add(@RequestParam(value = "lane") Lane lane) {
        ModelAndView view = add();
        Position position = (Position)view.getModel().get("position");
        position.setLane(lane);

        return view;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@RequestBody @ModelAttribute @RequestParam Position position, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }

        position = getService().save(position);
        return "redirect:" + BASE_URL + "/" + position.getId();
    }

    @RequestMapping(value = "/api/add", method = RequestMethod.POST)
    public String add(@RequestBody PositionDto dto, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }

        Position position = new Position();
        position.setQuantity(dto.getQuantity());
        position.setName(dto.getName());

        return "redirect:" + BASE_URL + "/" + position.getId();
    }

    @Override
    @Transactional
    @RequestMapping(value = "/{id}/remove", method = RequestMethod.GET)
    public String remove(@PathVariable("id") Integer id) {

        Position position = getService().findById(id);
        getService().remove(position);

        return "redirect:" + BASE_URL + "/";
    }

    @RequestMapping(value = "/api/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    PositionDto getOne(@PathVariable Integer id) {
        Position position = getService().findById(id);
        return new PositionDto(position);
    }

    @RequestMapping(value = "/api/list", method = RequestMethod.POST)
    public
    @ResponseBody
    Page<PositionDto> getPage(@RequestBody PageRequestDto pageRequest) {
        Page<Position> positionsPage = getService().findAll(new PageRequest(pageRequest.getPage(), pageRequest.getSize()), pageRequest.getSearch());
        return positionsPage.map(source -> new PositionDto(source));
    }

    @RequestMapping(value = "/api/save", method = RequestMethod.POST)
    public
    @ResponseBody
    PositionDto save(@RequestBody PositionDto positionDto) {
        Position position = getService().save(dtoToPosition(positionDto));
        return new PositionDto(position);
    }

    private Position dtoToPosition(PositionDto dto) {
        Position position = dto.getId() != null ? getService().findById(dto.getId()) : new Position();
        position.setName(dto.getName());
        position.setQuantity(dto.getQuantity());
        position.setProduct(productService.findById(dto.getProductId()));
        position.setLane(laneService.findById(dto.getLaneId()));

        return position;
    }

    @ResponseBody
    @RequestMapping(value = "/api/find", method = RequestMethod.POST)
    public ItemList find(@RequestParam String term) {
        ItemList result = new ItemList();
        List<Position> types = positionService.findAll(PositionRepository.Spec.nameLike(term));

        for (Position type : types) {
            result.getItemList()
                    .add(new SearchResult(type.getId(), type.toString()));
        }
        return result;
    }

    static class Sum {
        Integer id;
        Integer value;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/api/find/byProducts", method = RequestMethod.POST)
    public ItemList findByProducts(@RequestParam String term) {
        ItemList result = new ItemList();
        Set<Integer> products = productService.findAll(ProductRepository.Spec.globalSearch(term)).stream().map(product -> product.getId()).collect(Collectors.toSet());
        List<Position> types = positionService.findAll(PositionRepository.Spec.products(products));

        for (Position type : types) {
            result.getItemList()
                    .add(new SearchResult(type.getId(), type.toString()));
        }
        return result;
    }

}
