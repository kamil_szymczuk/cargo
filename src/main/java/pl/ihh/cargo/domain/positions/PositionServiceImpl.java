package pl.ihh.cargo.domain.positions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.ihh.cargo.domain.products.ProductRepository;

import java.util.List;


@Component("positionService")
@Transactional
class PositionServiceImpl implements PositionService{

    private final PositionRepository positionRepository;
    private final ProductRepository productRepository;

    @Autowired
    public PositionServiceImpl(PositionRepository positionRepository, ProductRepository productRepository) {
        this.positionRepository = positionRepository;
        this.productRepository = productRepository;
    }

    @Override
    public Page<Position> findAll(Pageable pageable) {
        return this.positionRepository.findAll(pageable);
    }

    @Override
    public Page<Position> findAll(Pageable pageable, String search) {
        return this.positionRepository.findAll(PositionRepository.Spec.globalSearch(search),pageable);
    }

    @Override
    public List<Position> findAll() {
        return this.positionRepository.findAll();
    }

    @Override
    public List<Position> findAll(String search) {
        return this.positionRepository.findAll(PositionRepository.Spec.globalSearch(search));
    }

    @Override
    public List<Position> findAll(Specification<Position> specification) {
        return this.positionRepository.findAll(specification);
    }

    @Override
    public Position findById(Integer id) {
        return this.positionRepository.findOne(id);
    }

    @Override
    public Position save(Position object) {
        return positionRepository.save(object);
    }

    @Override
    public void remove(Position object) {
        positionRepository.delete(object);
    }

    @Override
    public void removeById(Integer id) {
        positionRepository.delete(id);
    }

    @Override
    public Pageable createPageRequest(int page, int size, Sort.Direction direction, String... properties) {
        return new PageRequest(page, size, direction, properties);
    }

}
