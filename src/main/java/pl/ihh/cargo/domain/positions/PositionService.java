package pl.ihh.cargo.domain.positions;

import org.springframework.stereotype.Service;
import pl.ihh.cargo.BaseService;


@Service
public interface PositionService extends BaseService<Position, Integer> {
}
