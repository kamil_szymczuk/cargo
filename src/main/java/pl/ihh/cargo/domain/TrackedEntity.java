package pl.ihh.cargo.domain;

import pl.ihh.cargo.app.ModificationListener;
import pl.ihh.cargo.users.User;

import javax.persistence.*;


import java.util.Date;

/**
 * Created by kuta on 20.02.2016.
 */
@MappedSuperclass
@EntityListeners(value = { ModificationListener.class })
public abstract class TrackedEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	// @Audited
	private Date createdAt;

	@ManyToOne
	// @Audited
	protected User createdBy;

	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	// @Audited
	private Date updatedAt;

	@ManyToOne
	// @Audited
	protected User updatedBy;

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	protected Long getInitialVersion() {
		return Long.valueOf(0L);
	}

//	/**
//	 * Sets createdAt before insert
//	 */
//	@PrePersist
//	public void setCreationDate() {
//		this.createdAt = new Date();
//	}
//
//	/**
//	 * Sets updatedAt before update
//	 */
//	@PreUpdate
//	public void setChangeDate() {
//		this.updatedAt = new Date();
//	}

}