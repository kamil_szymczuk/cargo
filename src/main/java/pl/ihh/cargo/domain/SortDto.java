package pl.ihh.cargo.domain;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

/**
 * Created by kuta on 18.03.2016.
 */
public class SortDto {

    private Sort.Direction direction = Sort.Direction.ASC;
    private String property = "id";

    public SortDto() {
    }

    public Order getAsOrder(){
        return new Order(getDirection(),getProperty());
    }

    public Sort.Direction getDirection() {
        return direction != null ? direction : Sort.Direction.ASC;
    }

    public void setDirection(Sort.Direction direction) {
        this.direction = direction;
    }

    public String getProperty() {
        return property != null ? property : "id";
    }

    public void setProperty(String property) {
        this.property = property;
    }


}
