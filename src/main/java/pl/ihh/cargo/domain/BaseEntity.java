package pl.ihh.cargo.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by kuta on 20.02.2016.
 */

@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    protected Integer id;

    @Column(name = "version")
//    @Version
    private Long version = this.getInitialVersion();

    public Integer getId() {
        return id;
    }

    public Long getVersion() {
        return version;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    protected abstract Long getInitialVersion();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseEntity)) return false;

        BaseEntity that = (BaseEntity) o;

        if (!getId().equals(that.getId())) return false;
        return getVersion().equals(that.getVersion());

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getVersion().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + " [ID=" + id + "]";
    }
}