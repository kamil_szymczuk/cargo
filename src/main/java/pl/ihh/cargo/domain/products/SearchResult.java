package pl.ihh.cargo.domain.products;

/**
 * Created by kuta on 17.03.2016.
 */
public class SearchResult {

    private Integer id;
    private String label;
    private String extra;
    private String details;

    public SearchResult(Integer id, String label, String extra) {
        this.id = id;
        this.label = label;
        this.extra = extra;
        this.details = "";
    }

    public SearchResult(Integer id, String label) {
        this.id = id;
        this.label = label;
        this.details = "";
    }


    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}

