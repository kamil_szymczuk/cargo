/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.ihh.cargo.domain.products;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component("productService")
@Transactional
class ProductServiceImpl implements ProductService {

	private final ProductRepository productRepository;


	@Autowired
	public ProductServiceImpl(@Qualifier("productRepository") ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public Page<Product> findAll(Pageable pageable){
		return this.productRepository.findAll(pageable);
	}

	@Override
	public Page<Product> findAll(Pageable pageable, String search) {
		return this.productRepository.findAll(ProductRepository.Spec.globalSearch(search),pageable);
	}

	@Override
	public List<Product> findAll() {
		return this.productRepository.findAll();
	}

	@Override
	public List<Product> findAll(String search) {
		return this.productRepository.findAll(ProductRepository.Spec.globalSearch(search));
	}

	@Override
	public List<Product> findAll(Specification<Product> specification) {
		return this.productRepository.findAll(specification);
	}

	@Override
	public Product findById(Integer id) {
		return this.productRepository.findOne(id);
	}

	@Override
	public Product save(Product object) {
		return productRepository.save(object);
	}

	@Override
	public void remove(Product object) {

	}

	@Override
	public void removeById(Integer aInteger) {

	}

	@Override
	public Pageable createPageRequest(int page, int size, Sort.Direction direction, String... properties) {
		return new PageRequest(page, size, direction, properties);
	}
	
}