package pl.ihh.cargo.domain.products;

import com.google.common.collect.ImmutableSet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.ihh.cargo.BaseController;
import pl.ihh.cargo.domain.PageRequestDto;
import pl.ihh.cargo.domain.positions.PositionRepository;
import pl.ihh.cargo.domain.positions.PositionService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuta on 20.02.2016.
 */

@Slf4j
@Controller
@RequestMapping("/product")
public class ProductController implements BaseController<Product, ProductDto, Integer> {

    private static final String BASE_URL = "/product";
    private static final String RESOURCE_PATH = "products/";

    //region base methods
    @Autowired
    private ProductService productService;

    @Autowired
    private PositionService positionService;

    @Autowired
    private ProductTypeService productTypeService;

    @Override
    public ProductService getService() {
        return productService;
    }

    @Override
    public String getResourceListPath() {
        return RESOURCE_PATH + PATH_LIST;
    }

    @Override
    public String getResourceDetailsPath() {
        return RESOURCE_PATH + PATH_DETAILS + '2';
    }
    //endregion base methods

    @Override
    @PreAuthorize("hasRole('PRODUCT_LIST')")
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView list() {

        List products = new ArrayList<>(getService().findAll(new PageRequest(0, 100), "").getContent());

        ModelAndView model = new ModelAndView(getResourceListPath());
        model.addObject("products", products);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView details(@PathVariable("id") Integer id) {

        Product product = getService().findById(id);

        ModelAndView model = new ModelAndView(getResourceDetailsPath());
        model.addObject("product", product);
        model.addObject("positions", positionService.findAll(PositionRepository.Spec.product(ImmutableSet.of(id))));
        model.addObject("readonly", true);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("id") Integer id) {

        Product product = getService().findById(id);

        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("product", product);
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/" + id + "/edit");

        return view;
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    public String edit(@ModelAttribute @RequestBody Product product, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "error";
        }
        getService().save(product);

        return "redirect:/product/" + product.getId();
    }


    @RequestMapping(value = "api/edit", method = RequestMethod.POST)
    public String edit(@RequestBody Product product) {

        getService().save(product);

        return "redirect:/product/" + product.getId();
    }


    @Override
    public ModelAndView add() {
        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("product", new Product());
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/add");

        return view;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@RequestBody @ModelAttribute  @RequestParam Product product, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        product = getService().save(product);
        return "redirect:" + BASE_URL + "/" + product.getId();
    }

    @Override
    @Transactional
    @RequestMapping(value = "/{id}/remove", method = RequestMethod.GET)
    public String remove(@PathVariable("id") Integer id) {

        Product product = getService().findById(id);
        getService().remove(product);

        return "redirect:" + BASE_URL + "/";
    }

    @RequestMapping(value = "/api/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    ProductDto getOne(@PathVariable Integer id) {
        Product product = getService().findById(id);
        return new ProductDto(product);
    }


    @RequestMapping(value = "/api/list", method = RequestMethod.POST)
    public
    @ResponseBody
    Page<ProductDto> getPage(@RequestBody PageRequestDto pageRequest) {
        Page<Product> productsPage = getService().findAll(pageRequest.getAsPageRequest(), pageRequest.getSearch());
        return productsPage.map(source -> new ProductDto(source));

    }

    @RequestMapping(value = "/api/save", method = RequestMethod.POST)
    public
    @ResponseBody
    ProductDto save(@RequestBody ProductDto productDto) {
        Product product = getService().save(dtoToProduct(productDto));
        return new ProductDto(product);
    }

    private Product dtoToProduct(ProductDto dto) {
        Product product = dto.getId() != null ? getService().findById(dto.getId()) : new Product();
        product.setSubcode(dto.getSubcode());
        product.setDescription(dto.getDescription());
        product.setWeight(dto.getWeight());
        product.setProductType((dto.getProductType() != null) ? dto.getProductType().toProductType(productTypeService) : null);

        return product;
    }


    @ResponseBody
    @RequestMapping(value = "/api/find", method = RequestMethod.POST)
    public ItemList find(@RequestParam String term) {
        ItemList result = new ItemList();
        List<Product> products = productService.findAll(new PageRequest(0, 30), term).getContent();

        for (Product product : products) {
            result.getItemList()
                    .add(new SearchResult(product.getId(), product.getSubcode(), product.getProductType() != null ? product.getProductType().getName() : null));
        }
        return result;
    }


}