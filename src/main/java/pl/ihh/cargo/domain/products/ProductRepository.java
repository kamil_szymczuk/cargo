/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.ihh.cargo.domain.products;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.Path;
import java.util.Collection;

public interface ProductRepository extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {

    class Spec {
        public static Specification<Product> globalSearch(final String search) {
            return (root, query, cb) -> {
                Path<String> subCodePath = root.get("subcode");
                Path<String> descriptionPath = root.get("description");
                return cb.or(
                        cb.like(subCodePath, String.format("%%%s%%", search)),
                        cb.like(descriptionPath, String.format("%%%s%%", search))
                );
            };
        }
        public static Specification<Product> nameLike(final String search) {
            return (root, query, cb) -> {
                Path<String> namePath = root.get("subcode");
                return cb.like(namePath, String.format("%%%s%%", search));
            };
        }
        public static Specification<Product> descriptionLike(final String search) {
            return (root, query, cb) -> {
                Path<String> namePath = root.get("description");
                return cb.like(namePath, String.format("%%%s%%", search));
            };
        }
    }
}