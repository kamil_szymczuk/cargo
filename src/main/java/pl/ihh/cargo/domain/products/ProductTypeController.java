package pl.ihh.cargo.domain.products;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.ihh.cargo.BaseController;
import pl.ihh.cargo.domain.PageRequestDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuta on 20.02.2016.
 */

@Slf4j
@Controller
@RequestMapping("/productType")
public class ProductTypeController implements BaseController<ProductType, ProductTypeDto, Integer> {

    private static final String BASE_URL = "/productType";
//    private static final String RESOURCE_PATH = "products/";

    //region base methods
    @Autowired
    private ProductTypeService productTypeService;

    @Override
    public ProductTypeService getService() {
        return productTypeService;
    }

    @Override
    public String getResourceListPath() {
        return null;//RESOURCE_PATH + PATH_LIST;
    }

    @Override
    public String getResourceDetailsPath() {
        return null;//RESOURCE_PATH + PATH_DETAILS;
    }
    //endregion base methods

    @Override
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView list() {

        List productTypes = new ArrayList<>(getService().findAll(new PageRequest(0, 100)).getContent());

        ModelAndView model = new ModelAndView(getResourceListPath());
        model.addObject("productTypes", productTypes);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView details(@PathVariable("id") Integer id) {

        ProductType productType = getService().findById(id);

        ModelAndView model = new ModelAndView(getResourceDetailsPath());
        model.addObject("productType", productType);
        model.addObject("readonly", true);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("id") Integer id) {

        ProductType productType = getService().findById(id);

        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("productType", productType);
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/" + id + "/edit");

        return view;
    }

    @RequestMapping(value = "/api/{id}/edit", method = RequestMethod.POST)
    public String edit(@RequestBody ProductType productType, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "error";
        }

        ProductType productTypeDb = getService().findById(productType.getId());
        productTypeDb.setBaseCode(productType.getBaseCode());
        productTypeDb.setName(productType.getName());

        getService().save(productTypeDb);

        return "redirect:/productType/" + productTypeDb.getId();
    }

    @Override
    public ModelAndView add() {
        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("productType", new ProductType());
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/add");

        return view;
    }

    @Override
    public String add(@RequestBody ProductType productType, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        productType = getService().save(productType);

        return "redirect:" + BASE_URL + "/" + productType.getId();
    }

    @Override
    @Transactional
    @RequestMapping(value = "/{id}/remove", method = RequestMethod.GET)
    public String remove(@PathVariable("id") Integer id) {

        ProductType productType = getService().findById(id);
        getService().remove(productType);

        return "redirect:" + BASE_URL + "/";
    }

    @RequestMapping(value = "/api/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    ProductTypeDto getOne(@PathVariable Integer id) {
        ProductType type = getService().findById(id);
        return new ProductTypeDto(type);
    }


    @RequestMapping(value = "/api/list", method = RequestMethod.POST)
    public
    @ResponseBody
    Page<ProductTypeDto> getPage(@RequestBody PageRequestDto pageRequest) {

        Page<ProductType> productsPage = getService().findAll(new PageRequest(pageRequest.getPage(), pageRequest.getSize()), pageRequest.getSearch());
        return productsPage.map(source -> new ProductTypeDto(source));

    }

    @RequestMapping(value = "/api/save", method = RequestMethod.POST)
    public
    @ResponseBody
    ProductTypeDto save(@RequestBody ProductTypeDto productTypeDto) {
        ProductType productType = getService().save(productTypeDto.toProductType(getService()));
        return new ProductTypeDto(productType);
    }

    @ResponseBody
    @RequestMapping(value = "/api/find", method = RequestMethod.POST)
    public ItemList find(@RequestParam String term) {
        ItemList result = new ItemList();
        List<ProductType> types = productTypeService.findAll(ProductTypeRepository.Spec.baseCodeLike(term));

        for (ProductType type : types) {
            result.getItemList()
                    .add(new SearchResult(type.getId(), type.getBaseCode(), type.getName()));
        }
        return result;
    }
}