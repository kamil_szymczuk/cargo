/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.ihh.cargo.domain.products;

import lombok.Getter;
import lombok.Setter;
import pl.ihh.cargo.domain.positions.PositionDto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class ProductTypeDto {

    public enum DATA_TYPE {SIMPLE, FULL, POSITIONS, PRODUCTS}

    private Integer id;
    private String baseCode;
    private String name;

    private List<PositionDto> positions;
    private List<ProductDto> products;

    public ProductTypeDto() {
    }

    public ProductTypeDto(Integer id) {
        this.id = id;
    }

    public ProductTypeDto(ProductType productType) {
        this.id = productType.getId();
        this.baseCode = productType.getBaseCode();
        this.name = productType.getName();
    }

    public ProductType toProductType(ProductTypeService productTypeService) {
        ProductType productType = getId() != null ? productTypeService.findById(getId()) : new ProductType();
        productType.setName(getName());
        productType.setBaseCode(getBaseCode());
        return productType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBaseCode() {
        return baseCode;
    }

    public void setBaseCode(String baseCode) {
        this.baseCode = baseCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PositionDto> getPositions() {
        return positions;
    }

    public void setPositions(List<PositionDto> positions) {
        this.positions = positions;
    }

    public List<ProductDto> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDto> products) {
        this.products = products;
    }
}
