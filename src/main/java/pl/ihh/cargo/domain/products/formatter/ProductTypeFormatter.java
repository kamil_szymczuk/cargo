package pl.ihh.cargo.domain.products.formatter;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.format.Formatter;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import pl.ihh.cargo.domain.products.ProductType;
import pl.ihh.cargo.domain.products.ProductTypeRepository;

import java.io.IOException;
import java.util.Locale;

/**
 * Created by kuta on 14.03.2016.
 */
public class ProductTypeFormatter extends JsonDeserializer<ProductType> implements Formatter<ProductType> {

    @Autowired
    private ProductTypeRepository productTypeRepository;

    @Override
    public String print(ProductType object, Locale locale) {
        return object.getId().toString();
    }

    @Override
    public ProductType parse(String text, Locale locale) throws ParseException {
        return productTypeRepository.findOne(Integer.valueOf(text));
    }

    @Override
    public ProductType deserialize(JsonParser jp, DeserializationContext ctx) throws IOException, JsonProcessingException {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        return jp.getIntValue() > 0 ? productTypeRepository.findOne(jp.getIntValue()) : null;
    }

}
