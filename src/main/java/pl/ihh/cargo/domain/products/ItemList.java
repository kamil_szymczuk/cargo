package pl.ihh.cargo.domain.products;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuta on 17.03.2016.
 */
public class ItemList {
    private List<SearchResult> itemList = new ArrayList<>();

    public List<SearchResult> getItemList() {
        return itemList;
    }

    public void setItemList(List<SearchResult> itemList) {
        this.itemList = itemList;
    }
}
