/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.ihh.cargo.domain.products;

import pl.ihh.cargo.domain.TrackedEntity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Product extends TrackedEntity {


    @ManyToOne
    @JoinColumn(name = "product_type_id")
    private ProductType productType;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false, unique = true)
    private String subcode;

    @Column
    private BigDecimal weight;

    public Product() {
    }

    public Product(Integer id) {
        this.id = id;
    }

    public Product(ProductType productType, String subcode) {
        this.productType = productType;
        this.subcode = subcode;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubcode() {
        return subcode;
    }

    public void setSubcode(String subcode) {
        this.subcode = subcode;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String toString() {
        return (productType != null ? productType.getBaseCode()+" " : "")+ subcode;
    }
}
