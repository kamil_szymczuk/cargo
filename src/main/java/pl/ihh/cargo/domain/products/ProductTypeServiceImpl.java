/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.ihh.cargo.domain.products;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component("productTypeService")
@Transactional
class ProductTypeServiceImpl implements ProductTypeService {

	private final ProductTypeRepository productTypeRepository;


	@Autowired
	public ProductTypeServiceImpl(ProductTypeRepository productTypeRepository) {
		this.productTypeRepository = productTypeRepository;
	}

	@Override
	public Page<ProductType> findAll(Pageable pageable){
		return this.productTypeRepository.findAll(pageable);
	}

	@Override
	public Page<ProductType> findAll(Pageable pageable, String search) {
		return this.productTypeRepository.findAll(ProductTypeRepository.Spec.baseCodeLike(search),pageable);
	}

	@Override
	public List<ProductType> findAll() {
		return this.productTypeRepository.findAll();
	}

	@Override
	public List<ProductType> findAll(String search) {
		return this.productTypeRepository.findAll(ProductTypeRepository.Spec.baseCodeLike(search));
	}

	@Override
	public List<ProductType> findAll(Specification<ProductType> specification) {
		return this.productTypeRepository.findAll(specification);
	}

	@Override
	public ProductType findById(Integer id) {
		return this.productTypeRepository.findOne(id);
	}

	@Override
	public ProductType save(ProductType object) {
		return productTypeRepository.save(object);
	}


	@Override
	public void remove(ProductType object) {

	}

	@Override
	public void removeById(Integer aInteger) {

	}

	@Override
	public Pageable createPageRequest(int page, int size, Sort.Direction direction, String... properties) {
		return new PageRequest(page, size, direction, properties);
	}
	
}
