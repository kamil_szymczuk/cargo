package pl.ihh.cargo.domain.products;

import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Created by risen16 on 02.03.2016.
 */

@Getter
@Setter

public class ProductDto {

    private Integer id;
    private String description;
    private String baseCode;
    private String subcode;
    private BigDecimal weight;
    private ProductTypeDto productType;

    public ProductDto(Product product) {
        this.id = product.getId();
        this.description = product.getDescription();
        this.subcode = product.getSubcode();
        this.weight = product.getWeight();
        if (product.getProductType() != null) {
            this.productType = new ProductTypeDto(product.getProductType());
            this.baseCode = product.getProductType().getBaseCode();
        }
    }
    public ProductDto(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBaseCode() {
        return baseCode;
    }

    public void setBaseCode(String baseCode) {
        this.baseCode = baseCode;
    }

    public String getSubcode() {
        return subcode;
    }

    public void setSubcode(String subcode) {
        this.subcode = subcode;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public ProductTypeDto getProductType() {
        return productType;
    }

    public void setProductType(ProductTypeDto productType) {
        this.productType = productType;
    }
}
