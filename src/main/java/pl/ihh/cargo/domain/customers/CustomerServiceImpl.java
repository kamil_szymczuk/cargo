package pl.ihh.cargo.domain.customers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component("customerService")
@Transactional
class CustomerServiceImpl implements CustomerService {

	private final CustomerRepository customerRepository;

	@Autowired
	public CustomerServiceImpl(@Qualifier("customerRepository") CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	public Page<Customer> findAll(Pageable pageable){
		return this.customerRepository.findAll(pageable);
	}

	@Override
	public Page<Customer> findAll(Pageable pageable, String search) {
		return this.customerRepository.findAll(CustomerRepository.Spec.globalSearch(search),pageable);
	}

	@Override
	public List<Customer> findAll() {
		return this.customerRepository.findAll();
	}

	@Override
	public List<Customer> findAll(String search) {

		return this.customerRepository.findAll(CustomerRepository.Spec.globalSearch(search));
	}

	@Override
	public List<Customer> findAll(Specification<Customer> specification) {

		return this.customerRepository.findAll(specification);
	}

	@Override
	public Customer findById(Integer id){
		return this.customerRepository.findOne(id);
	}

	@Override
	public Customer save(Customer object) {
		return customerRepository.save(object);
	}

	@Override
	public void remove(Customer object) {
		customerRepository.delete(object);
	}

	@Override
	public void removeById(Integer id) {
		customerRepository.delete(id);
	}

	@Override
	public Pageable createPageRequest(int page, int size, Sort.Direction direction, String... properties) {
		return new PageRequest(page, size, direction, properties);
	}


}
