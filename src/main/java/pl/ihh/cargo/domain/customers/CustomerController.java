package pl.ihh.cargo.domain.customers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.ihh.cargo.BaseController;
import pl.ihh.cargo.domain.PageRequestDto;
import pl.ihh.cargo.domain.products.ItemList;
import pl.ihh.cargo.domain.products.SearchResult;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by kuta on 20.02.2016.
 * Customer controller class
 */

@Slf4j
@Controller
@RequestMapping("/customer")
public class CustomerController implements BaseController<Customer, CustomerDto, Integer> {

    private static final String BASE_URL = "/customer";
    private static final String RESOURCE_PATH = "customers/";

    //region base methods
    @Autowired
    private CustomerService customerService;

    @Override
    public CustomerService getService() {
        return customerService;
    }

    @Override
    public String getResourceListPath() {
        return RESOURCE_PATH + PATH_LIST;
    }

    @Override
    public String getResourceDetailsPath() {
        return RESOURCE_PATH + PATH_DETAILS+'2';
    }
    //endregion base methods

    //region methods
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView list() {

        List customers = new ArrayList<>(getService().findAll(new PageRequest(0, 100), "").getContent());

        ModelAndView model = new ModelAndView(getResourceListPath());
        model.addObject("customers", customers);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView details(@PathVariable("id") Integer id) {

        Customer customer = getService().findById(id);

        ModelAndView model = new ModelAndView(getResourceDetailsPath());
        model.addObject("customer", customer);
        model.addObject("readonly", true);

        return model;
    }


    @Override
    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("id") Integer id) {

        Customer customer = getService().findById(id);

        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("customer", customer);
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/" + id + "/edit");

        return view;
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    public String edit(@ModelAttribute Customer customer, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "error";
        }
        getService().save(customer);

        return "redirect:/customer/" + customer.getId();
    }

    @Override
    public ModelAndView add() {
        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("customer", new Customer());
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/add");

        return view;
    }

//    @RequestMapping(value = "/add", method = RequestMethod.POST)
//    public String add(@RequestBody CustomerDto dto, BindingResult result, ModelMap model) {
//        if (result.hasErrors()) {
//            return "error";
//        }
//
//        Customer customer = new Customer();
//        customer.setPhoneNumber(dto.getPhoneNumber());
//        customer.setRegonNumber(dto.getRegonNumber());
//        customer.setNotes(dto.getNotes());
//        customer.setNipNumber(dto.getNipNumber());
//        customer.setShortName(dto.getShortName());
//        customer.setEmail(dto.getEmail());
//        customer.setFullName(dto.getFullName());
//
//        customer = getService().save(customer);
//        return "redirect:" + BASE_URL + "/" + customer.getId();
//    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@ModelAttribute Customer dto, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }

        Customer customer = new Customer();
        customer.setPhoneNumber(dto.getPhoneNumber());
        customer.setRegonNumber(dto.getRegonNumber());
        customer.setNotes(dto.getNotes());
        customer.setNipNumber(dto.getNipNumber());
        customer.setShortName(dto.getShortName());
        customer.setEmail(dto.getEmail());
        customer.setFullName(dto.getFullName());

        customer = getService().save(customer);
        return "redirect:" + BASE_URL + "/" + customer.getId();
    }

//    @RequestMapping(value = "/api/add", method = RequestMethod.POST)
//    public String add(@RequestBody Customer customer, BindingResult result, ModelMap model) {
//        if (result.hasErrors()) {
//            return "error";
//        }
//        customer = getService().save(customer);
//        return "redirect:" + BASE_URL + "/" + customer.getId();
//    }

    @Override
    @Transactional
    @RequestMapping(value = "/{id}/remove", method = RequestMethod.GET)
    public String remove(@PathVariable("id") Integer id) {

        Customer customer = getService().findById(id);
        getService().remove(customer);

        return "redirect:" + BASE_URL + "/";
    }

    @RequestMapping(value = "/api/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    CustomerDto getOne(@PathVariable Integer id) {
        Customer customer = getService().findById(id);
        return new CustomerDto(customer);
    }


    @RequestMapping(value = "/api/list", method = RequestMethod.POST)
    public
    @ResponseBody
    Page<CustomerDto> getPage(@RequestBody PageRequestDto pageRequest) {

        Page<Customer> customersPage =  getService().findAll(new PageRequest(pageRequest.getPage(), pageRequest.getSize()), pageRequest.getSearch());
        return  customersPage.map(source -> new CustomerDto(source));
    }

    @RequestMapping(value = "/api/save", method = RequestMethod.POST)
    public
    @ResponseBody
    CustomerDto save(@RequestBody CustomerDto customerDto) {
        Customer customer = getService().save(dtoToCustomer(customerDto));
        return new CustomerDto(customer);
    }

    private Customer dtoToCustomer(CustomerDto dto) {
        Customer customer = dto.getId() != null ? getService().findById(dto.getId()) : new Customer();
        customer.setFullName(dto.getFullName());
        customer.setShortName(dto.getShortName());
        customer.setNipNumber(dto.getNipNumber());
        customer.setRegonNumber(dto.getRegonNumber());
        customer.setEmail(dto.getEmail());
        customer.setPhoneNumber(dto.getPhoneNumber());
        customer.setNotes(dto.getNotes());

        return customer;
    }

    @ResponseBody
    @RequestMapping(value = "/api/find", method = RequestMethod.POST)
    public ItemList find(@RequestParam String term) {
        ItemList result = new ItemList();
        List<Customer> types = customerService.findAll(CustomerRepository.Spec.fullNameLike(term));

        for (Customer type : types) {
            result.getItemList()
                    .add(new SearchResult(type.getId(), type.getFullName()));
        }
        return result;
    }

}