/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.ihh.cargo.domain.customers;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import javax.persistence.criteria.Path;

@org.springframework.stereotype.Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer>, JpaSpecificationExecutor<Customer> {

    class Spec {
        public static Specification<Customer> globalSearch(final String search) {
            return (root, query, cb) -> {
                Path<String> emailPath = root.get("email");
                Path<String> phoneNumberPath = root.get("phoneNumber");
                return cb.or(
                        cb.like(emailPath, String.format("%%%s%%", search)),
                        cb.like(phoneNumberPath, String.format("%%%s%%", search))
                );
            };
        }
        public static Specification<Customer> emailLike(final String search) {
            return (root, query, cb) -> {
                Path<String> namePath = root.get("email");
                return cb.like(namePath, String.format("%%%s%%", search));
            };
        }
        public static Specification<Customer> fullNameLike(final String search) {
            return (root, query, cb) -> {
                Path<String> namePath = root.get("fullName");
                return cb.like(namePath, String.format("%%%s%%", search));
            };
        }
    }
}