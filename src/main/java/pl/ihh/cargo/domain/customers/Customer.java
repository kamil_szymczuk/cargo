package pl.ihh.cargo.domain.customers;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import pl.ihh.cargo.domain.TrackedEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.validation.constraints.Size;

/**
 * Created by kuta on 20.02.2016.
 */
@Entity
public class Customer extends TrackedEntity {

    @NotEmpty
    @Column(nullable = false)
    @Size(max = 150)
    private String fullName;

    @NotEmpty
    @Column
    @Size(max = 120)
    private String shortName;

    @Column
    private String email;

    @Column
    @Size(max = 30)
    private String phoneNumber;

    @Column(columnDefinition = "BIT", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean active = true;


    //    @OneToOne(cascade = CascadeType.ALL)
//    @NotNull
//    @Valid
//    @JsonIgnoreProperties({"creationUser", "modificationUser"})
//    @JsonSerialize(as = Address.class)
//    private Address address = new Address();
    @Column
    @Size(max = 14)
    private String nipNumber;

    @Column
    @Size(max = 25)
    private String regonNumber;

    @Lob
    private String notes;

    public Customer() {
    }

    public Customer(Integer id) {
        this.id = id;
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getNipNumber() {
        return nipNumber;
    }

    public void setNipNumber(String nipNumber) {
        this.nipNumber = nipNumber;
    }

    public String getRegonNumber() {
        return regonNumber;
    }

    public void setRegonNumber(String regonNumber) {
        this.regonNumber = regonNumber;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}