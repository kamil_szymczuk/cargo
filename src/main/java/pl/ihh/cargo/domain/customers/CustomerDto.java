package pl.ihh.cargo.domain.customers;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by kuta on 21.02.2016.
 */
@Getter
@Setter
public class CustomerDto {

    private Integer id;
    private String fullName;
    private String shortName;
    private String email;
    private String phoneNumber;
    private String nipNumber;
    private String regonNumber;
    private String notes;

    public CustomerDto(Customer customer) {
        this.id = customer.getId();
        this.fullName = customer.getFullName();
        this.shortName = customer.getShortName();
        this.email = customer.getEmail();
        this.phoneNumber = customer.getPhoneNumber();
        this.nipNumber = customer.getNipNumber();
        this.regonNumber = customer.getRegonNumber();
        this.notes = customer.getNotes();
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNipNumber() {
        return nipNumber;
    }

    public void setNipNumber(String nipNumber) {
        this.nipNumber = nipNumber;
    }

    public String getRegonNumber() {
        return regonNumber;
    }

    public void setRegonNumber(String regonNumber) {
        this.regonNumber = regonNumber;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getId() {
        return id;
    }
}
