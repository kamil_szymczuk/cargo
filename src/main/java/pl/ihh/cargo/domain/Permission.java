package pl.ihh.cargo.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
/**
 * Created by kuta on 20.02.2016.
 */

@Entity
//@Audited
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"permission"})})
public class Permission extends BaseEntity implements Serializable {

    @Transient
    private static final long serialVersionUID = -6680998931729029336L;
    private String permission;

    public Permission() {
    }

    public Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return this.permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    protected Long getInitialVersion() {
        return Long.valueOf(0L);
    }
}