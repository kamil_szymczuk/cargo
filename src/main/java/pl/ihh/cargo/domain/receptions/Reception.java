package pl.ihh.cargo.domain.receptions;

import pl.ihh.cargo.domain.TrackedEntity;
import pl.ihh.cargo.domain.customers.Customer;
import pl.ihh.cargo.domain.productTransfers.ProductTransfer;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by risen16 on 14.03.2016.
 */
@Entity
public class Reception extends TrackedEntity {

    @OneToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reception")
    private List<ProductTransfer> transfers = new ArrayList<>();

    public Reception() {
    }

    public Reception(Customer customer, List<ProductTransfer> transfers) {
        this.customer = customer;
        this.transfers = transfers;

    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<ProductTransfer> getTransfers() {
        return transfers;
    }

    public void setTransfers(List<ProductTransfer> transfers) {
        this.transfers = transfers;
    }
}
