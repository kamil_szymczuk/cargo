package pl.ihh.cargo.domain.receptions;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Created by risen16 on 14.03.2016.
 */
@Repository
public interface ReceptionRepository extends JpaRepository<Reception, Integer>, JpaSpecificationExecutor<Reception> {

    class Spec {

//        public static Specification<Reception> nameLike(final Integer quantity) {
//            return (root, query, cb) -> {
//                Path<Integer> quantityPath = root.get("quantity");
//                return cb.like(quantityPath, Integer.format("%%%s%%", quantity));
//            };
//        }
    }
}
