package pl.ihh.cargo.domain.receptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.ihh.cargo.domain.productTransfers.ProductTransfer;
import pl.ihh.cargo.domain.productTransfers.ProductTransferStatus;

import java.util.List;

/**
 * Created by risen16 on 14.03.2016.
 */

@Component("receptionService")
@Transactional
class ReceptionServiceImpl implements ReceptionService {

    private final ReceptionRepository receptionRepository;

    @Autowired
    ReceptionServiceImpl(ReceptionRepository receptionRepository) {
        this.receptionRepository = receptionRepository;
    }

    @Override
    public Page<Reception> findAll(Pageable pageable) {
        return this.receptionRepository.findAll(pageable);
    }

    @Override
    public Page<Reception> findAll(Pageable pageable, String search) {
        return this.receptionRepository.findAll(pageable);
    }

    @Override
    public List<Reception> findAll() {
        return this.receptionRepository.findAll();
    }

    @Override
    public List<Reception> findAll(String search) {
        return null;
    }

    @Override
    public List<Reception> findAll(Specification<Reception> specification) {
        return null;
    }

    @Override
    public Reception findById(Integer id) {
        return this.receptionRepository.findOne(id);
    }

    @Override
    public Reception save(Reception object) {
        object.getTransfers().stream().filter(transfer -> transfer.getProductTransferStatus() == null).forEach(transfer -> {
            transfer.setProductTransferStatus(ProductTransferStatus.OPEN);
        });
        return receptionRepository.save(object);
    }

    @Override
    public void remove(Reception object) {

    }

    @Override
    public void removeById(Integer id) {

    }

    @Override
    public Pageable createPageRequest(int page, int size, Sort.Direction direction, String... properties) {
        return new PageRequest(page, size, direction, properties);
    }
}
