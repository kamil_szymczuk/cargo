package pl.ihh.cargo.domain.receptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.ihh.cargo.BaseController;
import pl.ihh.cargo.domain.PageRequestDto;
import pl.ihh.cargo.domain.customers.CustomerService;
import pl.ihh.cargo.domain.positions.PositionService;
import pl.ihh.cargo.domain.productTransfers.ProductTransfer;
import pl.ihh.cargo.domain.productTransfers.ProductTransferService;
import pl.ihh.cargo.domain.products.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by risen16 on 14.03.2016.
 */

@Slf4j
@Controller
@RequestMapping("/reception")
public class ReceptionController implements BaseController<Reception, ReceptionDto, Integer> {

    private static final String BASE_URL = "/reception";
    private static final String RESOURCE_PATH = "receptions/";

    //region base methods
    @Autowired
    private ReceptionService receptionService;

    @Autowired
    private PositionService positionService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductTypeService productTypeService;

    @Autowired
    private ProductTransferService productTransferService;


    @Override
    public ReceptionService getService() {
        return receptionService;
    }

    @Override
    public String getResourceListPath() {
        return RESOURCE_PATH + PATH_LIST;
    }

    @Override
    public String getResourceDetailsPath() {
        return RESOURCE_PATH + PATH_DETAILS + '2';
    }
    //endregion base methods


    @Override
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView list() {

        List receptions = new ArrayList<>(getService().findAll(new PageRequest(0, 100), "").getContent());

        ModelAndView model = new ModelAndView(getResourceListPath());
        model.addObject("receptions", receptions);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView details(@PathVariable("id") Integer id) {

        Reception reception = getService().findById(id);

        ModelAndView model = new ModelAndView(getResourceDetailsPath());
        model.addObject("reception", reception);
        model.addObject("readonly", true);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("id") Integer id) {

        Reception reception = getService().findById(id);

        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("reception", reception);
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/" + id + "/edit");

        return view;
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    public String edit(@ModelAttribute Reception reception, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "error";
        }
        getService().save(reception);

        return "redirect:/reception/" + reception.getId();
    }

    @Override
    public ModelAndView add() {
        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("reception", new Reception());
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/add");

        return view;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@ModelAttribute @RequestBody Reception reception, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }

        reception = getService().save(reception);

        for (ProductTransfer transfer : reception.getTransfers()) {
            productTransferService.save(transfer);
        }
        return "redirect:" + BASE_URL + "/" + reception.getId();
    }

    @ResponseBody
    @RequestMapping(value = "/api/add", method = RequestMethod.POST)
    public ReceptionDto edit(@RequestBody Reception reception) {

        reception = getService().save(reception);
        for (ProductTransfer transfer : reception.getTransfers()) {
            productTransferService.save(transfer);
        }

        return new ReceptionDto(reception);
    }


    @Override
    @Transactional
    @RequestMapping(value = "/{id}/remove", method = RequestMethod.GET)
    public String remove(@PathVariable("id") Integer id) {

        Reception reception = getService().findById(id);
        getService().remove(reception);

        return "redirect:" + BASE_URL + "/";
    }

    @RequestMapping(value = "/api/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    ReceptionDto getOne(@PathVariable Integer id) {
        Reception reception = getService().findById(id);
        return new ReceptionDto(reception);
    }

    @RequestMapping(value = "/api/list", method = RequestMethod.POST)
    public
    @ResponseBody
    Page<ReceptionDto> getPage(@RequestBody PageRequestDto pageRequest) { // bierzcie z tego wszyscy bo to jest dobre

        Page<Reception> receptionsPage = getService().findAll(new PageRequest(pageRequest.getPage(), pageRequest.getSize()), pageRequest.getSearch());
        return receptionsPage.map(source -> new ReceptionDto(source));

    }

    @RequestMapping(value = "/api/save", method = RequestMethod.POST)
    public
    @ResponseBody
    ReceptionDto save(@RequestBody ReceptionDto receptionDto) {
        Reception reception = getService().save(dtoToReception(receptionDto));
        return new ReceptionDto(reception);
    }

    private Reception dtoToReception(ReceptionDto dto) {
        Reception reception = getService().findById(dto.getId());//new Reception();
        reception.setCustomer(customerService.findById(dto.getCustomerId()));

        return reception;
    }

}
