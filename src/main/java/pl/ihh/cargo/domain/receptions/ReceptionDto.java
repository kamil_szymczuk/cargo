package pl.ihh.cargo.domain.receptions;

import pl.ihh.cargo.domain.productTransfers.ProductTransferDto;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by risen16 on 14.03.2016.
 */
public class ReceptionDto {

    private Integer id;
    private Integer customerId;
    private String customer;
    private List<ProductTransferDto> transfers;

    public ReceptionDto(Reception reception) {
        this.id = reception.getId();
        if (reception.getCustomer() != null) {
            this.customerId = reception.getCustomer().getId();
            this.customer = reception.getCustomer().getFullName();
        }
        reception.getTransfers();
        this.transfers = reception.getTransfers().stream().map(productTransfer -> new ProductTransferDto(productTransfer)).collect(Collectors.toList());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public List<ProductTransferDto> getTransfers() {
        return transfers;
    }

    public void setTransfers(List<ProductTransferDto> transfers) {
        this.transfers = transfers;
    }
}
