package pl.ihh.cargo.domain.receptions;

import pl.ihh.cargo.BaseService;

/**
 * Created by risen16 on 14.03.2016.
 */

public interface ReceptionService extends BaseService<Reception, Integer> {
}
