package pl.ihh.cargo.domain.issues;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.ihh.cargo.BaseController;
import pl.ihh.cargo.domain.PageRequestDto;
import pl.ihh.cargo.domain.customers.CustomerService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by risen16 on 29.02.2016.
 */

@Slf4j
@Controller
@RequestMapping("/issue")
public class IssueController implements BaseController<Issue, IssueDto, Integer> {

    private static final String BASE_URL = "/issue";
    private static final String RESOURCE_PATH = "issues/";

    @Autowired
    private IssueService issueService;

    @Autowired
    private CustomerService customerService;

    @Override
    public IssueService getService() {
        return issueService;
    }

    @Override
    public String getResourceListPath() {
        return RESOURCE_PATH + PATH_LIST;
    }

    @Override
    public String getResourceDetailsPath() {
        return RESOURCE_PATH + PATH_DETAILS;
    }


    @Override
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView list() {
        List issues = new ArrayList<>(getService().findAll(new PageRequest(0, 100), "").getContent());

        ModelAndView model = new ModelAndView(getResourceListPath());
        model.addObject("issues", issues);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView details(@PathVariable("id") Integer id) {
        Issue issue = getService().findById(id);

        ModelAndView model = new ModelAndView(getResourceListPath());
        model.addObject("issue", issue);
        model.addObject("readonly", true);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("id") Integer id) {
        Issue issue = getService().findById(id);

        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("issue", issue);
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/" + id + "/edit");

        return view;
    }


    @RequestMapping(value = "/api/{id}/edit", method = RequestMethod.POST)
    public String edit(@RequestBody Issue issue, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "error";
        }

        Issue issueDb = getService().findById(issue.getId());
        issueDb.setIssueNumber(issue.getIssueNumber());
        issueDb.setCustomer(issue.getCustomer());
        issueDb.setDepartureDate(issue.getDepartureDate());
        issueDb.setIssueStatus(issue.getIssueStatus());

        getService().save(issueDb);

        return "redirect:/issue/" + issue.getId();
    }

    @Override
    public ModelAndView add() {
        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("issue", new Issue());
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/add");

        return view;
    }

    @Override
    public String add(@RequestBody Issue issue, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }

        issue = getService().save(issue);

        return "redirect:" + BASE_URL + "/" + issue.getId();
    }

    @Override
    @Transactional
    @RequestMapping(value = "/{id}/remove", method = RequestMethod.GET)
    public String remove(@PathVariable("id") Integer id) {

        Issue issue = getService().findById(id);
        getService().remove(issue);

        return "redirect:" + BASE_URL + "/";
    }

    @RequestMapping(value = "/api/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    IssueDto getOne(@PathVariable Integer id) {
        Issue issue = getService().findById(id);
        return new IssueDto(issue);
    }

    @RequestMapping(value = "/api/list", method = RequestMethod.POST)
    public
    @ResponseBody
    Page<IssueDto> getPage(@RequestBody PageRequestDto pageRequest) {

        Page<Issue> issuesPage = getService().findAll(new PageRequest(pageRequest.getPage(), pageRequest.getSize()), pageRequest.getSearch());
        return issuesPage.map(source -> new IssueDto(source));

    }

    @RequestMapping(value = "/api/save", method = RequestMethod.POST)
    public
    @ResponseBody
    IssueDto save(@RequestBody IssueDto issueDto) {
        Issue issue = getService().save(dtoToIssue(issueDto));
        return new IssueDto(issue);
    }

    private Issue dtoToIssue(IssueDto dto) {
        Issue issue = (dto.getId() != null) ? getService().findById(dto.getId()) : new Issue();
        issue.setIssueNumber(dto.getIssueNumber());
        issue.setIssueStatus(dto.getIssueStatus());
        issue.setDepartureDate(dto.getDepartureDate());
        issue.setCustomer(customerService.findById(dto.getCustomerId()));

        return issue;
    }
}
