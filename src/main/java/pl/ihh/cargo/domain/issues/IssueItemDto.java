package pl.ihh.cargo.domain.issues;

import lombok.Getter;
import lombok.Setter;
import pl.ihh.cargo.domain.positions.PositionDto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by risen16 on 01.03.2016.
 */
@Getter
@Setter
public class IssueItemDto {

    private List<PositionDto> positions;
    private Integer productId;
    private String productName;
    private Integer quantity;

    public IssueItemDto(IssueItem issueItem) {
        productId = issueItem.getProduct().getId();
        quantity = issueItem.getQuantity();
        productName = issueItem.getProduct().getSubcode();
        positions = new ArrayList<>();
        positions.addAll(issueItem.getPositionList().stream().map(PositionDto::new).collect(Collectors.toList()));
    }

    public List<PositionDto> getPositions() {
        return positions;
    }

    public void setPositions(List<PositionDto> positions) {
        this.positions = positions;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
