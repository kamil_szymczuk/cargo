package pl.ihh.cargo.domain.issues;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import pl.ihh.cargo.domain.TrackedEntity;
import pl.ihh.cargo.domain.customers.Customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Issue extends TrackedEntity {

    @ManyToOne
    private Customer customer;

    @Column(nullable = false)
    private String departureDate;

    @Column(nullable = false)
    private String issueNumber;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany
    private List<IssueItem> items = new ArrayList<>();

    @Column(nullable = false)
    private String issueStatus;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    protected Issue () {

    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(String issueNumber) {
        this.issueNumber = issueNumber;
    }

    public List<IssueItem> getItems() {
        return items;
    }

    public void setItems(List<IssueItem> items) {
        this.items = items;
    }

    public String getIssueStatus() {
        return issueStatus;
    }

    public void setIssueStatus(String issueStatus) {
        this.issueStatus = issueStatus;
    }

}
