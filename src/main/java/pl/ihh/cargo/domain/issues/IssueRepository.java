package pl.ihh.cargo.domain.issues;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Path;

/**
 * Created by risen16 on 29.02.2016.
 */
@Repository
public interface IssueRepository extends JpaRepository<Issue, Integer>, JpaSpecificationExecutor<Issue> {

    class Spec {
        public static Specification<Issue> nameLike(final String issueNumber) {
            return (root, query, cb) -> {
                Path<String> issueNumberPath = root.get("issueNumber");
                return cb.like(issueNumberPath, String.format("%%%s%%", issueNumber));
            };
        }
    }
}
