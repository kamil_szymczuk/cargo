package pl.ihh.cargo.domain.issues;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class IssueServiceImpl implements IssueService {

    private final IssueRepository issueRepository;

    @Autowired
    public IssueServiceImpl(IssueRepository issueRepository) {
        this.issueRepository = issueRepository;
    }

    @Override
    public Page<Issue> findAll(Pageable pageable) {
        return this.issueRepository.findAll(pageable);
    }

    @Override
    public Page<Issue> findAll(Pageable pageable, String search) {

        return this.issueRepository.findAll(IssueRepository.Spec.nameLike(search),pageable);
    }

    @Override
    public List<Issue> findAll() {
        return this.issueRepository.findAll();
    }

    @Override
    public List<Issue> findAll(String search) {
        return null;
    }

    @Override
    public List<Issue> findAll(Specification<Issue> specification) {
        return null;
    }

    @Override
    public Issue findById(Integer id) {
        return this.issueRepository.findOne(id);
    }

    @Override
    public Issue save(Issue object) {
        return this.issueRepository.save(object);
    }

    @Override
    public void remove(Issue object) {
        this.issueRepository.delete(object);
    }

    @Override
    public void removeById(Integer id) {
        this.issueRepository.delete(id);

    }

    @Override
    public Pageable createPageRequest(int page, int size, Sort.Direction direction, String... properties) {
        return new PageRequest(page, size, direction, properties);
    }
}
