package pl.ihh.cargo.domain.issues;

import pl.ihh.cargo.BaseService;

/**
 * Created by risen16 on 29.02.2016.
 */
public interface IssueService extends BaseService<Issue, Integer> {
}
