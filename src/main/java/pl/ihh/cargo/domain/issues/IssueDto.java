package pl.ihh.cargo.domain.issues;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by risen16 on 02.03.2016.
 */
public class IssueDto {


    private Integer id;
    private Integer customerId;
    private String customer;
    private String departureDate;
    private String issueNumber;
    private List<IssueItemDto> items = new ArrayList<>();
    private String issueStatus;

    public IssueDto() {

    }

    public IssueDto(Issue issue) {
        this.id = issue.getId();
        this.departureDate = issue.getDepartureDate().toString();
        this.issueNumber = issue.getIssueNumber();
        this.issueStatus = issue.getIssueStatus().toString();
        if (issue.getCustomer() != null) {
            this.customerId = issue.getCustomer().getId();
            this.customer = issue.getCustomer().getFullName();
        }
        if (issue.getItems() != null)
            items.addAll(issue.getItems().stream().map(IssueItemDto::new).collect(Collectors.toList()));

    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(String issueNumber) {
        this.issueNumber = issueNumber;
    }

    public List<IssueItemDto> getItems() {
        return items;
    }

    public void setItems(List<IssueItemDto> items) {
        this.items = items;
    }

    public String getIssueStatus() {
        return issueStatus;
    }

    public void setIssueStatus(String issueStatus) {
        this.issueStatus = issueStatus;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }
}
