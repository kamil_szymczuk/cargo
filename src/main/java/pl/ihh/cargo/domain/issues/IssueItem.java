package pl.ihh.cargo.domain.issues;

import pl.ihh.cargo.domain.TrackedEntity;
import pl.ihh.cargo.domain.positions.Position;
import pl.ihh.cargo.domain.products.Product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by risen16 on 29.02.2016.
 */
@Entity
public class IssueItem extends TrackedEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private Product product;

    @OneToMany
    private List<Position> positionList = new ArrayList<>();

    @Column(nullable = false)
    private Integer quantity;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Position> getPositionList() {
        return positionList;
    }

    public void setPositionList(List<Position> positionList) {
        this.positionList = positionList;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
