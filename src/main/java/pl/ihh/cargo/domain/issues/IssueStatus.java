package pl.ihh.cargo.domain.issues;

/**
 * Created by risen16 on 29.02.2016.
 */
public enum IssueStatus {
    zamówienie, rezerwacja, realizacja, wydane
}
