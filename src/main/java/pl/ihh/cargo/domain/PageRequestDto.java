package pl.ihh.cargo.domain;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by kuta on 28.02.2016.
 */
public class PageRequestDto {

    int page = 0;
    int size = 20;
    String search = "";

    List<SortDto> sort = new ArrayList<>();

    public PageRequest getAsPageRequest() {
        Sort sort;
        if (getSort() != null) {
            sort = new Sort(getSort().stream().map((sortDto) -> sortDto.getAsOrder()).collect(Collectors.toList()));
            return new PageRequest(getPage(), getSize(), sort);
        }
        return new PageRequest(getPage(), getSize());

    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<SortDto> getSort() {
        return sort;
    }

    public void setSort(List<SortDto> sort) {
        this.sort = sort;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
