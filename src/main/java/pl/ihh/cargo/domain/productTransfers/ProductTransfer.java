package pl.ihh.cargo.domain.productTransfers;

import pl.ihh.cargo.domain.TrackedEntity;
import pl.ihh.cargo.domain.customers.Customer;
import pl.ihh.cargo.domain.positions.Position;
import pl.ihh.cargo.domain.products.Product;
import pl.ihh.cargo.domain.receptions.Reception;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by risen16 on 04.03.2016.
 */
@Entity
public class ProductTransfer extends TrackedEntity {

    @OneToOne
    private Position positionSource;

    @OneToOne
    private Position positionDestination;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ProductTransferStatus productTransferStatus;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "reception_id")
    private Reception reception;

    public ProductTransfer() {
    }

    public Position getPositionSource() {
        return positionSource;
    }

    public void setPositionSource(Position positionSource) {
        this.positionSource = positionSource;
    }

    public Position getPositionDestination() {
        return positionDestination;
    }

    public void setPositionDestination(Position positionDestination) {
        this.positionDestination = positionDestination;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public ProductTransferStatus getProductTransferStatus() {
        return productTransferStatus;
    }

    public void setProductTransferStatus(ProductTransferStatus productTransferStatus) {
        this.productTransferStatus = productTransferStatus;
    }

    public Position getPosition() {
        return positionDestination;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Reception getReception() {
        return reception;
    }

    public void setReception(Reception reception) {
        this.reception = reception;
    }
}
