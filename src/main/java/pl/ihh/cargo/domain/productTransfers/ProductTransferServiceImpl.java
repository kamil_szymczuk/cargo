package pl.ihh.cargo.domain.productTransfers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by risen16 on 04.03.2016.
 */
@Component("productTransferService")
@Transactional
public class ProductTransferServiceImpl implements ProductTransferService{

    private final ProductTransferRepository productTransferRepository;

    @Autowired
    ProductTransferServiceImpl(ProductTransferRepository productTransferRepository) { this.productTransferRepository = productTransferRepository; }

    @Override
    public Page<ProductTransfer> findAll(Pageable pageable) {
        return this.productTransferRepository.findAll(pageable);
    }

    @Override
    public Page<ProductTransfer> findAll(Pageable pageable, String search) {
        return this.productTransferRepository.findAll(pageable);
    }

    @Override
    public List<ProductTransfer> findAll() {
        return this.productTransferRepository.findAll();
    }

    @Override
    public List<ProductTransfer> findAll(String search) {
        return null;
    }

    @Override
    public List<ProductTransfer> findAll(Specification<ProductTransfer> specification) {
        return null;
    }

    @Override
    public ProductTransfer findById(Integer id) {
        return this.productTransferRepository.findOne(id);
    }

    @Override
    public ProductTransfer save(ProductTransfer object) {

        if(object.getProductTransferStatus()==null){
            object.setProductTransferStatus(ProductTransferStatus.OPEN);
        }
        return productTransferRepository.save(object);
    }

    @Override
    public void remove(ProductTransfer object) {
        productTransferRepository.delete(object);
    }

    @Override
    public void removeById(Integer id) {
        productTransferRepository.delete(id);
    }

    @Override
    public Pageable createPageRequest(int page, int size, Sort.Direction direction, String... properties) {
        return new PageRequest(page, size, direction, properties);
    }

}
