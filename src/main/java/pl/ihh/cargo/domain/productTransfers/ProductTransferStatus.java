package pl.ihh.cargo.domain.productTransfers;

/**
 * Created by risen16 on 04.03.2016.
 */
public enum ProductTransferStatus {
    OPEN, IN_PROGRESS, DONE, REJECTED
}
