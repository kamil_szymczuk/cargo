package pl.ihh.cargo.domain.productTransfers;

import lombok.Getter;
import lombok.Setter;
import pl.ihh.cargo.domain.positions.PositionDto;

/**
 * Created by risen16 on 04.03.2016.
 */
@Getter
@Setter
public class ProductTransferDto {

    private Integer id;
    private Integer positionSourceId;
    private Integer positionDestinationId;
    private Integer quantity;
    private ProductTransferStatus productTransferStatus;
    private Integer productId;
    private String product;

    public ProductTransferDto() {
    }

    public ProductTransferDto(ProductTransfer productTransfer) {
        this.id = productTransfer.getId();
        if (productTransfer.getPosition() != null) {
            this.positionSourceId = productTransfer.getPosition().getId();
            this.positionDestinationId = productTransfer.getPosition().getId();
        }
        if (productTransfer.getProduct() != null) {
            this.product = productTransfer.getProduct().getSubcode();
            this.productId = productTransfer.getProduct().getId();
        }
        this.quantity = productTransfer.getQuantity();
        this.productTransferStatus = productTransfer.getProductTransferStatus();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPositionSourceId() {
        return positionSourceId;
    }

    public void setPositionSourceId(Integer positionSourceId) {
        this.positionSourceId = positionSourceId;
    }

    public Integer getPositionDestinationId() {
        return positionDestinationId;
    }

    public void setPositionDestinationId(Integer positionDestinationId) {
        this.positionDestinationId = positionDestinationId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public ProductTransferStatus getProductTransferStatus() {
        return productTransferStatus;
    }

    public void setProductTransferStatus(ProductTransferStatus productTransferStatus) {
        this.productTransferStatus = productTransferStatus;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}
