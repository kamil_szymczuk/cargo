package pl.ihh.cargo.domain.productTransfers;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import javax.persistence.criteria.Path;

/**
 * Created by risen16 on 04.03.2016.
 */
public interface ProductTransferRepository extends JpaRepository<ProductTransfer, Integer>, JpaSpecificationExecutor<ProductTransfer>{

    class Spec {
        public static Specification<ProductTransfer> globalSearch(final String search) {
            return (root, query, cb) -> {
                Path<String> productTransferStatusPath = root.get("productTransferStatus");
                return cb.like(productTransferStatusPath, String.format("%%%s%%", search));
            };
        }
        public static Specification<ProductTransfer> nameLike(final String search) {
            return (root, query, cb) -> {
                Path<String> productTransferStatusPath = root.get("productTransferStatus");
                return cb.like(productTransferStatusPath, String.format("%%%s%%", search));
            };
        }
    }
}
