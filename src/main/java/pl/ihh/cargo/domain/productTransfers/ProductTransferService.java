package pl.ihh.cargo.domain.productTransfers;

import pl.ihh.cargo.BaseService;

/**
 * Created by risen16 on 04.03.2016.
 */
public interface ProductTransferService extends BaseService<ProductTransfer, Integer> {

    ProductTransfer findById(Integer id);
}
