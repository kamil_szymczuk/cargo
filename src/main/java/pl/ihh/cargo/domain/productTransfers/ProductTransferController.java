package pl.ihh.cargo.domain.productTransfers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.ihh.cargo.BaseController;
import pl.ihh.cargo.domain.PageRequestDto;
import pl.ihh.cargo.domain.customers.CustomerService;
import pl.ihh.cargo.domain.positions.PositionService;
import pl.ihh.cargo.domain.products.ItemList;
import pl.ihh.cargo.domain.products.ProductService;
import pl.ihh.cargo.domain.products.SearchResult;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by risen16 on 04.03.2016.
 */
@Slf4j
@Controller
@RequestMapping("/productTransfer")
public class ProductTransferController implements BaseController<ProductTransfer, ProductTransferDto, Integer> {

    private static final String BASE_URL = "/productTransfer";
    private static final String RESOURCE_PATH = "productTransfers/";

    @Autowired
    private ProductTransferService productTransferService;

    @Autowired
    private ProductService productService;

    @Override
    public ProductTransferService getService() {
        return productTransferService;
    }

    @Override
    public String getResourceListPath() {
        return RESOURCE_PATH + PATH_LIST;
    }

    @Override
    public String getResourceDetailsPath() {
        return RESOURCE_PATH + PATH_DETAILS + '2';
    }


    @Override
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView list() {

        List productTransfers = new ArrayList<>(getService().findAll(new PageRequest(0, 100), "").getContent());

        ModelAndView model = new ModelAndView(getResourceListPath());
        model.addObject("productTransfers", productTransfers);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView details(@PathVariable("id") Integer id) {

        ProductTransfer productTransfer = getService().findById(id);

        ModelAndView model = new ModelAndView(getResourceDetailsPath());
        model.addObject("productTransfer", productTransfer);
        model.addObject("readonly", true);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("id") Integer id) {

        ProductTransfer productTransfer = getService().findById(id);

        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("productTransfer", productTransfer);
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/" + id + "/edit");

        return view;
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    public String edit(@ModelAttribute ProductTransfer productTransfer, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "error";
        }
        getService().save(productTransfer);

        return "redirect:/productTransfer/" + productTransfer.getId();
    }

    @Override
    public ModelAndView add() {
        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("productTransfer", new ProductTransfer());
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/add");

        return view;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@ModelAttribute ProductTransfer productTransfer, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
//        ProductTransfer productTransfer = new ProductTransfer();
//        productTransfer = productTransferService.save(productTransfer);

        productTransfer = getService().save(productTransfer);

        return "redirect:" + BASE_URL + "/" + productTransfer.getId();
    }

    @Override
    @Transactional
    @RequestMapping(value = "/{id}/remove", method = RequestMethod.GET)
    public String remove(@PathVariable("id") Integer id) {

        ProductTransfer productTransfer = getService().findById(id);
        getService().remove(productTransfer);

        return "redirect:" + BASE_URL + "/";
    }

    @RequestMapping(value = "/api/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    ProductTransferDto getOne(@PathVariable Integer id) {
        ProductTransfer productTransfer = getService().findById(id);
        return new ProductTransferDto(productTransfer);
    }


    @RequestMapping(value = "/api/list", method = RequestMethod.POST)
    public
    @ResponseBody
    Page<ProductTransferDto> getPage(@RequestBody PageRequestDto pageRequest) {

        Page<ProductTransfer> productTransfersPage = getService().findAll(new PageRequest(pageRequest.getPage(), pageRequest.getSize()), pageRequest.getSearch());
        return productTransfersPage.map(source -> new ProductTransferDto(source));

    }

    @RequestMapping(value = "/api/save", method = RequestMethod.POST)
    public
    @ResponseBody
    ProductTransferDto save(@RequestBody ProductTransferDto productTransferDto) {
        ProductTransfer productTransfer = getService().save(dtoToProductTransfer(productTransferDto));
        return new ProductTransferDto(productTransfer);
    }

    private ProductTransfer dtoToProductTransfer(ProductTransferDto dto) {
        ProductTransfer productTransfer = dto.getId() != null ? getService().findById(dto.getId()) : new ProductTransfer();
        if (dto.getId() == null) {
            productTransfer.setProductTransferStatus(ProductTransferStatus.OPEN);
        }else{
            productTransfer.setProductTransferStatus(dto.getProductTransferStatus());
        }
        productTransfer.setQuantity(dto.getQuantity());

        productTransfer.setProduct(productService.findById(dto.getProductId()));


        return productTransfer;
    }

    @ResponseBody
    @RequestMapping(value = "/api/find", method = RequestMethod.POST)
    public ItemList find(@RequestParam String term) {
        ItemList result = new ItemList();
//        List<ProductTransfer> productTransfers = productTransferService.findAll(new PageRequest(0, 30), term).getContent();
        List<ProductTransfer> types = productTransferService.findAll(ProductTransferRepository.Spec.nameLike(term));

        for (ProductTransfer type : types) {
            result.getItemList()
                    .add(new SearchResult(type.getId(), type.toString()));
        }
        return result;
    }
}
