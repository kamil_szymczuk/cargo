package pl.ihh.cargo.domain.lanes;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.ihh.cargo.BaseController;
import pl.ihh.cargo.domain.PageRequestDto;
import pl.ihh.cargo.domain.products.ItemList;
import pl.ihh.cargo.domain.products.SearchResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by risen16 on 25.02.2016.
 */

@Slf4j
@Controller
@RequestMapping("/lane")
public class LaneController implements BaseController<Lane, LaneDto, Integer> {

    private static final String BASE_URL = "/lane";
    private static final String RESOURCE_PATH = "lanes/";

    //region base methods
    @Autowired
    private LaneService laneService;

    @Override
    public LaneService getService() {
        return laneService;
    }

    @Override
    public String getResourceListPath() {
        return RESOURCE_PATH + PATH_LIST;
    }

    @Override
    public String getResourceDetailsPath() {
        return RESOURCE_PATH + PATH_DETAILS + '2';
    }
    //endregion base methods

    @Override
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView list() {

        List lanes = new ArrayList<>(getService().findAll(new PageRequest(0, 100), "").getContent());

        ModelAndView model = new ModelAndView(getResourceListPath());
        model.addObject("lanes", lanes);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView details(@PathVariable("id") Integer id) {

        Lane lane = getService().findById(id);

        ModelAndView model = new ModelAndView(getResourceDetailsPath());
        model.addObject("lane", lane);
        model.addObject("positions", lane.getPositions());
        model.addObject("readonly", true);

        return model;
    }

    @Override
    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("id") Integer id) {

        Lane lane = getService().findById(id);

        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("lane", lane);
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/" + id + "/edit");

        return view;
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    public String edit(@ModelAttribute Lane lane, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "error";
        }
        getService().save(lane);

        return "redirect:/lane/" + lane.getId();
    }

    @Override
    public ModelAndView add() {
        ModelAndView view = new ModelAndView(getResourceDetailsPath());
        view.addObject("lane", new Lane());
        view.addObject("readonly", false);
        view.addObject("saveUrl", BASE_URL + "/add");

        return view;
    }

    @Override
    public String add(@RequestBody Lane lane, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }

        getService().save(lane);
        return "redirect:" + BASE_URL + "/" + lane.getId();
    }

    @Override
    @Transactional
    @RequestMapping(value = "/{id}/remove", method = RequestMethod.GET)
    public String remove(@PathVariable("id") Integer id) {

        Lane lane = getService().findById(id);
        getService().remove(lane);

        return "redirect:" + BASE_URL + "/";
    }

    @RequestMapping(value = "/api/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    LaneDto getOne(@PathVariable Integer id) {
        Lane lane = getService().findById(id);
        return new LaneDto(lane);
    }

    @RequestMapping(value = "/api/list", method = RequestMethod.POST)
    public
    @ResponseBody
    Page<LaneDto> getPage(@RequestBody PageRequestDto pageRequest) {

        Page<Lane> lanesPage = getService().findAll(new PageRequest(pageRequest.getPage(), pageRequest.getSize()), pageRequest.getSearch());
        return lanesPage.map(source -> new LaneDto(source));

    }

    @RequestMapping(value = "/api/list", method = RequestMethod.GET)
    public
    @ResponseBody
    List<Lane> getAll() {
        return getService().findAll(new PageRequest(0, 100)).getContent();
    }

    @RequestMapping(value = "/api/save", method = RequestMethod.POST)
    public
    @ResponseBody
    LaneDto save(@RequestBody LaneDto laneDto) {
        Lane lane = getService().save(dtoToLane(laneDto));
        return new LaneDto(lane);
    }

    private Lane dtoToLane(LaneDto dto) {
        Lane lane = dto.getId() != null ? getService().findById(dto.getId()) : new Lane();
        lane.setName(dto.getName());
        lane.setShortName(dto.getShortName());

        return lane;
    }

    @ResponseBody
    @RequestMapping(value = "/api/find", method = RequestMethod.POST)
    public ItemList find(@RequestParam String term) {
        ItemList result = new ItemList();
        List<Lane> types = laneService.findAll(term);

        for (Lane type : types) {
            result.getItemList()
                    .add(new SearchResult(type.getId(), type.getName(), type.getShortName()));
        }

        return result;
    }

}
