package pl.ihh.cargo.domain.lanes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by risen16 on 25.02.2016.
 */

@Component("/laneService")
@Transactional
public class LaneServiceImpl implements LaneService {

    private final LaneRepository laneRepository;

    @Autowired
    public LaneServiceImpl(LaneRepository laneRepository) {
        this.laneRepository = laneRepository;
    }

    @Override
    public Page<Lane> findAll(Pageable pageable) {
        return this.laneRepository.findAll(pageable);
    }

    @Override
    public Page<Lane> findAll(Pageable pageable, String search) {
        return this.laneRepository.findAll(LaneRepository.Spec.globalSearch(search), pageable);
    }

    @Override
    public List<Lane> findAll() {
        return this.laneRepository.findAll();
    }

    @Override
    public List<Lane> findAll(String search) {
        return this.laneRepository.findAll(LaneRepository.Spec.globalSearch(search));
    }

    @Override
    public List<Lane> findAll(Specification<Lane> specification) {
        return this.laneRepository.findAll(specification);
    }

    @Override
    public Lane findById(Integer id) {
        return this.laneRepository.findOne(id);
    }

    @Override
    public Lane save(Lane object) {
        return this.laneRepository.save(object);
    }

    @Override
    public void remove(Lane object) {
        this.laneRepository.delete(object);
    }

    @Override
    public void removeById(Integer id) {
        this.laneRepository.delete(id);
    }

    @Override
    public Pageable createPageRequest(int page, int size, Sort.Direction direction, String... properties) {
        return new PageRequest(page, size, direction, properties);
    }
}
