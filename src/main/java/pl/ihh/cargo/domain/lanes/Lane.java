package pl.ihh.cargo.domain.lanes;

import pl.ihh.cargo.domain.TrackedEntity;
import pl.ihh.cargo.domain.positions.Position;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by risen16 on 25.02.2016.
 */
@Entity
public class Lane extends TrackedEntity{
    private static final long serialVersionUID = 1L;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false, unique = true)
    private String shortName;

    @Column
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "lane")
    private List<Position> positions = new ArrayList<>();

    public Lane() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }
}
