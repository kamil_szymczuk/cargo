package pl.ihh.cargo.domain.lanes;

import org.springframework.stereotype.Service;
import pl.ihh.cargo.BaseService;

@Service
public interface LaneService extends BaseService<Lane, Integer> {

}
