package pl.ihh.cargo.domain.lanes;

import lombok.Getter;
import lombok.Setter;
import pl.ihh.cargo.domain.positions.PositionDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by risen16 on 02.03.2016.
 */
@Getter
@Setter
public class LaneDto {

    private Integer id;
    private String shortName;
    private String name;
    private List<PositionDto> positions = new ArrayList<>();

    public LaneDto (Lane lane) {
        this.id = lane.getId();
        this.shortName = lane.getShortName();
        this.name = lane.getName();
//        if (lane.getPositions() != null) {
//            positions.addAll(lane.getPositions().stream().map(PositionDto::new).collect(Collectors.toList()));
//        }
    }

    public List<PositionDto> getPositions() {
        return positions;
    }

    public void setPositions(List<PositionDto> positions) {
        this.positions = positions;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
