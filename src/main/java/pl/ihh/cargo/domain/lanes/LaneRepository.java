package pl.ihh.cargo.domain.lanes;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import javax.persistence.criteria.Path;

/**
 * Created by risen16 on 25.02.2016.
 */
@org.springframework.stereotype.Repository
public interface LaneRepository extends JpaRepository<Lane, Integer>, JpaSpecificationExecutor<Lane> {

    class Spec {
        public static Specification<Lane> globalSearch(final String search) {
            return (root, query, cb) -> {
                Path<String> namePath = root.get("name");
                Path<String> descriptionPath = root.get("shortName");
                return cb.or(
                        cb.like(namePath, String.format("%%%s%%", search)),
                        cb.like(descriptionPath, String.format("%%%s%%", search))
                );
            };
        }
        public static Specification<Lane> nameLike(final String search) {
            return (root, query, cb) -> {
                Path<String> namePath = root.get("name");
                return cb.like(namePath, String.format("%%%s%%", search));
            };
        }
        public static Specification<Lane> shortNameLike(final String search) {
            return (root, query, cb) -> {
                Path<String> namePath = root.get("shortName");
                return cb.like(namePath, String.format("%%%s%%", search));
            };
        }
    }
}
