/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.ihh.cargo.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        return this.userRepository.findAll(pageable);
    }

    @Override
    public Page<User> findAll(Pageable pageable, String search) {
        return this.userRepository.findAll(pageable);
    }

    @Override
    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    @Override
    public List<User> findAll(String search) {
        return null;
    }

    @Override
    public List<User> findAll(Specification<User> specification) {
        return null;
    }

    @Override
    public User findById(Integer id) {
        return this.userRepository.findOne(id);
    }

    @Override
    public User save(User object) {
        return userRepository.save(object);
    }

    @Override
    public void remove(User object) {
        userRepository.delete(object);
    }

    @Override
    public void removeById(Integer id) {
        userRepository.delete(id);
    }

    @Override
    public Pageable createPageRequest(int page, int size, Sort.Direction direction, String... properties) {
        return new PageRequest(page, size, direction, properties);
    }

    public User login(String email, String password) {
        return userRepository.findByUsernameAndPassword(email, password);
    }

    public User findUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

}
