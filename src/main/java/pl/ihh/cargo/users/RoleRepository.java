package pl.ihh.cargo.users;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kuta on 19.03.2016.
 */

public interface RoleRepository extends JpaRepository<Role, Serializable>
{

}