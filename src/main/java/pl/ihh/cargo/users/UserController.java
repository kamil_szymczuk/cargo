package pl.ihh.cargo.users;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.ihh.cargo.BaseController;
import pl.ihh.cargo.domain.PageRequestDto;
import pl.ihh.cargo.domain.products.Product;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/user")
public class UserController implements BaseController<User, UserDto, Integer> {

	private static final String RESOURCE_PATH = "users/";
	private static final String BASE_URL = "/user";

	@Autowired
	private UserService userService;

	@Override
	public UserService getService() {
		return userService;
	}

	@Override
	public String getResourceListPath() {
		return RESOURCE_PATH + PATH_LIST;
	}

	@Override
	public String getResourceDetailsPath() {
		return RESOURCE_PATH + PATH_DETAILS + '2';
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView list() {

		List users = new ArrayList<>(getService().findAll(new PageRequest(0, 100), "").getContent());

		ModelAndView model = new ModelAndView(getResourceListPath());
		model.addObject("users", users);

		return model;
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ModelAndView details(@PathVariable("id") Integer id) {

		User user = getService().findById(id);

		ModelAndView model = new ModelAndView(getResourceDetailsPath());
		model.addObject("user", user);
		model.addObject("readonly", true);

		return model;
	}

	@Override
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable("id") Integer id) {
		User user = getService().findById(id);

		ModelAndView view = new ModelAndView(getResourceDetailsPath());
		view.addObject("user", user);
		view.addObject("readonly", false);
		view.addObject("saveUrl", BASE_URL + "/" + id + "/edit");

		return view;
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
	public String edit(@ModelAttribute User user, BindingResult result, ModelMap model) {

		if (result.hasErrors()) {
			return "error";
		}
		getService().save(user);

		return "redirect:/user/" + user.getId();
	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView add() {
		ModelAndView view = new ModelAndView(getResourceDetailsPath());
		view.addObject("user", new User());
		view.addObject("readonly", false);
		view.addObject("saveUrl", BASE_URL + "/add");

		return view;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@RequestBody @ModelAttribute  User user, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			return "error";
		}
		
		try {
			user.setPassword(hash(user.getUsername(), user.getPassword()));
			user = getService().save(user);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "error";
		}

		return "redirect:" + BASE_URL + "/" + user.getId();
	}

	@Override
	@Transactional
	@RequestMapping(value = "/{id}/remove", method = RequestMethod.GET)
	public String remove(@PathVariable("id") Integer id) {

		User user = getService().findById(id);
		getService().remove(user);

		return "redirect:" + BASE_URL + "/";
	}

	@RequestMapping(value = "/api/{id}", method = RequestMethod.GET)
	public @ResponseBody UserDto getOne(@PathVariable Integer id) {
		User user = getService().findById(id);
		return new UserDto(user);
	}

	@RequestMapping(value = "/api/list", method = RequestMethod.POST)
	@ResponseBody
	public Page<UserDto> getPage(@RequestBody PageRequestDto pageRequest) {

		Page<User> usersPage = getService().findAll(new PageRequest(pageRequest.getPage(), pageRequest.getSize()),
				pageRequest.getSearch());
		return usersPage.map(source -> new UserDto(source));
	}

	@RequestMapping(value = "/api/save", method = RequestMethod.POST)
	public @ResponseBody UserDto save(@RequestBody UserDto userDto) {
		User user = getService().save(dtoToUser(userDto));
		return new UserDto(user);
	}

	private User dtoToUser(UserDto dto) {
		User user = dto.getId() != null ? getService().findById(dto.getId()) : new User();

		user.setFirstName(dto.getFirstName());
		user.setLastName(dto.getLastName());
		user.setUsername(dto.getUsername());
		user.setPassword(dto.getPassword());
		user.setPhoneNumber(dto.getPhoneNumber());
		user.setCode(dto.getCode());
		user.setCostAccount(dto.getCostAccount());
		user.setEmail(dto.getEmail());
		user.setFinanceAccount(dto.getFinanceAccount());
		return user;
	}

	@RequestMapping(value = "/active", method = RequestMethod.GET)
	public ModelAndView activeUserDetails() {

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		// String name = user.getUsername(); //get logged in username

		ModelAndView model = new ModelAndView(getResourceDetailsPath());
		model.addObject("user", user);
		model.addObject("readonly", true);

		return model;
	}

	private String hash(String username, String pass) throws NoSuchAlgorithmException {

		String saltPass = pass;

		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(saltPass.getBytes());

		byte byteData[] = md.digest();

		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

		System.out.println("Hex format : " + sb.toString());
		return sb.toString();
	}

}
