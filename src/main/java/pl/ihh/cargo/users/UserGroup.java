package pl.ihh.cargo.users;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import pl.ihh.cargo.domain.BaseEntity;
import pl.ihh.cargo.domain.Permission;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuta on 20.02.2016.
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"role"})}
)
public class UserGroup extends BaseEntity implements Serializable {
    @Transient
    private static final long serialVersionUID = 1L;
    @ManyToMany(
            mappedBy = "groups"
    )
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<User> users = new ArrayList();
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @OrderBy("permission")
    private List<Permission> permissions = new ArrayList();
//    @ElementCollection(targetClass = UserGroupAction.class)
//    @Enumerated(EnumType.STRING)
//    @CollectionTable(name = "UserGroup_UserGroupAction")
//    @Column(name = "action", nullable = true)
//    private List<UserGroupAction> actions = new ArrayList();
    @Column(nullable = false)
    private String role;

    protected Long getInitialVersion() {
        return Long.valueOf(0L);
    }

    public UserGroup() {
    }

    public UserGroup(String group) {
        this.role = group;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Permission> getPermissions() {
        return this.permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions.clear();
        if(permissions != null) {
            this.permissions.addAll(permissions);
        }

    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UserGroup [role=").append(this.role).append("]");
        return builder.toString();
    }

    public int hashCode() {
        return super.hashCode();
    }

    public boolean equals(Object obj) {
        if(!super.equals(obj)) {
            return false;
        } else {
            UserGroup other = (UserGroup)obj;
            if(this.role == null) {
                if(other.role != null) {
                    return false;
                }
            } else if(!this.role.equals(other.role)) {
                return false;
            }

            return true;
        }
    }

    public List<User> getUsers() {
        return this.users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

//    public List<UserGroupAction> getActions() {
//        return this.actions;
//    }
//
//    public void setActions(List<UserGroupAction> actions) {
//        this.actions = actions;
//    }
}