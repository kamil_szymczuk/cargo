package pl.ihh.cargo.users;

import com.google.common.base.Preconditions;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;
import pl.ihh.cargo.domain.TrackedEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name="USERS")
public class User extends TrackedEntity implements Serializable {

    @Transient
    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name="user_id")
    private Set<Role> roles = new HashSet<>();

    @Column(columnDefinition = "BIT", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean enabled;

    @Column
    private String code;

    @Column(nullable = true)
    private String registrationNumber;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = true)
    private String financeAccount;

    @Column(nullable = true)
    private String costAccount;

    @Column(nullable = true)
    private String phoneNumber;

    @Column(nullable = true)
    private String faxNumber;

    @Column(nullable = true)
    private String email;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<UserGroup> groups;

    public User() {
        this.enabled = Boolean.FALSE;
        this.groups = new ArrayList<>();
    }

    public User(String username, String password, Boolean enabled) {
        this();
        Preconditions.checkNotNull(username);
        Preconditions.checkNotNull(password);
        Preconditions.checkNotNull(enabled);
        Preconditions.checkArgument(!username.isEmpty());
        Preconditions.checkArgument(!password.isEmpty());
        this.username = username;
        this.password = password;
        this.enabled = enabled;
    }

    protected Long getInitialVersion() {
        return Long.valueOf(0L);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username.trim();
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<UserGroup> getGroups() {
        return this.groups;
    }

    public void setGroups(List<UserGroup> groups) {
        this.groups.clear();
        if(groups != null) {
            this.groups.addAll(groups);
        }

    }

    public String toString() {
        return "User [username=" + this.username + ", enabled=" + this.enabled + "]";
    }

    public int hashCode() {
        return super.hashCode();
    }

    public boolean equals(Object obj) {
        if(!super.equals(obj)) {
            return false;
        } else {
            User other = (User)obj;
            if(this.getUsername() == null) {
                if(other.getUsername() != null) {
                    return false;
                }
            } else if(!this.getUsername().equals(other.getUsername())) {
                return false;
            }

            return true;
        }
    }

    public void addGroup(UserGroup group) {
        this.getGroups().add(group);
        group.getUsers().add(this);
    }

    public void removeGroup(UserGroup group) {
        group.getUsers().remove(this);
        this.getGroups().remove(group);
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRegistrationNumber() {
        return this.registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCostAccount() {
        return this.costAccount;
    }

    public void setCostAccount(String costAccount) {
        this.costAccount = costAccount;
    }

    public String getFinanceAccount() {
        return this.financeAccount;
    }

    public void setFinanceAccount(String financeAccount) {
        this.financeAccount = financeAccount;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return this.faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
