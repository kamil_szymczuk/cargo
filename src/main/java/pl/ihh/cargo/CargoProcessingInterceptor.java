package pl.ihh.cargo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import antlr.StringUtils;
import pl.ihh.cargo.users.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kuta on 21.02.2016.
 */

public class CargoProcessingInterceptor implements HandlerInterceptor {

//	@Autowired
//	private SessionBean sessionBean;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		System.out.println("Pre-handle");

		String subMenuID = "";
		String menuID = "";

		// declare patterns
		final String contextPrefix = "^(?:/[\\S^/]+)?";
		Pattern customerPattern = Pattern.compile(contextPrefix
				+ "/mvc/(?<subMenuID>facilities|generalfacilities|wastecodes|extendedcodes|businesswastecodes|waste-management/transfers|changeProtocols|limits/storeTime|limits|wasteCodeGroups|fuel/storage|fuel/operation|fuel/registration|fuel/cauldron/registration)/?.*");
		Pattern productPattern = Pattern.compile(contextPrefix + "/mvc/fees/(?<subMenuID>environmentalfee|fuelenginefee|fuelcauldronfee)/?.*");

		String requestURL = request.getRequestURI();
		Matcher m;
		Pattern[] patterns = { customerPattern, productPattern };
		String[] menuIDs = { "customerMenu", "productMenu" };

		for (int i = 0; i < patterns.length; i++) {
			m = patterns[i].matcher(requestURL);
			if (m.matches()) {
				if (m.groupCount() > 0) { // omit the namedGroup if there is no
											// groups (arrivalFlowPattern)
					subMenuID = m.group("subMenuID");
				}
				menuID = menuIDs[i];
				break;
			}
		}

		request.setAttribute("subMenuID", subMenuID);
		request.setAttribute("menuID", menuID);

		// if returned false, we need to make sure 'response' is sent
		return true;

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		System.out.println("Post-handle");
		
//		request.setAttribute("appTitle", sessionBean.getAppTitle());
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		System.out.println("After completion handle");
	}

}