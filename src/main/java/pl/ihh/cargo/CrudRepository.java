package pl.ihh.cargo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kuta on 20.02.2016.
 */

@NoRepositoryBean
public interface CrudRepository<T, ID extends Serializable>  extends Repository<T, ID> {

    Page<T> findAll(Pageable pageable);

    List<T> findAll();

    T findById(ID id);

    <S extends T> S save(S entity);

    T findOne(ID primaryKey);

    Long count();

    void delete(T entity);

    void deleteById(ID id);

    boolean exists(ID primaryKey);

}
