<div class="panel-heading">
    <div class="row">
        <div class="col-lg-3" style="float: left">
            <select class="form-control" ng-model="ctrl.pagingOptions.pageSize" ng-change="ctrl.fetchAll()">
                <option ng-selected="ctrl.pagingOptions.pageSize == size"
                        ng-repeat="size in ctrl.pagingOptions.pageSizes"
                        ng-value="size">{{size}}</option>
            </select>
        </div>
        <div class="col-lg-3 text-right" style="float: right">
            <input id="search" type="text" class="form-control" placeholder="Szukaj..."
                   ng-bind="ctrl.pagingOptions.search"
                   ng-change="ctrl.fetchAll()"
                   ng-model="ctrl.pagingOptions.search">
        </div>
    </div>
</div>