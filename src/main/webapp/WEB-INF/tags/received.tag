<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Product Page template" pageEncoding="UTF-8"%>
<%@attribute name="receptions" required="true" type="java.util.List"%>

<c:forEach items="${receprions}" var="reception">
    <tr>
        <td>${reception.customer}</td>
        <td>${reception.position}</td>
        <td>${reception.product}</td>
        <td>${reception.productType}</td>
        <td>${reception.quantity}</td>
        <td><a href="<c:url value="/reception/${reception.id}"/>">detale</a></td>
    </tr>
</c:forEach>
