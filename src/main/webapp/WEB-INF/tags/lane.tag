<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Product Page template" pageEncoding="UTF-8"%>
<%@attribute name="lane" required="true" type="java.util.List"%>

<c:forEach items="${lanes}" var="lane">
    <tr>
        <td>${lane.name}</td>
        <td>${lane.shortName}</td>
        <td>${lane.position}</td>
        <td><a href="<c:url value="/lane/${lane.id}"/>">detale</a></td>
    </tr>
</c:forEach>
