<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="User Page template" pageEncoding="UTF-8"%>
<%@attribute name="user" required="true" type="java.util.List"%>

<c:forEach items="${users}" var="user">
    <tr>
        <td>${user.lastName}</td>
        <td>${user.firstName}</td>
        <td>${user.password}</td>
        <td>${user.nickname}</td>
        <td><a href="<c:url value="/user/${user.id}"/>">detale</a></td>
    </tr>
</c:forEach>