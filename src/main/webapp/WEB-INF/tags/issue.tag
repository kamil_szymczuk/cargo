<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Position Page template" pageEncoding="UTF-8"%>
<%@attribute name="issue" required="true" type="java.util.List"%>

<c:forEach items="${issues}" var="issue">
    <tr>

        <td>${issue.customer}</td>
        <td>${issue.issueNumber}</td>
        <td>${issue.issueStatus}</td>
        <td>${issue.departureDate}</td>
        <td><a href="<c:url value="/issue/${issue.id}"/>">detale</a></td>
    </tr>
</c:forEach>
