<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="sidebar" pageEncoding="UTF-8" %>
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="home navbar-brand nav-bar-home" href="<c:url value="//"/>"><i class="fa fa-home fa-2x"></i></a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="<c:url value="/user/active/"/>"><i class="fa fa-user fa-fw"></i>User Profile</a>
                </li>
                <li><a href="<c:url value="/user/settings/"/>/user/settings"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="<c:url value="/logout/"/>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->


    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                </li>
                <li><a href="<c:url value="/position/"/>">Magazyn</a></li>
                <li><a href="<c:url value="/product/"/>">Produkty</a></li>
                <%--<li><a href="<c:url value="/reception/"/>">Przyjęcie</a></li>--%>
                <%--<li><a href="<c:url value="/lane/"/>">Magazyn</a></li>--%>
                <li><a href="<c:url value="/productTransfer/"/>">Transfer</a></li>
                <%--<li><a href="<c:url value="/customer/"/>">Klienci</a></li>--%>
                <%--<li><a href="<c:url value="/issue/"/>">Wydanie</a></li>--%>
                <li><a href="<c:url value="/user/"/>">Uzytkownicy</a></li>

            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
