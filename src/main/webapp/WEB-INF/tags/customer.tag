<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Customer Page template" pageEncoding="UTF-8"%>
<%@attribute name="customers" required="true" type="java.util.List"%>

<c:forEach items="${customers}" var="customer">
    <tr>
        <td>${customer.fullName}</td>
        <td>${customer.shortName}</td>
        <td>${customer.nipNumber}</td>
        <td>${customer.regonNumber}</td>
        <td>${customer.phoneNumber}</td>
        <td>${customer.email}</td>
        <td>${customer.notes}</td>
        <td><a href="<c:url value="/customer/${customer.id}"/>">detale</a></td>
    </tr>
</c:forEach>
