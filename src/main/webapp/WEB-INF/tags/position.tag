<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Position Page template" pageEncoding="UTF-8"%>
<%@attribute name="position" required="true" type="java.util.List"%>

<c:forEach items="${positions}" var="position">
    <tr>
        <td>${position.name}</td>
        <td>${position.quantity}</td>
        <td>${position.product}</td>
        <td>${position.lane}</td>
        <td><a href="<c:url value="/position/${position.id}"/>">detale</a></td>
    </tr>
</c:forEach>
