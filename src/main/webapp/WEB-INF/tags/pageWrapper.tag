<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@attribute name="header" fragment="true"%>
<%@attribute name="footer" fragment="true"%>
<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 20.02.2016
  Time: 03:46
  To change this template use File | Settings | File Templates.

--%>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>${appTitle}</title>
<base href="<c:url value="/"/>" />
<c:url var="kendo_bootstrap_css" value="/resources/plugins/kendoui.core/styles/kendo.common-bootstrap.min.css" />
<c:url var="kendo_bootstrap_min_css" value="/resources/plugins/kendoui.core/styles/kendo.bootstrap.min.css" />
<c:url var="font_awesome" value="/resources/plugins/font-awesome/css/font-awesome.min.css" />
<c:url var="bootstrap" value="/resources/plugins/bootstrap/dist/css/bootstrap.min.css" />
<c:url var="metisMenu" value="/resources/bower-components/metisMenu/dist/metisMenu.min.css" />
<c:url var="styles" value="/resources/css/styles.css" />

<c:url var="jquery" value="/resources/jquery/2.2/jquery-2.2.0.min.js" />
<c:url var="bootstrap_js" value="/resources/js/bootstrap.min.js" />
<c:url var="angular" value="/resources/plugins/angular-1.5.2/angular.min.js" />

<c:url var="kendo_ui_core_min_js" value="/resources/plugins/kendoui.core/js/kendo.ui.core.min.js" />
<c:url var="angular_cookies" value="/resources/plugins/angular-1.5.2/angular-cookies.min.js" />
<c:url var="angular_route" value="/resources/plugins/angular-1.5.2/angular-route.min.js" />
<c:url var="cargoApp" value='/resources/angular/cargoApp.js' />
<c:url var="cargo_js" value="/resources/dist/cargo.js" />
<c:url var="dirPagination" value='/resources/angular/assets/js/dirPagination.js' />
<c:url var="angular_select" value='/resources/angular/assets/js/select/select.js' />
<c:url var="angular_choosen" value='/resources/angular/assets/js/angular-chosen-1.3.0/dist/angular-chosen.js' />
<c:url var="metisMenu_js" value="/resources/bower-components/metisMenu/dist/metisMenu.js" />

<link href="${bootstrap}" rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="${metisMenu}" rel="stylesheet">
<link href="${font_awesome}" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="${kendo_bootstrap_css}">
<link rel="stylesheet" href="${kendo_bootstrap_min_css}">

<link href="${styles}" rel="stylesheet">

<%--<!--[if lt IE 9]>--%>
<%--<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>--%>
<%--<![endif]-->--%>

<%--<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->--%>
<%--<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->--%>
<%--<!--[if lt IE 9]>--%>
<%--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>--%>
<%--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>--%>
<%--<![endif]-->--%>

<%--<!-- jQuery -->--%>
<script src="${jquery}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${bootstrap_js}"></script>

<script type="text/javascript" src="${angular}"></script>
<script type="text/javascript" src="${angular_cookies}"></script>
<script type="text/javascript" src="${angular_route}"></script>
<script type="text/javascript" src="${dirPagination}"></script>
<script type="text/javascript" src="${angular_select}"></script>
<script type="text/javascript" src="${angular_choosen}"></script>
<script type="text/javascript" src="${cargoApp}"></script>

</head>
<body>
	<div id="wrapper" ng-app="cargoApp">
		<tags:navigation2 />
		<div id="page-wrapper">
			<jsp:invoke fragment="header" />
			<jsp:doBody />
			<jsp:invoke fragment="footer" />
		</div>
		<div id="footer" class="text-center">
			<span>&copy; 2016 IhhCargo by IHH</span>
		</div>
	</div>
	<!-- /#wrapper -->

	<%--<!-- Metis Menu Plugin JavaScript -->--%>
	<script src="${metisMenu_js}"></script>
	<!-- Custom Theme JavaScript -->
	<script src="${cargo_js}"></script>
	<script src="${kendo_ui_core_min_js}"></script>
</body>
</html>
