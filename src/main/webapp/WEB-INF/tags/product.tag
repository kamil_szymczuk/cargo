<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Product Page template" pageEncoding="UTF-8"%>
<%@attribute name="product" required="true" type="java.util.List"%>

<c:forEach items="${products}" var="product">
    <tr>
        <td>${product.subcode}</td>
        <td>${product.description}</td>
        <td><a href="<c:url value="/product/${product.id}"/>">detale</a></td>
    </tr>
</c:forEach>
