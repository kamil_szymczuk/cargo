<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag description="Customer Page template" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@attribute required="true" type="java.lang.String" name="path"%>
<%@attribute required="true" type="java.lang.String" name="msg"%>
<%@attribute required="false" type="java.lang.Boolean" name="readonly"%>
<c:set var="readonly" value="${readonly != null ? readonly : false}"/>

<form:label cssclass="control-lable" path="${path}"> <fmt:message key="${msg}"/></form:label>
<form:input cssClass="form-control" readonly="${readonly}" path="${path}"/>