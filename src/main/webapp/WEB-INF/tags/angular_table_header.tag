<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@tag description="table header" pageEncoding="UTF-8"%>

<%@attribute required="true" type="java.lang.String" name="field" %>
<%@attribute required="true" type="java.lang.String" name="key" %>
<%@attribute required="false" type="java.lang.String" name="sorting" %>
<%@attribute required="false" type="java.lang.String" name="order"%>

<c:set var="sorting" value="${sorting != null ? sorting : 'ctrl.sortingOrder'}"/>
<c:set var="order" value="${order != null ? order : 'ctrl.reverse'}"/>

<th ng-click="sort_by('${field}')">
    <span class="text-center"><fmt:message key="${key}"/></span>
    <span class="pull-right glyphicon sort-icon"
          ng-show="${sorting}=='${field}'"
          ng-class="{'glyphicon-chevron-up':${order},'glyphicon-chevron-down':!${order}}"></span>
</th>