<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 17.03.2016
  Time: 20:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:pageWrapper>
	<h2>Uzytkownik</h2>
	<div class="table-responsive" ng-controller="UserDetailController as ctrl">
		<div class="generic-container">
			<div class="panel panel-default">
				<div class="panel-body">

					<div class="tab-content">
						<div class="tab-pane active" id="details">
							<form:form commandName="user">
								<form:hidden path="id" />
								<form:hidden path="version" />
								<%--<form:hidden path="creationUser"/>--%>
								<%--<form:hidden path="creationDate"/>--%>
								<div class="row">
									<div class="col-lg-3">
										<div class="form-group">
											<form:label cssclass="control-lable" path="username">Nick</form:label>
											<form:input cssClass="form-control" readonly="${readonly}" path="username" />
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group">
											<form:label cssclass="control-lable" path="code">Kod</form:label>
											<form:input cssClass="form-control" readonly="${readonly}" path="code" />
										</div>
									</div>
									<c:if test="${empty user.id}">
										<div class="col-lg-3">
											<div class="form-group">
												<form:label cssclass="control-lable" path="password">Hasło</form:label>
												<form:input cssClass="form-control" readonly="${readonly}" path="password" />
											</div>
										</div>
									</c:if>
								</div>
								<div class="row">
									<div class="col-lg-3">
										<div class="form-group">
											<form:label cssclass="control-lable" path="firstName">Imie</form:label>
											<form:input cssClass="form-control" readonly="${readonly}" path="firstName" />
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group">
											<form:label cssclass="control-lable" path="lastName">Nazwisko</form:label>
											<form:input cssClass="form-control" readonly="${readonly}" path="lastName" />
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group">
											<form:label cssclass="control-lable" path="email">E-mail</form:label>
											<form:input cssClass="form-control" readonly="${readonly}" path="email" />
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group">
											<form:label cssclass="control-lable" path="phoneNumber">Nr. telefonu</form:label>
											<form:input cssClass="form-control" readonly="${readonly}" path="phoneNumber" />
										</div>
									</div>

								</div>
								<div class="row">
									<c:url value="/user/" var="listUrl" />
									<c:url value="/user/${user.id}/edit" var="editUrl" />
									<c:choose>
										<c:when test="${readonly}">
											<div class="col-lg-6 col-md-6">
												<a id="back" href="${listUrl}" class="btn btn-default"> <i class="icon-chevron-left"></i> Wstecz <%--<fmt:message key="general.label.back"/>--%>
												</a>
											</div>
											<div class="col-lg-6 col-md-6 text-right">
												<a href="${editUrl}" class="btn btn-primary"> <i class="icon-edit"></i> Edycja <%--<fmt:message key="general.label.edit"/>--%>
												</a>
											</div>
										</c:when>
										<c:otherwise>
											<div class="col-lg-6 col-md-6">
												<a href="${listUrl}" class="btn btn-default"> <i class="icon-chevron-left"></i> Anuluj <%--<fmt:message key="general.button.cancel"/>--%>
												</a>
											</div>
											<div class="col-lg-6 col-md-6 text-right">
												<button type="submit" class="btn btn-success">
													<i class="icon-save"></i> Zapisz
													<%--<fmt:message key="general.button.save"/>--%>
												</button>
											</div>
										</c:otherwise>
									</c:choose>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		
	<%--$(function () {--%>
		
	<%--$("#productType").kendoComboBox({--%>
		
	<%--placeholder: '${pleaseSelect}',--%>
		
	<%--text: '${product.productType.baseCode}',--%>
		
	<%--filter: 'contains',--%>
		
	<%--dataTextField: 'baseCode',--%>
		
	<%--dataValueField: 'id',--%>
		
	<%--autoBind: false,--%>
		
	<%--dataSource: {--%>
		
	<%--dataType: "json",--%>
		
	<%--serverFiltering: true,--%>
		
	<%--transport: {--%>
		
	<%--read: {--%>
		
	<%--url: "<c:url value="/productType/api/find"/>",--%>
		
	<%--data: function (e) {--%>
		
	<%--return {--%>
		
	<%--term: (e['filter'] && e['filter']['filters']) ? e.filter.filters[0].value : null--%>
		
	<%--}--%>
		
	<%--},--%>
		
	<%--type: 'POST'--%>
		
	<%--}--%>
		
	<%--},--%>
		
	<%--schema: {--%>
		
	<%--data: "itemList",--%>
		
	<%--parse: function (response) {--%>
		
	<%--$.each(response.itemList, function (idx, elem) {--%>
		
	<%--elem.baseCode = elem.label + (elem.extra != null ? (" - " + elem.extra) : "" );--%>
		
	<%--});--%>
		
	<%--return response;--%>
		
	<%--}--%>
		
	<%--}--%>
		
	<%--}--%>
		
	<%--});--%>
		
	<%--});--%>
		
	</script>


</t:pageWrapper>