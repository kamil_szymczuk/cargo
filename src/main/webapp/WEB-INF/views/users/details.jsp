<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div ng-app="cargoApp">
            <h2>Użytkownicy</h2>
            <div class="table-responsive" ng-controller="UserController as ctrl">
                <div class="generic-container">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form id="editForm" ng-submit="ctrl.submit()" name="myForm" class="form-vertical">

                                <input type="hidden" ng-model="ctrl.selected.id"/>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-lable" for="code">Kod</label>
                                            <input type="text" ng-model="ctrl.user.code" id="code"
                                                   class="form-control" placeholder="Kod" required
                                                   />
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label class="control-lable" for="costAccount">cost Acc</label>
                                            <input type="text" ng-model="ctrl.user.costAccount" id="costAccount"
                                                   class="form-control"
                                                   placeholder="cost Acc"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label class="control-lable" for="email">email</label>
                                            <input type="text" ng-model="ctrl.user.costAccount" id="email"
                                                   class="form-control"
                                                   placeholder="email"/>
                                        </div>
                                    </div>

                                        <%--<div class="form-group col-lg-6">--%>
                                        <%--<div class="form-group ">--%>
                                        <%--<label class="control-lable" for="enabled">enabled</label>--%>
                                        <%--<input type="text" ng-model="ctrl.user.costAccount" id="enabled"--%>
                                        <%--class="form-control"--%>
                                        <%--placeholder="enabled"/>--%>
                                        <%--</div>--%>
                                        <%--</div>--%>

                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label class="control-lable" for="faxNumber">Fax</label>
                                            <input type="text" ng-model="ctrl.user.faxNumber" id="faxNumber"
                                                   class="form-control"
                                                   placeholder="fax"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label class="control-lable" for="financeAccount">finance Acc</label>
                                            <input type="text" ng-model="ctrl.user.financeAccount" id="financeAccount"
                                                   class="form-control"
                                                   placeholder="finance Acc"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label class="control-lable" for="firstName">Imię</label>
                                            <input type="text" ng-model="ctrl.user.firstName" id="firstName"
                                                   class="form-control"
                                                   placeholder="Imię"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label class="control-lable" for="lastName">Nazwisko</label>
                                            <input type="text" ng-model="ctrl.user.lastName" id="lastName"
                                                   class="form-control"
                                                   placeholder="Nazwisko"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label class="control-lable" for="password">Hasło</label>
                                            <input type="text" ng-model="ctrl.user.password" id="password"
                                                   class="form-control"
                                                   placeholder="Hasło"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label class="control-lable" for="phoneNumber">Nr. telefonu</label>
                                            <input type="text" ng-model="ctrl.user.phoneNumber" id="phoneNumber"
                                                   class="form-control"
                                                   placeholder="Nr. telefonu"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-lable" for="registrationNumber">Nr. rejestracji</label>
                                            <input type="text" ng-model="ctrl.user.registrationNumber" id="registrationNumber"
                                                   class="form-control"
                                                   placeholder="Nr. rejestracji"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-lable" for="username">Nick</label>
                                            <input type="text" ng-model="ctrl.user.username" id="username"
                                                   class="form-control"
                                                   placeholder="Nick"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="text-right">
                                            <button type="button" ng-click="ctrl.reset()" class="btn btn-danger"
                                                    ng-disabled="myForm.$pristine">{{!ctrl.user.id ? 'Anuluj' : 'Cofnij'}}
                                            </button>
                                            <button type="submit" class="btn btn-primary"
                                                    ng-disabled="myForm.$invalid">{{!ctrl.user.id ? 'Dodaj' : 'Zapisz'}}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <form:form method="POST" ng-controller="UserController as ctrl" action="${saveUrl}"
                   modelAttribute="user">
            <form:hidden path="id"/>
            <div class="col-lg-3">
                <div class="form-group ">
                    <form:label class="control-lable" path="firstName">Imię</form:label>
                    <form:input class="form-control" readonly="${readonly}" path="firstName"/>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group ">
                    <form:label class="control-lable" path="lastName">Nazwisko</form:label>
                    <form:input class="form-control" readonly="${readonly}" path="lastName"/>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group ">
                    <form:label class="control-lable" path="password">Hasło</form:label>
                    <form:input class="form-control" readonly="${readonly}" path="password"/>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group ">
                    <form:label class="control-lable" path="username">Nick</form:label>
                    <form:input class="form-control" readonly="${readonly}" path="username"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group ">
                    <button type="submit" class="btn btn-primary">Zapisz</button>
                </div>
            </div>
        </form:form>

        <script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
        <script type="text/javascript">
            var App = angular.module('cargoApp', []).directive('chosen', function () {
                var linker = function (scope, element, attr) {
                    // update the select when data is loaded
                    scope.$watch(attr.chosen, function (oldVal, newVal) {
                        element.trigger('chosen:updated');
                    });

                    // update the select when the model changes
                    scope.$watch(attr.ngModel, function () {
                        element.trigger('chosen:updated');
                    });

                    element.chosen();
                };

                return {
                    restrict: 'A',
                    link: linker
                }
            });
        </script>
        <script src="<c:url value='/resources/angular/user/user_controller.js' />"></script>
        <script src="<c:url value='/resources/angular/user/user_service.js' />"></script>
        <script src="<c:url value='/resources/angular/user/users.js' />"></script>
    </jsp:body>
</t:pageWrapper>


