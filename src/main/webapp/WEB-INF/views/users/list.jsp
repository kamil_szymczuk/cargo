<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>


<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div class="col-lg-12">
                <h2>Użytkownicy</h2>
            </div>
        </div>
        <div class="row">
            <div class="panel-body">
                <a href="<c:url value="/user/add"/> " class="btn btn-primary">
                    <i class="icon-edit"></i>
                    <fmt:message key="general.label.add"/>
                </a>
            </div>
        </div>
        <div class="panel panel-default" ng-controller="UserController as ctrl">
            <t:tableHeading/>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <th ng-click="sort_by('id')">Akcje
                                            <span class="glyphicon sort-icon"
                                                  ng-show="sortKey=='id'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th ng-click="sort_by('username')">Login
                                            <span class="glyphicon sort-icon"
                                                  ng-show="sortKey=='username'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th ng-click="sort_by('firstName')">Imie
                                            <span class="glyphicon sort-icon"
                                                  ng-show="sortKey=='firstName'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th ng-click="sort_by('lastName')">Nazwisko
                                            <span class="glyphicon sort-icon"
                                                  ng-show="sortKey=='lastName'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-show="ctrl.users.length <= 0">
                            <td colspan="12" style="text-align:center;">Ładowanie</td>
                        </tr>
                        <tr dir-paginate="u in ctrl.users | filter : search |itemsPerPage:ctrl.userPageData.size"
                            total-items="ctrl.userPageData.totalElements">
                            <td class="col-sm-1">
                                <a href="<c:url value="/user/"/>{{u.id}}" class="btn btn-primary btn-xs">
                                    <i class="icon-edit"></i>
                                    <fmt:message key="general.label.details"/>
                                </a>
                            </td>
                            <td><span ng-bind="u.username"></span></td>
                            <td><span ng-bind="u.firstName"></span></td>
                            <td><span ng-bind="u.lastName"></span></td>
                        </tr>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6 text-right ng-hide" ng-show="ctrl.users.length > 0" colspan="6">
                        <dir-pagination-controls
                                max-size="8"
                                direction-links="true"
                                auto-hide="false"
                                boundary-links="false"
                                on-page-change="setPage(newPageNumber)">
                        </dir-pagination-controls>
                    </div>
                </div>
            </div>
        </div>
        <!-- ASSETS -->
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".clickable-row").click(function () {
                    window.document.location = $(this).data("href");
                });

            });
        </script>
        <script src="<c:url value='/resources/angular/user/user_controller.js' />"></script>
        <script src="<c:url value='/resources/angular/user/user_service.js' />"></script>
        <script src="<c:url value='/resources/angular/user/users.js' />"></script>
    </jsp:body>
</t:pageWrapper>