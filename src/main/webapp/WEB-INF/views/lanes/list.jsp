<%--
  Created by IntelliJ IDEA.
  User: risen16
  Date: 07.03.2016
  Time: 12:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div class="col-lg-12">
                <h2>Alejki</h2>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-body">
                <a href="<c:url value="/lane/add"/> " class="btn btn-primary">
                    <i class="icon-edit"></i>
                    <fmt:message key="general.label.add"/>
                </a>
            </div>
        </div>
        <div class="panel panel-default" ng-controller="LaneController as ctrl">
            <t:tableHeading/>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12 table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th ng-click="sort_by('id')">Akcje
                                            <span class="glyphicon sort-icon"
                                                  ng-show="sortKey=='id'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort_by('name')">Nazwa
                                            <span class="glyphicon sort-icon"
                                                  ng-show="sortKey=='name'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort_by('shortName')">Opis
                                            <span class="glyphicon sort-icon"
                                                  ng-show="sortKey=='shortName'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-show="ctrl.lanes.length <= 0">
                                <td colspan="5" style="text-align:center;">Ładowanie</td>
                            </tr>
                            <tr dir-paginate="u in ctrl.lanes | filter : search |itemsPerPage:ctrl.lanePageData.size"
                                total-items="ctrl.lanePageData.totalElements"
                                ng-click="ctrl.edit(u.id)">
                                <td class="col-sm-1">
                                    <a href="<c:url value="/lane/"/>{{u.id}}" class="btn btn-primary btn-xs">
                                        <i class="icon-edit"></i>
                                        <fmt:message key="general.label.details"/>
                                    </a>
                                </td>
                                <td><span ng-bind="u.name"></span></td>
                                <td><span ng-bind="u.shortName"></span></td>
                            </tr>
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6 text-right ng-hide" ng-show="ctrl.products.length > 0" colspan="6">
                        <dir-pagination-controls
                                max-size="8"
                                direction-links="true"
                                auto-hide="false"
                                boundary-links="false"
                                on-page-change="setPage(newPageNumber)">
                        </dir-pagination-controls>
                    </div>
                </div>
            </div>
        </div>
        <!-- ASSETS -->
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".clickable-row").click(function () {
                    window.document.location = $(this).data("href");
                });

            });
        </script>
        <script src="<c:url value='/resources/angular/lane/lane_controller.js' />"></script>
        <script src="<c:url value='/resources/angular/lane/lane_service.js' />"></script>

    </jsp:body>
</t:pageWrapper>