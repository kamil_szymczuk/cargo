<%--
  Created by IntelliJ IDEA.
  User: risen16
  Date: 07.03.2016
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<set attributeName="readonly" value="${true}"/>
<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div>
            <h2>Alejki</h2>
                <div class="table-responsive" ng-controller="LaneController as ctrl">
                    <div class="generic-container">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form id="editForm" ng-submit="ctrl.submit()" name="myForm" class="form-vertical">
                                    <input type="hidden" ng-model="ctrl.selected.id"/>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="control-lable" for="name">Nazwa</label>
                                            <input type="text" ng-model="ctrl.lane.name" id="name"
                                                   class="form-control" placeholder="Nazwa" required
                                                   ng-minlength="3"/>
                                            <div class="has-error" ng-show="myForm.$dirty">
                                                <span ng-show="myForm.uname.$error.required">This is a required field</span>
                                                <span ng-show="myForm.uname.$error.minlength">Minimum length required is 3</span>
                                                <span ng-show="myForm.uname.$invalid">This field is invalid </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="control-lable" for="shortName">Opis</label>
                                            <input type="text" ng-model="ctrl.lane.shortName" id="shortName"
                                                   class="form-control"
                                                   placeholder="Opis Alejki"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group text-right">
                                            <button type="button" ng-click="ctrl.reset()" class="btn btn-danger"
                                                    ng-disabled="myForm.$pristine">{{!ctrl.selected.id ? 'Anuluj' :
                                                'Cofnij'}}
                                            </button>
                                            <button type="submit" class="btn btn-primary"
                                                    ng-disabled="myForm.$invalid">{{!ctrl.selected.id ? 'Dodaj' : 'Zapisz'}}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


            <form:form method="POST" ng-controller="LaneController as ctrl" action="${saveUrl}"
                       modelAttribute="lane">
                <form:hidden path="id"/>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="name">Nazwa</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="name"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="shortName">Opis</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="shortName"/>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group ">
                        <button type="submit" class="btn btn-primary">Zapisz</button>
                    </div>
                </div>
            </form:form>
            <script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
            <script type="text/javascript">
                var App = angular.module('cargoApp', []).directive('chosen', function () {
                    var linker = function (scope, element, attr) {
                        // update the select when data is loaded
                        scope.$watch(attr.chosen, function (oldVal, newVal) {
                            element.trigger('chosen:updated');
                        });

                        // update the select when the model changes
                        scope.$watch(attr.ngModel, function () {
                            element.trigger('chosen:updated');
                        });

                        element.chosen();
                    };

                    return {
                        restrict: 'A',
                        link: linker
                    }
                });
            </script>
        <script src="<c:url value='/resources/angular/lane/lane_controller.js' />"></script>
        <script src="<c:url value='/resources/angular/lane/lane_service.js' />"></script>
        </div>
    </jsp:body>
</t:pageWrapper>

