<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 17.03.2016
  Time: 20:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:pageWrapper>
    <h2>Alejka</h2>
    <div class="table-responsive" ng-controller="LaneDetailController as ctrl">
        <div class="generic-container">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="details">
                            <form:form commandName="lane">
                                <form:hidden path="id"/>
                                <form:hidden path="version"/>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <t:ihh_form path="name" msg="general.label.name" readonly="${readonly}"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <t:ihh_form path="shortName" msg="general.label.shortName"
                                                        readonly="${readonly}"/>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="panel panel-body">
                                        <a href="<c:url value="/position/add?lane=${lane.id}"/> "
                                           class="btn btn-primary">
                                            <i class="icon-edit"></i>
                                            <fmt:message key="position.add"/>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <t:tableHeading/>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover">
                                                <t:angular_table_header field="position" key="position.label"/>
                                                <t:angular_table_header field="product.subcode" key="product.label"/>
                                                <t:angular_table_header field="quantity" key="general.label.quantity"/>
                                                <t:angular_table_header field="position" key="general.label.actions"/>
                                                <c:forEach items="${lane.positions}" var="position">
                                                    <tr>
                                                        <td>${position.name}</td>
                                                        <td>${position.product.subcode}</td>
                                                        <td>${position.quantity}</td>
                                                        <td><c:url value="/position/" var="listUrl"/>
                                                            <c:url value="/position/${position.id}" var="editUrl"/>
                                                            <c:choose>
                                                                <c:when test="${readonly}">
                                                                    <div class="col-lg-6 col-md-6 text-right">
                                                                        <a href="${editUrl}" class="btn btn-primary">
                                                                            <i class="icon-edit"></i>
                                                                            <fmt:message key="general.label.details"/>
                                                                        </a>
                                                                    </div>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <div class="col-lg-6 col-md-6">
                                                                        <a href="${listUrl}" class="btn btn-default">
                                                                            <i class="icon-chevron-left"></i>
                                                                            <fmt:message key="general.button.cancel"/>
                                                                        </a>
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6 text-right">
                                                                        <button type="submit" class="btn btn-success">
                                                                            <i class="icon-save"></i>
                                                                            <fmt:message key="general.button.save"/>
                                                                        </button>
                                                                    </div>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
    </script>


</t:pageWrapper>