<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 21.02.2016
  Time: 01:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<t:pageWrapper>
    <jsp:attribute name="header">
        <div class="row">
            <div class="col-lg-12">
                <h2>Pulpit</h2>
                <p>zarządzanie ruchem produktów w magazynach</p>
            </div>
        </div>
         <div class="row">
             <div class="col-lg-3 col-md-6">
                 <div class="panel panel-green">
                     <div class="panel-heading">
                         <div class="row">
                             <div class="col-xs-3">
                                 <i class="fa fa-shopping-cart fa-5x"></i>
                             </div>
                             <div class="col-xs-9 text-right">
                                 <div>Towar</div>
                             </div>
                         </div>
                     </div>
                     <a href="<c:url value='/product/add'/>">
                         <div class="panel-footer">
                             <span class="pull-left">Dodaj nowy</span>
                             <span class="pull-right"><i class="fa fa-plus-circle"></i></span>
                             <div class="clearfix"></div>
                         </div>
                     </a>
                 </div>
             </div>
         </div>

    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>

    </jsp:body>
</t:pageWrapper>