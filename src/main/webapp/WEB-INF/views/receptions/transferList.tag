<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 19.03.2016
  Time: 13:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@attribute required="false" type="java.util.ArrayList" name="transfers"%>
<%@attribute required="false" type="pl.ihh.cargo.domain.productTransfers.ProductTransfer" name="newItem"%>

<c:forEach items="${transfers}" var="transfer">

</c:forEach>
<form:form method="POST" action="${saveUrl}" modelAttribute="newItem">
    <form:hidden path="id"/>
    <table>
        <tr>
            <td><form:label path="positionDestination">stanowisko docelowe</form:label></td>
            <td><form:input readonly="${readonly}" path="positionDestination"/></td>
        </tr>
        <tr>
            <td><form:label path="positionSource">źródło pozycji</form:label></td>
            <td><form:input readonly="${readonly}" path="positionSource"/></td>
        </tr>
        <tr>
            <td><form:label path="productTransferStatus">status transferu</form:label></td>
            <td><form:input readonly="${readonly}" path="productTransferStatus"/></td>
        </tr>
        <tr>
            <td><form:label path="quantity">ilość</form:label></td>
            <td><form:input readonly="${readonly}" path="quantity"/></td>
        </tr>

        <tr>
            <td><input type="submit" value="Zapisz"/></td>
        </tr>
    </table>
</form:form>


<table id="transfers">
    <tr>
        <td>Row1 cell1</td>
        <td>Row1 cell2</td>
    </tr>
    <tr>
        <td>Row2 cell1</td>
        <td>Row2 cell2</td>
    </tr>
    <tr>
        <td>Row3 cell1</td>
        <td>Row3 cell2</td>
    </tr>
</table>
<br>

<button onclick="myFunction()">Try it</button>

<script>
    function myFunction() {
        var table = document.getElementById("transfers");
        var row = table.insertRow(0);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        cell1.innerHTML = "NEW CELL1";
        cell2.innerHTML = "NEW CELL2";
    }
</script>
