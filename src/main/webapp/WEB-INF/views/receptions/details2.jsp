<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 17.03.2016
  Time: 20:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:pageWrapper>
    <h2>Przyjęcie towaru</h2>
    <div class="table-responsive" ng-controller="ReceptionDetailController as ctrl">
        <div class="generic-container">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="details">
                            <form:form commandName="reception">
                                <form:hidden path="id"/>
                                <form:hidden path="version"/>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <form:label cssclass="control-lable" path="customer">Dostawca</form:label>
                                            <form:input cssClass="form-control" readonly="${readonly}"
                                                        path="${readonly == true ? 'customer.fullName' : 'customer'}"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                                <%--<form:label cssclass="control-lable" path="position">Pozycja</form:label>--%>
                                                <%--<form:input cssClass="form-control" readonly="${readonly}"--%>
                                                <%--path="${readonly == true ? 'position.fullName' : 'position'}"/>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                                <%--<form:label cssclass="control-lable" path="product">Produkt</form:label>--%>
                                                <%--<form:input cssClass="form-control" readonly="${readonly}"--%>
                                                <%--path="${readonly == true ? 'product.subcode' : 'product'}"/>--%>
                                        </div>

                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                                <%--<c:if test="${reception.product != null}">--%>

                                                <%--<form:label cssclass="control-lable"--%>
                                                <%--path="product.productType">Typ produktu</form:label>--%>
                                                <%--<form:input cssClass="form-control" readonly="true"--%>
                                                <%--path="product.productType"/>--%>
                                                <%--</c:if>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                                <%--<form:label cssclass="control-lable" path="quantity">Ilosc</form:label>--%>
                                                <%--<form:input cssClass="form-control" readonly="${readonly}"--%>
                                                <%--path="quantity"/>--%>
                                        </div>

                                    </div>
                                </div>
                                <%--<div class="row">--%>
                                <%--<c:url value="/reception/" var="listUrl"/>--%>
                                <%--<c:url value="/reception/${reception.id}/edit" var="editUrl"/>--%>
                                <%--<c:choose>--%>
                                <%--<c:when test="${readonly}">--%>
                                <%--<div class="col-lg-6 col-md-6">--%>
                                <%--<a id="back" href="${listUrl}" class="btn btn-default">--%>
                                <%--<i class="icon-chevron-left"></i>--%>
                                <%--<fmt:message key="general.label.back"/>--%>
                                <%--</a>--%>
                                <%--</div>--%>
                                <%--<div class="col-lg-6 col-md-6 text-right">--%>
                                <%--<a href="${editUrl}" class="btn btn-primary">--%>
                                <%--<i class="icon-edit"></i>--%>
                                <%--<fmt:message key="general.label.edit"/>--%>
                                <%--</a>--%>
                                <%--</div>--%>
                                <%--</c:when>--%>
                                <%--<c:otherwise>--%>
                                <%--<div class="col-lg-6 col-md-6">--%>
                                <%--<a href="${listUrl}" class="btn btn-default">--%>
                                <%--<i class="icon-chevron-left"></i>--%>
                                <%--<fmt:message key="general.button.cancel"/>--%>
                                <%--</a>--%>
                                <%--</div>--%>
                                <%--<div class="col-lg-6 col-md-6 text-right">--%>
                                <%--<button type="submit" class="btn btn-success">--%>
                                <%--<i class="icon-save"></i>--%>
                                <%--<fmt:message key="general.button.save"/>--%>
                                <%--</button>--%>
                                <%--</div>--%>
                                <%--</c:otherwise>--%>
                                <%--</c:choose>--%>
                                <%--</div>--%>
                                <br>
                                <div class="row">
                                    <div class="panel panel-body">
                                        <a href="<c:url value="/productTransfer/add"/> " class="btn btn-primary">
                                            <i class="icon-edit"></i>
                                            <fmt:message key="general.label.add"/>
                                        </a>
                                        nowy transfer
                                    </div>
                                </div>

                                <div class="row">
                                    <table class="table table-striped table-bordered table-hover">
                                        <t:angular_table_header field="productTransfer.createdAt"
                                                                key="general.label.createdAt"/>
                                        <t:angular_table_header field="product.subcode" key="product.label"/>
                                        <t:angular_table_header field="quantity" key="general.label.quantity"/>
                                        <t:angular_table_header field="positionSource" key="positionSource.label"/>
                                        <t:angular_table_header field="positionDestination"
                                                                key="positionDestination.label"/>
                                        <t:angular_table_header field="productTransferStatus"
                                                                key="productTransferStatus.label"/>

                                            <c:forEach items="${productTransfers}" var="productTransfer">
                                            <tr>
                                            <td>${productTransfer.createdAt}</td>
                                            <td>${productTransfer.product.subcode}</td>
                                            <td>${productTransfer.quantity}</td>
                                            <td>${productTransfer.positionSource}</td>
                                            <td>${productTransfer.positionDestination}</td>
                                            <td>${productTransfer.productTransferStatus}</td>

                                            <%--<td><c:url value="/productTransfer/" var="listUrl"/>--%>
                                            <%--<c:url value="/productTransfer/${productTransfer.id}/edit" var="editUrl"/>--%>
                                            <%--<c:choose>--%>
                                            <%--<c:when test="${readonly}">--%>
                                            <%--<div class="col-lg-6 col-md-6 text-right">--%>
                                            <%--<a href="${editUrl}" class="btn btn-primary">--%>
                                            <%--<i class="icon-edit"></i>--%>
                                            <%--<fmt:message key="general.label.edit"/>--%>
                                            <%--</a>--%>
                                            <%--</div>--%>
                                            <%--</c:when>--%>
                                            <%--<c:otherwise>--%>
                                            <%--<div class="col-lg-6 col-md-6">--%>
                                            <%--<a href="${listUrl}" class="btn btn-default">--%>
                                            <%--<i class="icon-chevron-left"></i>--%>
                                            <%--<fmt:message key="general.button.cancel"/>--%>
                                            <%--</a>--%>
                                            <%--</div>--%>
                                            <%--<div class="col-lg-6 col-md-6 text-right">--%>
                                            <%--<button type="submit" class="btn btn-success">--%>
                                            <%--<i class="icon-save"></i>--%>
                                            <%--<fmt:message key="general.button.save"/>--%>
                                            <%--</button>--%>
                                            <%--</div>--%>
                                            <%--</c:otherwise>--%>
                                            <%--</c:choose>--%>
                                            <%--</td>--%>
                                            <%--</tr>--%>
                                            </c:forEach>
                                    </table>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--<jsp:include page="transferList.tag"/>--%>

    <%--<script type="text/javascript">--%>
        <%--$(function () {--%>
            <%--$("#product").kendoComboBox({--%>
                <%--placeholder: '${pleaseSelect}',--%>
                <%--text: '${reception.product.subcode}',--%>
                <%--filter: 'contains',--%>
                <%--dataTextField: 'product',--%>
                <%--dataValueField: 'id',--%>
                <%--autoBind: false,--%>
                <%--dataSource: {--%>
                    <%--dataType: "json",--%>
                    <%--serverFiltering: true,--%>
                    <%--transport: {--%>
                        <%--read: {--%>
                            <%--url: "<c:url value="/product/api/find"/>",--%>
                            <%--data: function (e) {--%>
                                <%--return {--%>
                                    <%--term: (e['filter'] && e['filter']['filters']) ? e.filter.filters[0].value : null--%>
                                <%--}--%>
                            <%--},--%>
                            <%--type: 'POST'--%>
                        <%--}--%>
                    <%--},--%>
                    <%--schema: {--%>
                        <%--data: "itemList",--%>
                        <%--parse: function (response) {--%>
                            <%--$.each(response.itemList, function (idx, elem) {--%>
                                <%--elem.product = elem.label + (elem.extra != null ? (" - " + elem.extra) : "" );--%>
                            <%--});--%>
                            <%--return response;--%>
                        <%--}--%>
                    <%--}--%>
                <%--}--%>
            <%--});--%>

            <%--$("#customer").kendoComboBox({--%>
                <%--placeholder: '${pleaseSelect}',--%>
                <%--text: '${reception.customer.fullName}',--%>
                <%--filter: 'contains',--%>
                <%--dataTextField: 'customer',--%>
                <%--dataValueField: 'id',--%>
                <%--autoBind: false,--%>
                <%--dataSource: {--%>
                    <%--dataType: "json",--%>
                    <%--serverFiltering: true,--%>
                    <%--transport: {--%>
                        <%--read: {--%>
                            <%--url: "<c:url value="/customer/api/find"/>",--%>
                            <%--data: function (e) {--%>
                                <%--return {--%>
                                    <%--term: (e['filter'] && e['filter']['filters']) ? e.filter.filters[0].value : null--%>
                                <%--}--%>
                            <%--},--%>
                            <%--type: 'POST'--%>
                        <%--}--%>
                    <%--},--%>
                    <%--schema: {--%>
                        <%--data: "itemList",--%>
                        <%--parse: function (response) {--%>
                            <%--$.each(response.itemList, function (idx, elem) {--%>
                                <%--elem.customer = elem.label + (elem.extra != null ? (" - " + elem.extra) : "" );--%>
                            <%--});--%>
                            <%--return response;--%>
                        <%--}--%>
                    <%--}--%>
                <%--}--%>
            <%--});--%>

            <%--$("#position").kendoComboBox({--%>
                <%--placeholder: '${pleaseSelect}',--%>
                <%--text: '${reception.position.name}',--%>
                <%--filter: 'contains',--%>
                <%--dataTextField: 'position',--%>
                <%--dataValueField: 'id',--%>
                <%--autoBind: false,--%>
                <%--dataSource: {--%>
                    <%--dataType: "json",--%>
                    <%--serverFiltering: true,--%>
                    <%--transport: {--%>
                        <%--read: {--%>
                            <%--url: "<c:url value="/position/api/find"/>",--%>
                            <%--data: function (e) {--%>
                                <%--return {--%>
                                    <%--term: (e['filter'] && e['filter']['filters']) ? e.filter.filters[0].value : null--%>
                                <%--}--%>
                            <%--},--%>
                            <%--type: 'POST'--%>
                        <%--}--%>
                    <%--},--%>
                    <%--schema: {--%>
                        <%--data: "itemList",--%>
                        <%--parse: function (response) {--%>
                            <%--$.each(response.itemList, function (idx, elem) {--%>
                                <%--elem.position = elem.label + (elem.extra != null ? (" - " + elem.extra) : "" );--%>
                            <%--});--%>
                            <%--return response;--%>
                        <%--}--%>
                    <%--}--%>
                <%--}--%>
            <%--});--%>
        <%--});--%>
    <%--</script>--%>


</t:pageWrapper>