<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 20.02.2016
  Time: 03:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<set attributeName="readonly" value="${true}"/>
<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div>
            <h2>Przyjęcie towaru</h2>
            <div class="table-responsive" ng-controller="ReceptionController as ctrl">
                <div class="generic-container">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form id="editForm" ng-submit="ctrl.submit()" name="myForm" class="form-vertical" >
                                <input type="hidden" ng-model="ctrl.reception.id"/>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="control-lable" for="customer">Dostawca</label>
                                        <input type="text" ng-model="ctrl.reception.customer" id="customer"
                                               class="form-control"
                                               placeholder="Nazwa dostawcy [zawiera się również w tabeli klienci]"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="control-lable" for="position">Pozycja</label>
                                        <input type="text" ng-model="ctrl.reception.position" id="position"
                                               class="form-control"
                                               placeholder="Pozycja w magazynie"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="control-lable" for="product">Produkt</label>
                                        <input type="text" ng-model="ctrl.reception.product" id="product"
                                               class="form-control"
                                               placeholder="Dostarczany towar"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="control-lable" for="productType">Typ produktu</label>
                                        <input type="text" ng-model="ctrl.reception.productType" id="productType"
                                               class="form-control"
                                               placeholder="Typ produktu"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="control-lable" for="quantity">Ilość</label>
                                        <input type="text" ng-model="ctrl.reception.quantity" id="quantity"
                                               class="form-control"
                                               placeholder="Ilość"/>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group text-right">
                                        <button type="button" ng-click="ctrl.reset()" class="btn btn-danger"
                                                ng-disabled="myForm.$pristine">{{!ctrl.reception.id ? 'Anuluj' :
                                            'Cofnij'}}
                                        </button>
                                        <button type="submit" class="btn btn-primary"
                                                ng-disabled="myForm.$invalid">{{!ctrl.reception.id ? 'Dodaj' :
                                            'Zapisz'}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <form:form method="POST" ng-controller="ReceptionController as ctrl" action="${saveUrl}"
                       modelAttribute="reception">
                <form:hidden path="id"/>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="customer">Dostawca</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="customer"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="position">Pozycja</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="position"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="product">Produkt</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="product"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="productType">Typ produktu</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="productType"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="quantity">Ilość</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="quantity"/>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group ">
                        <button type="submit" class="btn btn-primary">Zapisz</button>
                    </div>
                </div>
            </form:form>

            <script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
            <script type="text/javascript">
                var App = angular.module('cargoApp', []).directive('chosen', function () {
                    var linker = function (scope, element, attr) {
                        // update the select when data is loaded
                        scope.$watch(attr.chosen, function (oldVal, newVal) {
                            element.trigger('chosen:updated');
                        });

                        // update the select when the model changes
                        scope.$watch(attr.ngModel, function () {
                            element.trigger('chosen:updated');
                        });

                        element.chosen();
                    };

                    return {
                        restrict: 'A',
                        link: linker
                    }
                });
            </script>

            <script src="<c:url value='/resources/angular/reception/reception_controller.js' />"></script>
            <script src="<c:url value='/resources/angular/reception/reception_service.js' />"></script>

        </div>
    </jsp:body>
</t:pageWrapper>


