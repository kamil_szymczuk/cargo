<%--
  Created by IntelliJ IDEA.
  User: risen16
  Date: 07.03.2016
  Time: 12:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div class="col-lg-12">
                <h2>Przyjęcie towaru</h2>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-body">
                <a href="<c:url value="/reception/add"/> " class="btn btn-primary">
                    <i class="icon-edit"></i>
                    <fmt:message key="general.label.add"/>
                </a>
            </div>
        </div>
        <div class="panel panel-default" ng-controller="ReceptionController as ctrl">
            <t:tableHeading/>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12 table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th ng-click="sort_by('id')">Akcje
                                            <p class="glyphicon sort-icon"
                                                  ng-show="sortKey=='id'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></p>
                                </th>
                                <t:angular_table_header field="customer" key="customer.label"/>
                                <t:angular_table_header field="position" key="position.label"/>
                                <t:angular_table_header field="product" key="product.label"/>
                                <t:angular_table_header field="productType" key="productType.label"/>
                                <t:angular_table_header field="quantity" key="general.label.quantity"/>

                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-show="ctrl.receptions.length <= 0">
                                <td colspan="6" style="text-align:center;">Ładowanie
                                    <i class="fa fa-cog fa-spin"></i></td>
                            </tr>
                            <tr ng-show="ctrl.receptions.length > 0" class="ng-hide"
                                dir-paginate="u in ctrl.receptions | filter : search | itemsPerPage:ctrl.receptionPageData.size"
                                total-items="ctrl.receptionPageData.totalElements">
                                <td>
                                    <a href="<c:url value='/reception/'/>{{u.id}}" class="btn btn-primary btn-xs">
                                        <i class="icon-edit"></i>
                                        <fmt:message key="general.label.details"/>
                                    </a>
                                </td>
                                <td><span ng-bind="u.customer"></span></td>
                                <td><span ng-bind="u.position"></span></td>
                                <td><span ng-bind="u.product"></span></td>
                                <td><span ng-bind="u.productType"></span></td>
                                <td><span ng-bind="u.quantity"></span></td>
                            </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6 text-right ng-hide" ng-show="ctrl.receptions.length > 0" colspan="6">
                        <dir-pagination-controls
                                max-size="8"
                                direction-links="true"
                                auto-hide="false"
                                boundary-links="false"
                                on-page-change="setPage(newPageNumber)">
                        </dir-pagination-controls>
                    </div>
                </div>
            </div>
        </div>

        <!-- ASSETS -->
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".clickable-row").click(function () {
                    window.document.location = $(this).data("href");
                });

            });
        </script>
        <script src="<c:url value='/resources/angular/reception/reception_controller.js' />"></script>
        <script src="<c:url value='/resources/angular/reception/reception_service.js' />"></script>

    </jsp:body>
</t:pageWrapper>