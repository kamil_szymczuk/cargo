<%--
  Created by IntelliJ IDEA.
  User: risen16
  Date: 01.03.2016
  Time: 21:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div class="page-header">
            <h2>Wydanie towaru</h2>
            <p></p>
        </div>
        <div class="row">
            <div class="panel panel-body">
                <a href="<c:url value="/issue/add"/> " class="btn btn-primary">
                    <i class="icon-edit"></i>
                    <fmt:message key="general.label.add"/>
                </a>
            </div>
        </div>
        <div>
            <div class="row">
                <br>
                <div class="table-responsive">
                    <div id="issueForm" class="generic-container" ng-controller="IssueController as ctrl">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form id="editForm" ng-submit="ctrl.submit()" name="myForm" class="form-vertical">
                                    <input type="hidden" ng-model="ctrl.issue.id"/>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class=" control-lable" for="customer">Klient</label>
                                            <input type="text" ng-model="ctrl.issue.customer" id="customer"
                                                   class="form-control" placeholder="Klient" required/>

                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class=" control-lable" for="issueNumber">Nr. Wydania</label>
                                            <input type="text" ng-model="ctrl.issue.issueNumber" id="issueNumber"
                                                   class="form-control" placeholder="Nr. Wydania" required/>

                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class=" control-lable" for="issueStatus">Status wydania</label>
                                            <input type="text" ng-model="ctrl.issue.issueStatus" id="issueStatus"
                                                   class="form-control" placeholder="Status wydania" required/>

                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class=" control-lable" for="departureDate">Data Wydania</label>
                                            <input type="text" ng-model="ctrl.issue.departureDate" id="departureDate"
                                                   class="form-control" placeholder="Data Wydania" required/>

                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group text-right">
                                            <button type="button" ng-click="ctrl.reset()" class="btn btn-danger"
                                                    ng-disabled="myForm.$pristine">{{!ctrl.issue.id ? 'Anuluj' : 'Cofnij'}}
                                            </button>
                                            <button type="submit" class="btn btn-primary"
                                                    ng-disabled="myForm.$invalid">{{!ctrl.issue.id ? 'Dodaj' : 'Zapisz'}}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div>
                            <div class="col-lg-12 form-group form-inline">
                                <div class="col-lg-6">
                                    <select ng-model="ctrl.pagingOptions.pageSize" ng-change="ctrl.fetchAllIssues()">
                                        <option
                                                ng-selected="ctrl.pagingOptions.pageSize == size"
                                                ng-repeat="size in ctrl.pagingOptions.pageSizes"
                                                ng-value="size">{{size}}</option>
                                    </select>
                                </div>
                                <div class="col-lg-6 text-right">
                                    <label class="control-label">Szukaj:</label>
                                    <input id="search" type="text" class="form-control" placeholder="Czego szukasz?"
                                           ng-bind="ctrl.pagingOptions.search"
                                           ng-change="ctrl.fetchAllIssues()"
                                           ng-model="ctrl.pagingOptions.search">
                                </div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-striped table-bordered table-hover">
                                    <tr>
                                        <th ng-click="sort_by('id')">Id
                                            <span class="glyphicon sort-icon"
                                                  ng-show="sortKey=='id'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                        </th>
                                        <th ng-click="sort_by('customer')">Klient
                                            <span class="glyphicon sort-icon"
                                                  ng-show="sortKey=='customer'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                        </th>
                                        <th ng-click="sort_by('issueNumber')">Nr. wydania
                                            <span class="glyphicon sort-icon"
                                                  ng-show="sortKey=='issueNumber'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                        </th>
                                        <th ng-click="sort_by('issueStatus')">Status wydania
                                            <span class="glyphicon sort-icon"
                                                  ng-show="sortKey=='issueStatus'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                        </th>
                                        <th ng-click="sort_by('departureDate')">Data wydania
                                            <span class="glyphicon sort-icon"
                                                  ng-show="sortKey=='departureDate'"
                                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tr ng-show="ctrl.issues.length <= 0">
                                        <td colspan="5" style="text-align:center;">Ładowanie</td>
                                    </tr>
                                    <tr dir-paginate="u in ctrl.issues | filter : search |itemsPerPage:ctrl.issuePageData.size"
                                        total-items="ctrl.issuePageData.totalElements"
                                        ng-click="ctrl.edit(u.id)">
                                            <td class="text-right"><a href="<c:url value="/issue/"/>{{u.id}}"><span ng-bind="u.id"></span></a></td>
                                            <td><span ng-bind="u.customer"></span></td>
                                            <td><span ng-bind="u.issueNumber"></span></td>
                                            <td><span ng-bind="u.issueStatus"></span></td>
                                            <td><span ng-bind="u.departureDate"></span></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                    <td colspan="6">
                                        <div class="pagination pull-right">
                                            <dir-pagination-controls
                                                    max-size="8"
                                                    direction-links="true"
                                                    auto-hide="false"
                                                    boundary-links="false"
                                                    on-page-change="setPage(newPageNumber)">
                                            </dir-pagination-controls>
                                        </div>
                                    </td>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ASSETS -->
       <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".clickable-row").click(function () {
                    window.document.location = $(this).data("href");
                });

            });
        </script>
        <script src="<c:url value='/resources/angular/issue/issue_controller.js' />"></script>
        <script src="<c:url value='/resources/angular/issue/issue_service.js' />"></script>

    </jsp:body>
</t:pageWrapper>