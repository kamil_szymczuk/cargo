<%--
  Created by IntelliJ IDEA.
  User: risen16
  Date: 01.03.2016
  Time: 21:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div>
            <h2>Wydanie</h2>
            <div class="table-responsive" ng-controller="IssueController as ctrl">
                <div class="generic-container" >
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form id="editForm" ng-submit="ctrl.submit()" name="myForm" class="form-vertical">
                                <input type="hidden" ng-model="ctrl.selected.id"/>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class=" control-lable" for="customer">Klient</label>
                                            <input type="text" ng-model="ctrl.issue.customer" id="customer"
                                                   class="form-control" placeholder="Klient" required/>

                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class=" control-lable" for="issueNumber">Nr. Wydania</label>
                                            <input type="text" ng-model="ctrl.issue.issueNumber" id="issueNumber"
                                                   class="form-control" placeholder="Nr. Wydania" required/>

                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class=" control-lable" for="issueStatus">Status wydania</label>
                                            <input type="text" ng-model="ctrl.issue.issueStatus" id="issueStatus"
                                                   class="form-control" placeholder="Status wydania" required/>

                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class=" control-lable" for="departureDate">Data Wydania</label>
                                            <input type="text" ng-model="ctrl.issue.departureDate" id="departureDate"
                                                   class="form-control" placeholder="Data Wydania" required/>

                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group text-right">
                                            <button type="button" ng-click="ctrl.reset()" class="btn btn-danger"
                                                    ng-disabled="myForm.$pristine">{{!ctrl.selected.id ? 'Anuluj' :
                                                'Cofnij'}}
                                            </button>
                                            <button type="submit" class="btn btn-primary"
                                                    ng-disabled="myForm.$invalid">{{!ctrl.selected.id ? 'Dodaj' : 'Zapisz'}}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <form:form method="POST" ng-controller="IssueController as ctrl" action="${saveUrl}"
                       modelAttribute="issue">
                <form:hidden path="id"/>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="customer">Klient</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="customer"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="issueNumber">Nr. wydania</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="issueNumber"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="issueStatus">Status wydania</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="issueStatus"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="departureDate">Data wydania</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="departureDate"/>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group ">
                        <button type="submit" class="btn btn-primary">Zapisz</button>
                    </div>
                </div>
            </form:form>
            <script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
            <script type="text/javascript">
                var App = angular.module('cargoApp', []).directive('chosen', function () {
                    var linker = function (scope, element, attr) {
                        // update the select when data is loaded
                        scope.$watch(attr.chosen, function (oldVal, newVal) {
                            element.trigger('chosen:updated');
                        });

                        // update the select when the model changes
                        scope.$watch(attr.ngModel, function () {
                            element.trigger('chosen:updated');
                        });

                        element.chosen();
                    };

                    return {
                        restrict: 'A',
                        link: linker
                    }
                });
            </script>
        <script src="<c:url value='/resources/angular/issue/issue_controller.js' />"></script>
        <script src="<c:url value='/resources/angular/issue/issue_service.js' />"></script>
        </div>
    </jsp:body>
</t:pageWrapper>