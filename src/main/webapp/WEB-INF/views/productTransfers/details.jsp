<%--
  Created by IntelliJ IDEA.
  User: risen16
  Date: 06.03.2016
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <h2>Produkt</h2>
        <form:form method="POST" action="${saveUrl}" modelAttribute="productTransfer">
            <form:hidden path="id"/>
            <table>
                <tr>
                    <td><form:label path="positionDestination">stanowisko docelowe</form:label></td>
                    <td><form:input readonly="${readonly}" path="positionDestination"/></td>
                </tr>
                <tr>
                    <td><form:label path="positionSource">źródło pozycji</form:label></td>
                    <td><form:input readonly="${readonly}" path="positionSource"/></td>
                </tr>
                <tr>
                    <td><form:label path="productTransferStatus">status transferu</form:label></td>
                    <td><form:input readonly="${readonly}" path="productTransferStatus"/></td>
                </tr>
                <tr>
                    <td><form:label path="quantity">ilość</form:label></td>
                    <td><form:input readonly="${readonly}" path="quantity"/></td>
                </tr>

                <tr>
                    <td><input type="submit" value="Zapisz"/></td>
                </tr>
            </table>
        </form:form>
    </jsp:body>
</t:pageWrapper>