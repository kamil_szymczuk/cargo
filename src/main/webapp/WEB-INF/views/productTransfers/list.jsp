<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div class="col-lg-12">
                <h2>Transfer</h2>
            </div>
        </div>
        <div class="row">
            <div class="panel-body">
                <a href="<c:url value="/productTransfer/add"/> " class="btn btn-primary">
                    <i class="icon-edit"></i>
                    <fmt:message key="general.label.add"/>
                </a>
            </div>
        </div>
        <div class="panel panel-default" ng-controller="ProductTransferController as ctrl">
            <t:tableHeading/>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <th ng-click="sort_by('id')">
                                <span class="text-center">Akcje</span>
                                            <span class="pull-right icon sort-icon"
                                                  ng-show="ctrl.sortingOrder=='id'"
                                                  ng-class="{'icon-chevron-up':ctrl.reverse,'icon-chevron-down':!ctrl.reverse}"></span>
                            </th>
                            <t:angular_table_header field="createdAt" key="general.label.createdAt"/>
                            <t:angular_table_header field="product" key="product.label"/>
                            <t:angular_table_header field="quantity" key="general.label.quantity"/>
                            <t:angular_table_header field="positionSource" key="positionSource.label"/>
                            <t:angular_table_header field="positionDestination" key="positionDestination.label"/>
                            <t:angular_table_header field="productTransferStatus"
                                                    key="productTransferStatus.label"/>

                        </tr>
                        </thead>
                        <tfoot>
                        </tfoot>
                        <tbody>
                        <tr ng-show="ctrl.productTransfers.length <= 0">
                            <td colspan="7" style="text-align:center;">Ładowanie <i
                                    class="fa fa-cog fa-spin"></i></td>
                        </tr>
                        <tr ng-show="ctrl.productTransfers.length > 0" class="ng-hide"
                            dir-paginate="u in ctrl.productTransfers | filter : search | itemsPerPage:ctrl.productTransferPageData.size | orderBy: ctrl.sortingOrder : ctrl.reverse"
                            total-items="ctrl.productTransferPageData.totalElements">
                            <td class="col-sm-1">
                                <a href="<c:url value="/productTransfer/"/>{{u.id}}" class="btn btn-primary btn-xs">
                                    <i class="icon-edit"></i>
                                    <fmt:message key="general.label.details"/>
                                </a>
                            </td>
                            <td><span ng-bind="u.createdAt"></span></td>
                            <td><span ng-bind="u.product"></span></td>
                            <td><span ng-bind="u.quantity"></span></td>
                            <td><span ng-bind="u.positionSource"></span></td>
                            <td><span ng-bind="u.positionDestination"></span></td>
                            <td><span ng-bind="u.productTransferStatus"></span></td>

                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6 text-right ng-hide" ng-show="ctrl.productTransfers.length > 0" colspan="6">
                        <dir-pagination-controls
                                max-size="8"
                                direction-links="true"
                                auto-hide="false"
                                boundary-links="false"
                                on-page-change="setPage(newPageNumber)">
                        </dir-pagination-controls>
                    </div>
                </div>
            </div>
        </div>

        <!-- ASSETS -->

        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".clickable-row").click(function () {
                    window.document.location = $(this).data("href");
                });

            });
        </script>
        <script src="<c:url value='/resources/angular/product_transfer/product_transfer_controller.js' />"></script>
        <script src="<c:url value='/resources/angular/product_transfer/product_transfer_service.js' />"></script>

    </jsp:body>
</t:pageWrapper>