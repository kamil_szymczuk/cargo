<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 17.03.2016
  Time: 20:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:pageWrapper>
    <h2>Transfer</h2>
    <div class="table-responsive" ng-controller="ProductTransferDetailController as ctrl">
        <div class="generic-container">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="tab-content">
                        <div class="tab-pane active" id="details">
                            <form:form commandName="productTransfer">
                                <form:hidden path="id"/>
                                <form:hidden path="version"/>
                                <%--<form:hidden path="creationUser"/>--%>
                                <%--<form:hidden path="creationDate"/>--%>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssclass="control-lable" path="product">Produkt</form:label>
                                            <form:input cssClass="form-control" readonly="${readonly}" path="product"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssclass="control-lable" path="quantity">Ilość</form:label>
                                            <form:input cssClass="form-control" readonly="${readonly}" path="quantity"/>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssclass="control-lable" path="positionSource">Źródło</form:label>
                                            <form:input cssClass="form-control" readonly="${readonly}"
                                                        path="positionSource"/>
                                        </div>

                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssclass="control-lable" path="positionDestination">Cel</form:label>
                                            <form:input cssClass="form-control" readonly="${readonly}" path="positionDestination"/>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssclass="control-lable" path="productTransferStatus">Status</form:label>
                                            <form:input cssClass="form-control" readonly="${readonly}"
                                                        path="productTransferStatus"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <c:url value="/productTransfer/" var="listUrl"/>
                                    <c:url value="/productTransfer/${productTransfer.id}/edit" var="editUrl"/>
                                    <c:choose>
                                        <c:when test="${readonly}">
                                            <div class="col-lg-6 col-md-6">
                                                <a id="back" href="${listUrl}" class="btn btn-default">
                                                    <i class="icon-chevron-left"></i>
                                                    Wstecz
                                                        <%--<fmt:message key="general.label.back"/>--%>
                                                </a>
                                            </div>
                                            <div class="col-lg-6 col-md-6 text-right">
                                                <a href="${editUrl}" class="btn btn-primary">
                                                    <i class="icon-edit"></i>
                                                    Edycja
                                                        <%--<fmt:message key="general.label.edit"/>--%>
                                                </a>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="col-lg-6 col-md-6">
                                                <a href="${listUrl}" class="btn btn-default">
                                                    <i class="icon-chevron-left"></i>
                                                    Anuluj
                                                        <%--<fmt:message key="general.button.cancel"/>--%>
                                                </a>
                                            </div>
                                            <div class="col-lg-6 col-md-6 text-right">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="icon-save"></i>
                                                    Zapisz
                                                        <%--<fmt:message key="general.button.save"/>--%>
                                                </button>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function () {

            $("#product").kendoComboBox({
                placeholder: '${pleaseSelect}',
                text: '${productTransfer.product.subcode}',
                filter: 'contains',
                dataTextField: 'product',
                dataValueField: 'id',
                autoBind: false,
                dataSource: {
                    dataType: "json",
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "<c:url value="/product/api/find"/>",
                            data: function (e) {
                                return {
                                    term: (e['filter'] && e['filter']['filters']) ? e.filter.filters[0].value : null
                                }
                            },
                            type: 'POST'
                        }
                    },
                    schema: {
                        data: "itemList",
                        parse: function (response) {
                            $.each(response.itemList, function (idx, elem) {
                                elem.product = elem.label + (elem.extra != null ? (" - " + elem.extra) : "" );
                            });
                            return response;
                        }
                    }
                }
            });

            $("#positionSource").kendoComboBox({
                placeholder: '${pleaseSelect}',
                text: '${productTransfer.positionSource.name}',
                filter: 'contains',
                dataTextField: 'positionSource',
                dataValueField: 'id',
                autoBind: false,
                dataSource: {
                    dataType: "json",
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "<c:url value="/position/api/find"/>",
                            data: function (e) {
                                return {
                                    term: (e['filter'] && e['filter']['filters']) ? e.filter.filters[0].value : null
                                }
                            },
                            type: 'POST'
                        }
                    },
                    schema: {
                        data: "itemList",
                        parse: function (response) {
                            $.each(response.itemList, function (idx, elem) {
                                elem.positionSource = elem.label + (elem.extra != null ? (" - " + elem.extra) : "" );
                            });
                            return response;
                        }
                    }
                }
            });

            $("#positionDestination").kendoComboBox({
                placeholder: '${pleaseSelect}',
                text: '${productTransfer.positionDestination.name}',
                filter: 'contains',
                dataTextField: 'positionDestination',
                dataValueField: 'id',
                autoBind: false,
                dataSource: {
                    dataType: "json",
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "<c:url value="/position/api/find"/>",
                            data: function (e) {
                                return {
                                    term: (e['filter'] && e['filter']['filters']) ? e.filter.filters[0].value : null
                                }
                            },
                            type: 'POST'
                        }
                    },
                    schema: {
                        data: "itemList",
                        parse: function (response) {
                            $.each(response.itemList, function (idx, elem) {
                                elem.positionDestination = elem.label + (elem.extra != null ? (" - " + elem.extra) : "" );
                            });
                            return response;
                        }
                    }
                }
            });

//            $("#productTransferStatus").DropDownListFor(m => m.ePriority, new SelectList(Enum.GetNames(typeof(PriorityEnum))))
        });

    </script>


</t:pageWrapper>