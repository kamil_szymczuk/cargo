<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <title>${appTitle}</title>

    <c:url value="/resources/plugins/kendoui.core/styles/kendo.common-bootstrap.min.css"
           var="kendo_bootstrap_css"/>
    <c:url value="/resources/plugins/kendoui.core/styles/kendo.bootstrap.min.css" var="kendo_bootstrap_min_css"/>
    <c:url value="/resources/plugins/kendoui.core/js/kendo.ui.core.min.js" var="kendo_ui_core_min_js"/>


    <link rel="stylesheet" href="${kendo_bootstrap_css}"/>
    <link rel="stylesheet" href="${kendo_bootstrap_min_css}"/>
    <link rel='stylesheet' href='<c:url value="/resources/plugins/bootstrap/dist/css/bootstrap.min.css" />'>
    <link href="<c:url value="/resources/css/styles.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/plugins/font-awesome/css/font-awesome.min.css" />" rel="stylesheet"
          type="text/css">

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->
    <script type="text/javascript" src="<c:url value="/resources/jquery/2.2/jquery-2.2.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/resources/plugins/angular-1.5.2/angular.min.js"/>"></script>
    <script type="text/javascript"
            src="<c:url value="/resources/plugins/angular-1.5.2/angular-cookies.min.js"/>"></script>
    <script type="text/javascript"
            src="<c:url value="/resources/plugins/angular-1.5.2/angular-route.min.js"/>"></script>
    <script type="text/javascript"
            src="<c:url value="/resources/plugins/spring-security-csrf-token-interceptor.js"/>"></script>

    <script type="text/javascript" src="<c:url value='/resources/angular/assets/js/dirPagination.js' />"></script>
    <script type="text/javascript" src="<c:url value='/resources/angular/assets/js/select/select.js' />"></script>
    <script type="text/javascript"
            src="<c:url value='/resources/angular/assets/js/angular-chosen-1.3.0/dist/angular-chosen.js' />"></script>
    <script type="text/javascript" src="<c:url value='/resources/angular/cargoApp.js' />"></script>

    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0"/>
</head>
<body>
<div id="pageheader">
</div>
<section id="body" class="width">
    <section id="logincontent" class="column-right">
        <article id="main">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="login-panel panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><fmt:message key="general.login.title"/></h3>
                            </div>
                            <div class="panel-body">
                                <c:url value="/login" var="securityUrl"/>
                                <form:form id="loginform" method="post" action="${securityUrl}"
                                           class="form-vertical no-padding no-margin">
                                    <fieldset>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="E-mail" name="username"
                                                   autofocus>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Password" name="password"
                                                   type="password" value="">
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input name="remember" type="checkbox" value="Remember Me"><fmt:message
                                                    key="general.login.rememberMe"/>
                                            </label>
                                        </div>
                                        <!-- Change this to a button or input when using this as a form -->
                                        <input type="submit" class="btn btn-lg btn-success btn-block"/>
                                    </fieldset>
                                </form:form>
                                <c:if test="${param.error != null}">
                                    <div id="error" class="alert alert-danger">
                                        <fmt:message key="general.login.error"/>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
</section>
<footer class="footer">
    <div id="container" class="text-center">
        <span>&copy; 2016 IhhCargo by IHH</span>
    </div>
</footer>

<script src="${kendo_ui_core_min_js}"></script>
</body>
</html>

