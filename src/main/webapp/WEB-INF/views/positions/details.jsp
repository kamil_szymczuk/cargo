<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 29.02.2016
  Time: 22:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div>
            <h2>Magazyn</h2>
            <div class="table-responsive" ng-controller="PositionController as ctrl">
                <div class="generic-container">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form id="editForm" ng-submit="ctrl.submit()" name="myForm" class="form-vertical">
                                <input type="hidden" ng-model="ctrl.selected.id"/>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-lable" for="name">Nazwa</label>
                                            <input type="text" ng-model="ctrl.position.name" id="name"
                                                   class="form-control" placeholder="Nazwa" required
                                                   ng-minlength="3"/>
                                            <div class="has-error" ng-show="myForm.$dirty">
                                                <span ng-show="myForm.name.$error.required">This is a required field</span>
                                                <span ng-show="myForm.name.$error.minlength">Minimum length required is 3</span>
                                                <span ng-show="myForm.name.$invalid">This field is invalid </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label class=" control-lable" for="quantity">Ilość</label>
                                            <input type="text" ng-model="ctrl.position.quantity" id="quantity"
                                                   class="form-control"
                                                   placeholder="Ilość"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label class=" control-lable" for="lane">Alejka</label>
                                            <input type="text" ng-model="ctrl.position.lane" id="lane"
                                                   class="form-control"
                                                   placeholder="Alejka"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label class=" control-lable" for="product">Produkt</label>
                                            <input type="text" ng-model="ctrl.position.product" id="product"
                                                   class="form-control"
                                                   placeholder="Produkt"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group text-right">
                                            <button type="button" ng-click="ctrl.reset()" class="btn btn-danger"
                                                    ng-disabled="myForm.$pristine">{{!ctrl.selected.id ? 'Anuluj' :
                                                'Cofnij'}}
                                            </button>
                                            <button type="submit" class="btn btn-primary"
                                                    ng-disabled="myForm.$invalid">{{!ctrl.selected.id ? 'Dodaj' : 'Zapisz'}}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <form:form method="POST" ng-controller="PositionController as ctrl" action="${saveUrl}"
                   modelAttribute="position">
            <form:hidden path="id"/>
            <div class="col-lg-3">
                <div class="form-group ">
                    <form:label class="control-lable" path="name">Nazwa</form:label>
                    <form:input class="form-control" readonly="${readonly}" path="name"/>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group ">
                    <form:label class="control-lable" path="quantity">Ilość</form:label>
                    <form:input class="form-control" readonly="${readonly}" path="quantity"/>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group ">
                    <form:label class="control-lable" path="lane">Alejka</form:label>
                    <form:input class="form-control" readonly="${readonly}" path="lane"/>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group ">
                    <form:label class="control-lable" path="product">Produ</form:label>
                    <form:input class="form-control" readonly="${readonly}" path="product"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group ">
                    <button type="submit" class="btn btn-primary">Zapisz</button>
                </div>
            </div>
        </form:form>

        <script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
        <script type="text/javascript">
            var App = angular.module('cargoApp', [])
                    .directive('chosen', function () {
                        var linker = function (scope, element, attr) {
                            // update the select when data is loaded
                            scope.$watch(attr.chosen, function (oldVal, newVal) {
                                element.trigger('chosen:updated');
                            });

                            // update the select when the model changes
                            scope.$watch(attr.ngModel, function () {
                                element.trigger('chosen:updated');
                            });

                            element.chosen();
                        };

                        return {
                            restrict: 'A',
                            link: linker
                        }
                    });

            jQuery(document).ready(function ($) {
                $(".clickable-row").click(function () {
                    window.document.location = $(this).data("href");
                });
            });
        </script>
        <script src="<c:url value='/resources/angular/position/position_controller.js' />"></script>
        <script src="<c:url value='/resources/angular/position/position_service.js' />"></script>

    </jsp:body>
</t:pageWrapper>


