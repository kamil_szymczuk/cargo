<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 29.02.2016
  Time: 22:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<c:url var="url" value="%2fposition%2f"/>

<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div class="col-lg-12">
                <h2><fmt:message key="general.label.warehouse"/></h2>
            </div>
        </div>
        <div class="row">
            <div class="panel-body">
                <%-- <a href="<c:url value="/product/add"/> " class="btn btn-primary">
                    <i class="icon-edit"></i>
                    <fmt:message key="product.add"/>
                </a> --%>
                <a href="<c:url value="/position/add"/> " class="btn btn-primary">
                    <i class="icon-edit"></i>
                    <fmt:message key="position.add"/>
                </a>

            </div>
        </div>
        <div class="panel panel-default" ng-controller="PositionController as ctrl">
                <t:tableHeading/>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <t:angular_table_header field="id" key="general.label.actions"/>
                            <t:angular_table_header field="laneName" key="general.label.shortName"/>
                            <t:angular_table_header field="name" key="general.label.shortName"/>
                            <t:angular_table_header field="product" key="product.label"/>
                            <t:angular_table_header field="productType" key="productType.label"/>
                            <t:angular_table_header field="quantity" key="general.label.quantity"/>
                        </tr>
                        </thead>
                        <tfoot>
                        </tfoot>
                        <tbody>
                        <tr ng-show="ctrl.positions.length <= 0">
                            <td colspan="5" style="text-align:center;">Ładowanie<i class="fa fa-cog fa-spin"></i></td>
                        </tr>
                        <tr ng-show="ctrl.positions.length > 0" class="ng-hide"
                            dir-paginate="u in ctrl.positions | filter : search | itemsPerPage:ctrl.positionPageData.size | orderBy: ctrl.sortingOrder : ctrl.reverse"
                            total-items="ctrl.positionPageData.totalElements">
                            <td>
                                <a href="<c:url value="/position/"/>{{u.id}}?backUrl=${url}" class="btn btn-primary btn-xs">
                                    <i class="icon-edit"></i>
                                    <fmt:message key="general.label.details"/>
                                </a>
                            </td>
                            <td><span ng-bind="u.laneName"></span></td>
                            <td><span ng-bind="u.name"></span></td>
                            <td><span ng-bind="u.product"></span>
                                <a ng-hide="u.productId" href="<c:url value="/position/{{u.id}}/edit"/>"
                                   class="btn btn-primary btn-xs">
                                    <i class="icon-edit"></i>
                                    <fmt:message key="general.label.add"/>
                                </a>
                            </td>
                            <td><span ng-bind="u.productType"></span></td>
                            <td><span ng-bind="u.quantity"></span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6 text-right ng-hide" ng-show="ctrl.positions.length > 0" colspan="6">
                        <dir-pagination-controls
                                max-size="8"
                                direction-links="true"
                                auto-hide="false"
                                boundary-links="false"
                                on-page-change="setPage(newPageNumber)">
                        </dir-pagination-controls>
                    </div>
                </div>
            </div>
        </div>

        <!-- ASSETS -->

        <%--<script type="text/javascript">--%>
            <%--jQuery(document).ready(function ($) {--%>
                <%--$(".clickable-row").click(function () {--%>
                    <%--window.document.location = $(this).data("href");--%>
                <%--});--%>

            <%--});--%>
        <%--</script>--%>
        <script src="<c:url value='/resources/angular/position/position_controller.js' />"></script>
        <script src="<c:url value='/resources/angular/position/position_service.js' />"></script>
        <script src="<c:url value='/resources/angular/position/positions.js' />"></script>
    </jsp:body>
</t:pageWrapper>