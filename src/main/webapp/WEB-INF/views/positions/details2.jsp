<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 17.03.2016
  Time: 20:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url var="backUrl" value="${not empty backUrl ? backUrl : param.backUrl}"/>
<c:url var="editUrl" value="/position/${position.id}/edit"/>

<c:url var="list" value="/position/"/>
<c:set var="backUrl" value="${empty backUrl ? list : backUrl}"/>

<t:pageWrapper>
    <div class="row">
        <h2>Magazyn</h2>
    </div>
    <div ng-controller="PositionDetailController as ctrl">
        <div class="generic-container">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="tab-content">
                        <div class="tab-pane active" id="details">
                            <form:form commandName="position">
                                <form:hidden path="id"/>
                                <form:hidden path="version"/>
                                <%--<form:hidden path="creationUser"/>--%>
                                <%--<form:hidden path="creationDate"/>--%>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssclass="control-lable" path="name">Nazwa</form:label>
                                            <form:input cssClass="form-control" readonly="${readonly}" path="name"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssclass="col-lg-6 control-lable"
                                                        path="lane">Alejka</form:label>
                                            <div>
                                                <form:input cssClass="col-lg-6 form-control" readonly="${readonly}"
                                                            path="${readonly ? 'lane.name' : 'lane'}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssclass="control-lable" path="product">Produkt</form:label>
                                            <div>
                                                <form:input cssClass="form-control" readonly="${readonly}"
                                                            path="${readonly ? 'product.subcode' : 'product'}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssclass="control-lable" path="quantity">Ilość</form:label>
                                            <form:input cssClass="form-control" readonly="${readonly}"
                                                        path="quantity"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <c:choose>
                                        <c:when test="${readonly}">
                                            <div class="col-lg-6">
                                                <a id="back" href="${backUrl}" class="btn btn-default">
                                                    <i class="icon-chevron-left"></i>
                                                    <fmt:message key="general.label.back"/>
                                                </a>
                                            </div>
                                            <div class="col-lg-6 text-right">
                                                <a href="${editUrl}" class="btn btn-primary">
                                                    <i class="icon-edit"></i>
                                                    <fmt:message key="general.label.edit"/>
                                                </a>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="col-lg-6">
                                                <a href="${backUrl}" class="btn btn-default">
                                                    <i class="icon-chevron-left"></i>
                                                    <fmt:message key="general.button.cancel"/>
                                                </a>
                                            </div>
                                            <div class="col-lg-6 text-right">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="icon-save"></i>
                                                    <fmt:message key="general.button.save"/>
                                                </button>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function () {

            $("#product").kendoComboBox({
                placeholder: '${pleaseSelect}',
                text: '${position.product.subcode}',
                filter: 'contains',
                dataTextField: 'product',
                dataValueField: 'id',
                autoBind: false,
                dataSource: {
                    dataType: "json",
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "<c:url value="/product/api/find"/>",
                            data: function (e) {
                                return {
                                    term: (e['filter'] && e['filter']['filters']) ? e.filter.filters[0].value : null
                                }
                            },
                            type: 'POST'
                        }
                    },
                    schema: {
                        data: "itemList",
                        parse: function (response) {
                            $.each(response.itemList, function (idx, elem) {
                                elem.product = elem.label + (elem.extra != null ? (" - " + elem.extra) : "" );
                            });
                            return response;
                        }
                    }
                }
            });

            $("#lane").kendoComboBox({
                placeholder: '${pleaseSelect}',
                text: '${position.lane.name}',
                filter: 'contains',
                dataTextField: 'lane',
                dataValueField: 'id',
                autoBind: false,
                dataSource: {
                    dataType: "json",
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "<c:url value="/lane/api/find"/>",
                            data: function (e) {
                                return {
                                    term: (e['filter'] && e['filter']['filters']) ? e.filter.filters[0].value : null
                                }
                            },
                            type: 'POST'
                        }
                    },
                    schema: {
                        data: "itemList",
                        parse: function (response) {
                            $.each(response.itemList, function (idx, elem) {
                                elem.lane = elem.label + (elem.extra != null ? (" - " + elem.extra) : "" );
                            });
                            return response;
                        }
                    }
                }
            });
        });

    </script>


</t:pageWrapper>