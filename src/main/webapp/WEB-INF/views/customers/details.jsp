<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 20.02.2016
  Time: 03:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<set attributeName="readonly" value="${true}"/>
<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div>
            <h2>Klienci</h2>
            <div class="table-responsive" ng-controller="CustomerController as ctrl">
                <div class="generic-container">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form id="editForm" ng-submit="ctrl.submit()" name="myForm" class="form-vertical" >
                                <input type="hidden" ng-model="ctrl.customer.id"/>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="control-lable" for="fullName">Nazwa</label>
                                            <input type="text" ng-model="ctrl.customer.fullName" id="fullName"
                                                   class="form-control" placeholder="Nazwa" required
                                                   ng-minlength="3"/>
                                            <div class="has-error" ng-show="myForm.$dirty">
                                                <span ng-show="myForm.fullName.$error.required">This is a required field</span>
                                                <span ng-show="myForm.fullName.$error.minlength">Minimum length required is 3</span>
                                                <span ng-show="myForm.fullName.$invalid">This field is invalid </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class=" control-lable" for="shortName">Nazwa skrócona</label>
                                            <input type="text" ng-model="ctrl.customer.shortName" id="shortName"
                                                   class="form-control"
                                                   placeholder="Nazwa skrócona [wyświetlans w systemie]"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class=" control-lable" for="nipNumber">NIP</label>
                                            <input type="text" ng-model="ctrl.customer.nipNumber" id="nipNumber"
                                                   class="form-control" placeholder="NIP"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group ">
                                            <label class=" control-lable" for="regonNumber">REGON</label>
                                            <input type="text" ng-model="ctrl.customer.regonNumber" id="regonNumber"
                                                   class="form-control" placeholder="REGON"/>

                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group ">
                                            <label class=" control-lable" for="phoneNumber">Nr. telefonu</label>
                                            <input type="text" ng-model="ctrl.customer.phoneNumber" id="phoneNumber"
                                                   class="form-control" placeholder="Nr. telefonu"/>

                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group ">
                                            <label class=" control-lable" for="email">e-mail</label>
                                            <input type="email" ng-model="ctrl.customer.email" id="email"
                                                   class="email form-control" placeholder="e-mail" required/>
                                            <div class="has-error" ng-show="myForm.$dirty">
                                                <span ng-show="myForm.email.$error.required">This is a required field</span>
                                                <span ng-show="myForm.email.$invalid">This field is invalid </span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group text-right">
                                            <button type="button" ng-click="ctrl.reset()" class="btn btn-danger"
                                                    ng-disabled="myForm.$pristine">{{!ctrl.selected.id ? 'Anuluj' :
                                                'Cofnij'}}
                                            </button>
                                            <button type="submit" class="btn btn-primary"
                                                    ng-disabled="myForm.$invalid">{{!ctrl.selected.id ? 'Dodaj' : 'Zapisz'}}
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>


            <form:form method="POST" ng-controller="CustomerController as ctrl" action="${saveUrl}"
                       modelAttribute="customer">
                <form:hidden path="id"/>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="fullName">Pełna nazwa</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="fullName"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="shortName">Nazwa skrócona</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="shortName"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="nipNumber">NIP</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="nipNumber"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="regonNumber">Regon</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="regonNumber"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="email">E-mail</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="email"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="phoneNumber">Nr. telefonu</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="phoneNumber"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <form:label class="control-lable" path="notes">Notatka</form:label>
                        <form:input class="form-control" readonly="${readonly}" path="notes"/>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group ">
                        <button type="submit" class="btn btn-primary">Zapisz</button>
                    </div>
                </div>
            </form:form>

            <script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
            <script type="text/javascript">
                var App = angular.module('cargoApp', []).directive('chosen', function () {
                    var linker = function (scope, element, attr) {
                        // update the select when data is loaded
                        scope.$watch(attr.chosen, function (oldVal, newVal) {
                            element.trigger('chosen:updated');
                        });

                        // update the select when the model changes
                        scope.$watch(attr.ngModel, function () {
                            element.trigger('chosen:updated');
                        });

                        element.chosen();
                    };

                    return {
                        restrict: 'A',
                        link: linker
                    }
                });
            </script>

        <script src="<c:url value='/resources/angular/customer/customer_controller.js' />"></script>
        <script src="<c:url value='/resources/angular/customer/customer_service.js' />"></script>

        </div>
    </jsp:body>
</t:pageWrapper>


