<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<div class="table-responsive" ng-controller="ProductDetailController as ctrl">
    <div class="generic-container">
        <div class="panel panel-default">
            <div class="panel-body">
                <form id="editForm" ng-submit="ctrl.submit()" name="myForm" class="form-vertical">
                    <input type="hidden" ng-model="ctrl.selected.id"/>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="control-lable" for="subcode">Kod</label>
                            <input type="text" ng-model="ctrl.product.subcode" id="subcode"
                                   class="form-control" placeholder="Kod" required
                                   ng-minlength="3"/>

                            <div class="has-error" ng-show="myForm.$dirty">
                                <span ng-show="myForm.uname.$error.required">This is a required field</span>
                                <span ng-show="myForm.uname.$error.minlength">Minimum length required is 3</span>
                                <span ng-show="myForm.uname.$invalid">This field is invalid </span>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class=" control-label" for="description">Opis</label>
                            <input type="text" ng-model="ctrl.product.description" id="description"
                                   class="form-control" placeholder="Opis"/>
                        </div>
                        <div class="form-group ">
                            <label class=" control-label" for="weight">Masa</label>
                            <input type="number" autocomplete="off" ng-model="ctrl.product.weight"
                                   id="weight"
                                   class="form-control" placeholder="Masa"/>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="control-label" for="productType">Kod główny:</label>
                            <input autocomplete="off"
                                   ng-model="ctrl.product.baseCode"
                                   ng-change="ctrl.fetchAllProductTypes()"
                                   id="productType"
                                   class="form-control" placeholder="Kod główny"/>
                            <button class="btn btn-primary"
                                    ng-show="ctrl.productTypes.length == 0">
                                Dodaj nowy kod główny
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group text-right">
                            <button type="button" ng-click="ctrl.reset()" class="btn btn-danger"
                                    ng-disabled="myForm.$pristine">{{!ctrl.product.id ? 'Anuluj' :
                                'Cofnij'}}
                            </button>
                            <button type="submit" class="btn btn-primary"
                                    ng-disabled="myForm.$invalid">{{!ctrl.product.id ? 'Dodaj' : 'Zapisz'}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<c:url value='/resources/angular/product/product_detail_controller.js' />"></script>
<script src="<c:url value='/resources/angular/product/product_service.js' />"></script>
<script src="<c:url value='/resources/angular/product/product_type_controller.js' />"></script>
<script src="<c:url value='/resources/angular/product/product_type_service.js' />"></script>
