<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 17.03.2016
  Time: 20:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:pageWrapper>
    <div class="row">
        <h2>Produkt</h2>
    </div>
    <div class="table-responsive" ng-controller="ProductDetailController as ctrl">
        <div class="generic-container">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="tab-content">
                        <div class="tab-pane active" id="details">
                            <form:form commandName="product">
                                <form:hidden path="id"/>
                                <form:hidden path="version"/>
                                <form:hidden path="createdAt"/>
                                <form:hidden path="createdBy"/>
                                <form:hidden path="updatedAt"/>
                                <form:hidden path="updatedBy"/>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssclass="control-lable" path="subcode">Kod</form:label>
                                            <form:input cssClass="form-control" readonly="${readonly}" path="subcode"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssClass="control-lable"
                                                        path="productType">Kod główny</form:label>
                                            <div>
                                                <form:input readonly="${readonly}" cssClass="form-control"
                                                            path="${readonly == true ? 'productType.baseCode' : 'productType'}"/>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssclass="control-lable" path="description">Opis</form:label>
                                            <form:input cssClass="form-control" readonly="${readonly}"
                                                        path="description"/>
                                        </div>

                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <form:label cssclass="control-lable" path="weight">Masa</form:label>
                                            <form:input cssClass="form-control" readonly="${readonly}" path="weight"/>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <c:url value="/product/" var="listUrl"/>
                                    <c:url value="/product/${product.id}/edit" var="editUrl"/>
                                    <c:choose>
                                        <c:when test="${readonly}">
                                            <div class="col-lg-6 col-md-6">
                                                <a id="back" href="${listUrl}" class="btn btn-default">
                                                    <i class="icon-chevron-left"></i>
                                                    Wstecz
                                                        <%--<fmt:message key="general.label.back"/>--%>
                                                </a>
                                            </div>
                                            <div class="col-lg-6 col-md-6 text-right">
                                                <a href="${editUrl}" class="btn btn-primary">
                                                    <i class="icon-edit"></i>
                                                    Edycja
                                                        <%--<fmt:message key="general.label.edit"/>--%>
                                                </a>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="col-lg-6 col-md-6">
                                                <a href="${listUrl}" class="btn btn-default">
                                                    <i class="icon-chevron-left"></i>
                                                    Anuluj
                                                        <%--<fmt:message key="general.button.cancel"/>--%>
                                                </a>
                                            </div>
                                            <div class="col-lg-6 col-md-6 text-right">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="icon-save"></i>
                                                    Zapisz
                                                        <%--<fmt:message key="general.button.save"/>--%>
                                                </button>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </form:form>
                        </div>
                    </div>

                    <c:if test="${not empty positions}">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">

                                    <thead>
                                    <tr>
                                        <t:angular_table_header field="id" key="general.label.actions"/>
                                        <t:angular_table_header field="name" key="position.label"/>
                                        <t:angular_table_header field="quantity" key="general.label.quantity"/>
                                    </tr>
                                    </thead>
                                    <c:forEach items="${positions}" var="position" varStatus="status">
                                        <tr>
                                            <td>
                                                <a href="<c:url value="/position/"/>${position.id}"
                                                   class="btn btn-primary btn-xs">
                                                    <i class="icon-edit"></i>
                                                    <fmt:message key="general.label.details"/>
                                                </a>
                                            </td>
                                            <td>${position.lane.shortName} ${position.name}</td>
                                            <td>${position.quantity}</td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function () {

            $("#productType").kendoComboBox({
                placeholder: '${pleaseSelect}',
                text: '${product.productType.baseCode}',
                filter: 'contains',
                dataTextField: 'baseCode',
                dataValueField: 'id',
                autoBind: false,
                dataSource: {
                    dataType: "json",
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "<c:url value="/productType/api/find"/>",
                            data: function (e) {
                                return {
                                    term: (e['filter'] && e['filter']['filters']) ? e.filter.filters[0].value : null
                                }
                            },
                            type: 'POST'
                        }
                    },
                    schema: {
                        data: "itemList",
                        parse: function (response) {
                            $.each(response.itemList, function (idx, elem) {
                                elem.baseCode = elem.label + (elem.extra != null ? (" - " + elem.extra) : "" );
                            });
                            return response;
                        }
                    }
                }
            });
        });

    </script>


</t:pageWrapper>