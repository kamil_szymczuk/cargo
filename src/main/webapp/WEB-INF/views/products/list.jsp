<%--
  Created by IntelliJ IDEA.
  User: kuta
  Date: 20.02.2016
  Time: 03:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<t:pageWrapper>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div class="col-lg-12">
                <h2>Produkty</h2>
            </div>
        </div>
        <div class="row">
            <div class="panel-body">
                <a href="<c:url value="/product/add"/> " class="btn btn-primary">
                    <i class="icon-edit"></i>
                    <fmt:message key="general.label.add"/>
                </a>
            </div>
        </div>
        <div class="panel panel-default" ng-controller="ProductController as ctrl">
            <t:tableHeading/>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <t:angular_table_header field="id" key="general.label.actions"/>
                            <t:angular_table_header field="productType.baseCode" key="productType.label.baseCode"/>
                            <t:angular_table_header field="subcode" key="product.label.subcode"/>
                            <t:angular_table_header field="description" key="general.label.description"/>
                            <t:angular_table_header field="weight" key="general.label.weight"/>

                        </tr>
                        </thead>
                        <tfoot>
                        </tfoot>
                        <tbody>
                        <tr ng-show="ctrl.products.length <= 0">
                            <td colspan="5" style="text-align:center;">Ładowanie<i class="fa fa-cog fa-spin"></i></td>
                        </tr>
                        <tr ng-show="ctrl.products.length > 0" class="ng-hide"
                            dir-paginate="u in ctrl.products | filter : search | itemsPerPage:ctrl.productPageData.size | orderBy: ctrl.sortingOrder : ctrl.reverse"
                            total-items="ctrl.productPageData.totalElements">
                            <td>
                                <a href="<c:url value="/product/"/>{{u.id}}" class="btn btn-primary btn-xs">
                                    <i class="icon-edit"></i>
                                    <fmt:message key="general.label.details"/>
                                </a>
                            </td>
                            <td><span ng-bind="u.baseCode"></span></td>
                            <td><span ng-bind="u.subcode"></span></td>
                            <td><span ng-bind="u.description"></span></td>
                            <td><span ng-bind="u.weight"></span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-right ng-hide" ng-show="ctrl.products.length > 0" colspan="6">
                        <dir-pagination-controls
                                max-size="8"
                                direction-links="true"
                                auto-hide="false"
                                boundary-links="false"
                                on-page-change="setPage(newPageNumber)">
                        </dir-pagination-controls>
                    </div>
                </div>
            </div>
        </div>


        <!-- ASSETS -->

        <script src="<c:url value='/resources/angular/product/product_controller.js' />"></script>
        <script src="<c:url value='/resources/angular/product/product_service.js' />"></script>
        <script src="<c:url value='/resources/angular/product/product_type_service.js' />"></script>
    </jsp:body>
</t:pageWrapper>