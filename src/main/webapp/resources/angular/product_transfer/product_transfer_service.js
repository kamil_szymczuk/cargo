'use strict';

App.factory('ProductTransferService', ['$http', '$q', 'config', function($http, $q, config){

    self.pageRequest={};

    return {

        fetchPageOfProductTransfers: function(pageRequest) {

            self.pageRequest.size = pageRequest.size;
            self.pageRequest.page = pageRequest.number;
            self.pageRequest.search = pageRequest.search;

            return $http.post(config.basePath + '/productTransfer/api/list',self.pageRequest)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching product transfers');
                        return $q.reject(errResponse);
                    }
                );
        },
        fetchAllProductTransfers: function() {
            return $http.get(config.basePath + '/productTransfer/api/list')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching product transfers');
                        return $q.reject(errResponse);
                    }
                );
        },

        getOne: function(id){
            return $http.get(config.basePath + '/productTransfer/api/'+id)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while fetching productTransfer');
                        return $q.reject(errResponse);
                    }
                );
        },

        createProductTransfer: function(productTransfer){
            return $http.post(config.basePath + '/productTransfer/api/add', productTransfer)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating product transfer');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateProductTransfer: function(productTransfer, id){
            return $http.post(config.basePath + '/productTransfer/api/'+id+'/edit', productTransfer)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating product transfers');
                        return $q.reject(errResponse);
                    }
                );
        },

        saveProductTransfer: function(productTransfer){
            return $http.post(config.basePath + '/productTransfer/api/save', productTransfer)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while updating product transfer');
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteProductTransfer: function(id){
            return $http.delete(config.basePath + '/productTransfer/api/'+id+'/remove')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting product transfer');
                        return $q.reject(errResponse);
                    }
                );
        }

    };

}]);