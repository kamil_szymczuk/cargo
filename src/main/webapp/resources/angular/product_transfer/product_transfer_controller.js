'use strict';

App.controller('ProductTransferController', ['$scope', 'ProductTransferService', function($scope, ProductTransferService) {
    var self = this;

    self.productTransfer={
        id:null,
        positionDestination:null,
        positionSource:null,
        productTransferStatus: '',
        product:null,
        quantity:null,
        createdAt:''
    };

    self.selected={
        id:null,
        positionDestination:null,
        positionSource:null,
        productTransferStatus: '',
        product:null,
        quantity:null,
        createdAt:'',
        search:''
    };

    self.productTransferPageData={
        content: [],
        first: null,
        last: null,
        number: 0,
        numberOfElements: 20,
        size: 20,
        sort: null,
        totalElements: null,
        totalPages: null,
        search: ''
    };

    self.pagingOptions = {
        pageSizes: [10, 25, 50, 100],
        pageSize: '25',
        currentPage: 0,
        search: ''
    };

    self.productTransfers=[];
    self.typePageData = {};
    self.typeSearch = '';

    self.fetchAllProductTransfers = function(){
        self.productTransferPageData.search = self.pagingOptions.search;
        self.productTransferPageData.size = self.pagingOptions.pageSize;
        self.productTransferPageData.number = self.pagingOptions.currentPage;

        console.log("search: ",self.productTransferPageData.search);
        console.log(self.productTransferPageData);
        ProductTransferService.fetchPageOfProductTransfers(self.productTransferPageData)
            .then(
                function(d) {
                    self.productTransferPageData = d;
                    self.productTransfers = d.content;
                },
                function(errResponse){
                    console.error('Error while fetching Currencies');
                }
            );
    };

    self.getOne = function(id){
        productTransferService.getOne(id)
            .then(
                function(d) {
                    self.selected = d;
                    console.error(self.selected);
                },
                function(errResponse){
                    console.error('Error while fetching productTransfer.');
                }
            );
    };

    self.createProductTransfer = function(productTransfer){
        ProductTransferService.createProductTransfer(productTransfer)
            .then(
                self.fetchAllProductTransfers,
                function(errResponse){
                    console.error('Error while creating Product Transfer.');
                }
            );
    };

    self.updateProductTransfer = function(productTransfer, id){
        ProductTransferService.updateProductTransfer(productTransfer, id)
            .then(
                self.fetchAllProductTransfers,
                function(errResponse){
                    console.error('Error while updating Product Transfer.');
                }
            );
    };

    self.saveProductTransfer = function(productTransfer, id){
        ProductTransferService.saveProductTransfer(productTransfer, id)
            .then(
                self.fetchAllProductTransfers,
                function(errResponse){
                    console.error('Error while updating Product Transfer.');
                }
            );
    };

    self.deleteProductTransfer = function(id){
        ProductTransferService.deleteProductTransfer(id)
            .then(
                self.fetchAllProductTransfers,
                function(errResponse){
                    console.error('Error while deleting Product Transfer.');
                }
            );
    };

    self.submit = function() {
        if(self.productTransfer.id===null){
            console.log('Saving New Product Transfer', self.productTransfer);
            self.createProductTransfer(self.productTransfer);
        }else{
            self.saveProductTransfer(self.productTransfer);
            //self.updateProductTransfer(self.productTransfer, self.productTransfer.id);
            console.log('Product Transfer updated with id ', self.productTransfer.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.productTransfers.length; i++){
            if(self.productTransfers[i].id === id) {
                self.productTransfer = angular.copy(self.productTransfers[i]);
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        if(self.productTransfer.id === id) {//clean form if the product transfer to be deleted is shown there.
            self.reset();
        }
        self.deleteProductTransfer(id);
    };


    self.reset = function(){
        self.productTransfer={id:null, positionDestination:'', positionSource:'', quantity:'', productTransferStatus:''};
        $scope.myForm.$setPristine(); //reset Form
    };

    $scope.setPage = function (pageno) { // This would fetch the data on page change.
        self.pagingOptions.currentPage = pageno - 1;
        self.fetchAllProductTransfers();
    };

    $scope.prevPage = function () {
        if (self.pagingOptions.currentPage > 0) {
            self.pagingOptions.currentPage--;
            self.fetchAllProductTransfers();
        }
    };

    $scope.nextPage = function () {
        if (self.pagingOptions.currentPage < self.productTransferPageData.totalPages - 1) {
            self.pagingOptions.currentPage++;
            self.fetchAllProductTransfers();
        }
    };

    $scope.sort_by = function (newSortingOrder) {
        if ($scope.sortingOrder == newSortingOrder)
            $scope.reverse = !$scope.reverse;

        $scope.sortingOrder = newSortingOrder;

    };

    self.fetchAllProductTransfers();

}]);