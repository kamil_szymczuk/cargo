'use strict';

/* App Module */


var App = angular.module('cargoApp', [
        'ngCookies',
        'ngRoute',
        'angularUtils.directives.dirPagination'
    ]).value('config', {
        basePath: '/cargo' // Set your base path here
    });

App.controller('StateCtrl', function ($scope) {

    $scope.typeSearchChangeHandler = function typeSearchChangeHandler(val) {
        // set model
        console.log('typeSearchChangeHandler ', val);
        // $scope.chosenSearchTerm = val;
    }
});
