'use strict';

App.factory('PositionService', ['$http', '$q', 'config', function($http, $q, config){

    self.pageRequest={};

    return {

        fetchPageOfPositions: function(pageRequest) {

            self.pageRequest.size = pageRequest.size;
            self.pageRequest.page = pageRequest.number;
            self.pageRequest.search = pageRequest.search;


            return $http.post(config.basePath + '/position/api/list',self.pageRequest)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching positions');
                        return $q.reject(errResponse);
                    }
                );
        },
        fetchAllPositions: function() {
            return $http.get(config.basePath + '/position/api/list')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching positions');
                        return $q.reject(errResponse);
                    }
                );
        },

        getOne: function(id){
            return $http.get(config.basePath + '/position/api/'+id)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while fetching position');
                        return $q.reject(errResponse);
                    }
                );
        },

        createPosition: function(position){
            return $http.post(config.basePath + '/position/api/add', position)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating position');
                        return $q.reject(errResponse);
                    }
                );
        },

        updatePosition: function(position, id){
            return $http.post(config.basePath + '/position/api/'+id+'/edit', position)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating position');
                        return $q.reject(errResponse);
                    }
                );
        },

        savePosition: function(position){
            return $http.post(config.basePath + '/position/api/save', position)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while updating position');
                        return $q.reject(errResponse);
                    }
                );
        },


        deletePosition: function(id){
            return $http.delete(config.basePath + '/position/api/'+id+'/remove')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting position');
                        return $q.reject(errResponse);
                    }
                );
        }

    };

}]);