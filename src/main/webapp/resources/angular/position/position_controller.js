'use strict';

App.controller('PositionController', ['$scope', 'PositionService', function ($scope, PositionService) {
    var self = this;

    self.position = {
        id: null,
        name: '',
        quantity: null,
        product: null,
        lane: null
    };

    self.selected = {
        id: null,
        name: '',
        quantity: null,
        product: null,
        lane: null
    };

    self.positionPageData = {
        content: [],
        first: null,
        last: null,
        number: 0,
        numberOfElements: 20,
        size: 20,
        sort: null,
        totalElements: null,
        totalPages: null,
        search: ''
    };

    self.pagingOptions = {
        pageSizes: [10, 25, 50, 100],
        pageSize: '25',
        currentPage: 0,
        search: ''
    };
    
    self.positions = [];
    self.typePageData = {};
    self.typeSearch = '';

    self.fetchAll = function () {

        self.positionPageData.search = self.pagingOptions.search;
        self.positionPageData.size = self.pagingOptions.pageSize;
        self.positionPageData.number = self.pagingOptions.currentPage;
        self.positionPageData.sort = [
            {
                "direction": (self.reverse ? "DESC" :"ASC"),
                "property": self.sortingOrder
            }
        ];

        console.log("search: ", self.positionPageData.search);
        console.log(self.positionPageData);
        PositionService.fetchPageOfPositions(self.positionPageData)
            .then(
                function (d) {
                    self.positionPageData = d;
                    self.positions = d.content;
                },
                function (errResponse) {
                    console.error('Error while fetching Positions');
                }
            );
    };

    self.getOne = function (id) {
        PositionService.getOne(id)
            .then(
                function (d) {
                    self.selected = d;
                    console.error(self.selected);
                },
                function (errResponse) {
                    console.error('Error while fetching ProductType.');
                }
            );
    };

    self.createPosition = function (position) {
        PositionService.createPosition(position)
            .then(
                self.fetchAll,
                function (errResponse) {
                    console.error('Error while creating Position.');
                }
            );
    };

    self.updatePosition = function (position, id) {
        PositionService.updatePosition(position, id)
            .then(
                self.fetchAll,
                function (errResponse) {
                    console.error('Error while updating Position.');
                }
            );
    };

    self.savePosition = function (position, id) {
        PositionService.savePosition(position, id)
            .then(
                self.fetchAll,
                function (errResponse) {
                    console.error('Error while updating Position.');
                }
            );
    };
    
    self.increment = function (id, value) {
        PositionService.increment(id, value)
            .then(
                self.fetchAll,
                function (errResponse) {
                    console.error('Error add');
                }
            );
    };

    self.deletePosition = function (id) {
        PositionService.deletePosition(id)
            .then(
                self.fetchAll,
                function (errResponse) {
                    console.error('Error while deleting Position.');
                }
            );
    };

    self.submit = function () {
        if (self.position.id === null) {
            console.log('Saving New Position', self.position);
            self.createPosition(self.position);
        } else {
            self.savePosition(self.position);
            //self.updatePosition(self.position, self.position.id);
            console.log('Position updated with id ', self.position.id);
        }
        self.reset();
    };

    self.edit = function (id) {
        console.log('id to be edited', id);
        for (var i = 0; i < self.positions.length; i++) {
            if (self.positions[i].id === id) {
                self.position = angular.copy(self.positions[i]);
                break;
            }
        }
    };

    self.remove = function (id) {
        console.log('id to be deleted', id);
        if (self.position.id === id) {//clean form if the position to be deleted is shown there.
            self.reset();
        }
        self.deletePosition(id);
    };


    self.reset = function () {
        self.position = {id: null, name: '', description: '', lane_id: null, product_id: null};
        $scope.myForm.$setPristine(); //reset Form
    };


    $scope.setPage = function (pageno) { // This would fetch the data on page change.
        self.pagingOptions.currentPage = pageno - 1;
        self.fetchAll();
    };
    $scope.prevPage = function () {
        if (self.pagingOptions.currentPage > 0) {
            self.pagingOptions.currentPage--;
            self.fetchAll();
        }
    };

    $scope.nextPage = function () {
        if (self.pagingOptions.currentPage < self.positionPageData.totalPages - 1) {
            self.pagingOptions.currentPage++;
            self.fetchAll();
        }
    };

    $scope.sort_by = function (newSortingOrder) {
        if (self.sortingOrder == newSortingOrder)
            self.reverse = !self.reverse;

        self.sortingOrder = newSortingOrder;


        self.fetchAll();
        // icon setup
        $('th i').each(function () {
            //     icon reset
            $(this).removeClass().addClass('icon-sort');
        });
        if (self.reverse)
            $('th.' + newSortingOrder + ' i').removeClass().addClass('icon-chevron-up');
        else
            $('th.' + newSortingOrder + ' i').removeClass().addClass('icon-chevron-down');
    };

    self.fetchAll();


}]);