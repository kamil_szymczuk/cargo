'use strict';

App.factory('ReceptionService', ['$http', '$q', function($http, $q){

    self.pageRequest={};

    return {

        fetchPageOfReceptions: function(pageRequest) {

            self.pageRequest.size = pageRequest.size;
            self.pageRequest.page = pageRequest.number;
            self.pageRequest.search = pageRequest.search;


            return $http.post('/reception/api/list',self.pageRequest)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching receptions');
                        return $q.reject(errResponse);
                    }
                );
        },
        fetchAllReceptions: function() {
            return $http.get('/reception/api/list')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching receptions');
                        return $q.reject(errResponse);
                    }
                );
        },

        getOne: function(id){
            return $http.get('/reception/api/'+id)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while fetching reception');
                        return $q.reject(errResponse);
                    }
                );
        },
        createReception: function(reception){
            return $http.post('/reception/api/add', reception)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating reception');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateReception: function(reception, id){
            return $http.post('/reception/api/'+id+'/edit', reception)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating reception');
                        return $q.reject(errResponse);
                    }
                );
        },

        saveReception: function(reception){
            return $http.post('/reception/api/save', reception)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while updating reception');
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteReception: function(id){
            return $http.delete('/reception/api/'+id+'/remove')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting reception');
                        return $q.reject(errResponse);
                    }
                );
        }

    };

}]);