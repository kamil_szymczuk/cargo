'use strict';

App.controller('ReceptionController', ['$scope', 'ReceptionService', function($scope, ReceptionService) {
    var self = this;

    self.reception={
        id:null,
        customer:null,
        position:null,
        product:null,
        productType:null,
        quantity:null
    };

    self.selected={
        id:null,
        customer:null,
        position:null,
        product:null,
        productType:null,
        quantity:null
    };

    self.receptionPageData = {
        content: [],
        first: null,
        last: null,
        number: 0,
        numberOfElements: 20,
        size: 20,
        sort: null,
        totalElements: null,
        totalPages: null,
        search: ''
    };

    self.pagingOptions = {
        pageSizes: [10, 25, 50, 100],
        pageSize: '25',
        currentPage: 0,
        search: ''
    };

    self.receptions=[];
    self.typePageData = {};
    self.typeSearch = '';

    self.fetchAllReceptions = function(){
        self.receptionPageData.search = self.pagingOptions.search;
        self.receptionPageData.size = self.pagingOptions.pageSize;
        self.receptionPageData.number = self.pagingOptions.currentPage;


        console.log("search: ",self.receptionPageData.search);
        console.log(self.receptionPageData);
        ReceptionService.fetchPageOfReceptions(self.receptionPageData)
            .then(
                function(d) {
                    self.receptionPageData = d;
                    self.receptions = d.content;
                },
                function(errResponse){
                    console.error('Error while fetching Currencies');
                }
            );
    };

    self.getOne = function(id){
        ReceptionService.getOne(id)
            .then(
                function(d) {
                    self.selected = d;
                    console.error(self.selected);
                },
                function(errResponse){
                    console.error('Error while fetching Reception.');
                }
            );
    };

    self.createReception = function(reception){
        ReceptionService.createReception(reception)
            .then(
                self.fetchAllReceptions,
                function(errResponse){
                    console.error('Error while creating Reception.');
                }
            );
    };

    self.updateReception = function(reception, id){
        ReceptionService.updateReception(reception, id)
            .then(
                self.fetchAllReceptions,
                function(errResponse){
                    console.error('Error while updating Reception.');
                }
            );
    };

    self.saveReception = function(reception, id){
        ReceptionService.saveReception(reception, id)
            .then(
                self.fetchAllReceptions,
                function(errResponse){
                    console.error('Error while updating Reception.');
                }
            );
    };

    self.deleteReception = function(id){
        ReceptionService.deleteReception(id)
            .then(
                self.fetchAllReceptions,
                function(errResponse){
                    console.error('Error while deleting Reception.');
                }
            );
    };

    self.submit = function() {
        if(self.reception.id===null){
            console.log('Saving New Reception', self.reception);
            self.createReception(self.reception);
        }else{
            self.saveReception(self.reception);
            //self.updateReception(self.reception, self.reception.id);
            console.log('Reception updated with id ', self.reception.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.receptions.length; i++){
            if(self.receptions[i].id === id) {
                self.reception = angular.copy(self.receptions[i]);
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        if(self.reception.id === id) {//clean form if the reception to be deleted is shown there.
            self.reset();
        }
        self.deleteReception(id);
    };


    self.reset = function(){
        self.reception={id:null, name:'',description:''};
        $scope.myForm.$setPristine(); //reset Form
    };

    $scope.setPage = function (pageno) { // This would fetch the data on page change.
        self.pagingOptions.currentPage = pageno - 1;
        self.fetchAllReceptions();
    };

    $scope.prevPage = function () {
        if (self.pagingOptions.currentPage > 0) {
            self.pagingOptions.currentPage--;
            self.fetchAllReceptions();
        }
    };

    $scope.nextPage = function () {
        if (self.pagingOptions.currentPage < self.receptionPageData.totalPages - 1) {
            self.pagingOptions.currentPage++;
            self.fetchAllReceptions();
        }
    };

    $scope.sort_by = function (newSortingOrder) {
        if ($scope.sortingOrder == newSortingOrder)
            $scope.reverse = !$scope.reverse;

        $scope.sortingOrder = newSortingOrder;

    };

    //region init
    self.fetchAllReceptions();
    //endregion init


}])
//    .directive('chosen', function () {
//        var linker = function (scope, element, attr) {
//            // update the select when data is loaded
//            scope.$watch(attr.chosen, function (oldVal, newVal) {
//                console.log('update the select when data is loaded');
//                element.trigger('chosen:updated');
////                                element.chosen();
//
//            });
//
//            // update the select when the model changes
//            scope.$watch(attr.ngModel, function () {
//                console.log('update the select when the model changes');
//                console.log(self.productTypes);
//                element.trigger('chosen:updated');
//                element.chosen();
//            });
//
//            //
//            //var select, chosen;
//            //
//            //// cache the select element as we'll be using it a few times
//            //select = $("#productType");
//            //
//            //// init the chosen plugin
//            //select.chosen({no_results_text: 'Press Enter to add new entry:'});
//            //
//            //// get the chosen object
//            //chosen = select.data('chosen');
//
//            // Bind the keyup event to the search box input
//
////                            chosen.dropdown.find('input').on('keyup', function (e) {
////                                // if we hit Enter and the results list is empty (no matches) add the option
////                                if (e.which == 13 && chosen.dropdown.find('li.no-results').length > 0) {
////                                    var option = $("<option>").val(this.value).text(this.value);
////                                    scope.ctrl.typeSearch = this.value;
////                                    scope.ctrl.fetchAllProductTypes();
////
////                                    select.find(option).prop('selected', true);
////                                    select.trigger("chosen:updated");
////                                }
////                            });
//
//            element.chosen();
//        };
//
//        return {
//            restrict: 'A',
//            link: linker
//        }
//    });