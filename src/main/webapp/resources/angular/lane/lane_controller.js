'use strict';

App.controller('LaneController', ['$scope', 'LaneService', function($scope, LaneService) {
    var self = this;

    self.lane={
        id:null,
        name:'',
        //version:null,
        position:null,
        description:''
    };

    self.selected={
        id:null,
        name:'',
        position:null,
        //version:null,
        description:''
    };

    self.lanePageData = {
        content: [],
        first: null,
        last: null,
        number: 0,
        numberOfElements: 20,
        size: 20,
        sort: null,
        totalElements: null,
        totalPages: null,
        search: ''
    };

    self.pagingOptions = {
        pageSizes: [10, 25, 50, 100],
        pageSize: '25',
        currentPage: 0,
        search: ''
    };

    self.lanes=[];
    self.typePageData = {};
    self.typeSearch = '';

    self.fetchAllLanes = function(){
        self.lanePageData.search = self.pagingOptions.search;
        self.lanePageData.size = self.pagingOptions.pageSize;
        self.lanePageData.number = self.pagingOptions.currentPage;


        console.log("search: ",self.lanePageData.search);
        console.log(self.lanePageData);
        LaneService.fetchPageOfLanes(self.lanePageData)
            .then(
                function(d) {
                    self.lanePageData = d;
                    self.lanes = d.content;
                },
                function(errResponse){
                    console.error('Error while fetching Currencies');
                }
            );
    };

    self.getOne = function(id){
        LaneService.getOne(id)
            .then(
                function(d) {
                    self.selected = d;
                    console.error(self.selected);
                },
                function(errResponse){
                    console.error('Error while fetching Lane.');
                }
            );
    };

    self.createLane = function(lane){
        LaneService.createLane(lane)
            .then(
                self.fetchAllLanes,
                function(errResponse){
                    console.error('Error while creating Lane.');
                }
            );
    };

    self.updateLane = function(lane, id){
        LaneService.updateLane(lane, id)
            .then(
                self.fetchAllLanes,
                function(errResponse){
                    console.error('Error while updating Lane.');
                }
            );
    };

    self.saveLane = function(lane, id){
        LaneService.saveLane(lane, id)
            .then(
                self.fetchAllLanes,
                function(errResponse){
                    console.error('Error while updating Lane.');
                }
            );
    };

    self.deleteLane = function(id){
        LaneService.deleteLane(id)
            .then(
                self.fetchAllLanes,
                function(errResponse){
                    console.error('Error while deleting Lane.');
                }
            );
    };

    self.submit = function() {
        if(self.lane.id===null){
            console.log('Saving New Lane', self.lane);
            self.createLane(self.lane);
        }else{
            self.saveLane(self.lane);
            //self.updateLane(self.lane, self.lane.id);
            console.log('Lane updated with id ', self.lane.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.lanes.length; i++){
            if(self.lanes[i].id === id) {
                self.lane = angular.copy(self.lanes[i]);
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        if(self.lane.id === id) {//clean form if the lane to be deleted is shown there.
            self.reset();
        }
        self.deleteLane(id);
    };


    self.reset = function(){
        self.lane={id:null, name:'',description:''};
        $scope.myForm.$setPristine(); //reset Form
    };

    $scope.setPage = function (pageno) { // This would fetch the data on page change.
        self.pagingOptions.currentPage = pageno - 1;
        self.fetchAllLanes();
    };

    $scope.prevPage = function () {
        if (self.pagingOptions.currentPage > 0) {
            self.pagingOptions.currentPage--;
            self.fetchAllLanes();
        }
    };

    $scope.nextPage = function () {
        if (self.pagingOptions.currentPage < self.lanePageData.totalPages - 1) {
            self.pagingOptions.currentPage++;
            self.fetchAllLanes();
        }
    };

    $scope.sort_by = function (newSortingOrder) {
        if ($scope.sortingOrder == newSortingOrder)
            $scope.reverse = !$scope.reverse;

        $scope.sortingOrder = newSortingOrder;

    };

    //region init
    self.fetchAllLanes();
    //endregion init


}])
//    .directive('chosen', function () {
//        var linker = function (scope, element, attr) {
//            // update the select when data is loaded
//            scope.$watch(attr.chosen, function (oldVal, newVal) {
//                console.log('update the select when data is loaded');
//                element.trigger('chosen:updated');
////                                element.chosen();
//
//            });
//
//            // update the select when the model changes
//            scope.$watch(attr.ngModel, function () {
//                console.log('update the select when the model changes');
//                console.log(self.productTypes);
//                element.trigger('chosen:updated');
//                element.chosen();
//            });
//
//            //
//            //var select, chosen;
//            //
//            //// cache the select element as we'll be using it a few times
//            //select = $("#productType");
//            //
//            //// init the chosen plugin
//            //select.chosen({no_results_text: 'Press Enter to add new entry:'});
//            //
//            //// get the chosen object
//            //chosen = select.data('chosen');
//
//            // Bind the keyup event to the search box input
//
////                            chosen.dropdown.find('input').on('keyup', function (e) {
////                                // if we hit Enter and the results list is empty (no matches) add the option
////                                if (e.which == 13 && chosen.dropdown.find('li.no-results').length > 0) {
////                                    var option = $("<option>").val(this.value).text(this.value);
////                                    scope.ctrl.typeSearch = this.value;
////                                    scope.ctrl.fetchAllProductTypes();
////
////                                    select.find(option).prop('selected', true);
////                                    select.trigger("chosen:updated");
////                                }
////                            });
//
//            element.chosen();
//        };
//
//        return {
//            restrict: 'A',
//            link: linker
//        }
//    });