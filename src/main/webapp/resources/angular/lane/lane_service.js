'use strict';

App.factory('LaneService', ['$http', '$q', 'config', function($http, $q, config){

    self.pageRequest={};

    return {

        fetchPageOfLanes: function(pageRequest) {

            self.pageRequest.size = pageRequest.size;
            self.pageRequest.page = pageRequest.number;
            self.pageRequest.search = pageRequest.search;


            return $http.post(config.basePath + '/lane/api/list',self.pageRequest)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching lanes');
                        return $q.reject(errResponse);
                    }
                );
        },
        fetchAllLanes: function() {
            return $http.get(config.basePath + '/lane/api/list')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching lanes');
                        return $q.reject(errResponse);
                    }
                );
        },

        getOne: function(id){
            return $http.get(config.basePath + '/lane/api/'+id)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while fetching lane');
                        return $q.reject(errResponse);
                    }
                );
        },

        createLane: function(lane){
            return $http.post(config.basePath + '/lane/api/add', lane)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating lane');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateLane: function(lane, id){
            return $http.post(config.basePath + '/lane/api/'+id+'/edit', lane)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating lane');
                        return $q.reject(errResponse);
                    }
                );
        },

        saveLane: function(lane){
            return $http.post(config.basePath + '/lane/api/save', lane)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while updating lane');
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteLane: function(id){
            return $http.delete(config.basePath + '/lane/api/'+id+'remove')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting lane');
                        return $q.reject(errResponse);
                    }
                );
        }

    };

}]);