'use strict';

App.controller('UserController', ['$scope', 'UserService', function($scope, UserService) {
    var self = this;

    self.user={
        id:null,
        //version:null,
        code:'',
        costAccount:'',
        financeAccount:'',
        firstName:'',
        lastName:'',
        username:'',
        password: '',
        email:'',
        //enabled:true,
        phoneNumber:'',
        registrationNumber:'',
        faxNumber:''

    };

    self.selected={
        id:null,
        //version:null,
        code:'',
        costAccount:'',
        financeAccount:'',
        firstName:'',
        lastName:'',
        username:'',
        password: '',
        email:'',
        //enabled:true,
        phoneNumber:'',
        registrationNumber:'',
        faxNumber:''
    };

    self.userPageData = {
        content: [],
        first: null,
        last: null,
        number: 0,
        numberOfElements: 20,
        size: 20,
        sort: null,
        totalElements: null,
        totalPages: null,
        search: ''
    };

    self.pagingOptions = {
        pageSizes: [10, 25, 50, 100],
        pageSize: '25',
        currentPage: 0,
        search: ''
    };

    self.users = [];


    self.typePageData = {};
    self.typeSearch = '';

    self.fetchAllUsers = function(){
        self.userPageData.search = self.pagingOptions.search;
        self.userPageData.size = self.pagingOptions.pageSize;
        self.userPageData.number = self.pagingOptions.currentPage;

        console.log("search: ",self.userPageData.search);
        console.log(self.userPageData);
        UserService.fetchPageOfUsers(self.userPageData)
            .then(
                function(d) {
                    self.userPageData = d;
                    self.users = d.content;
                },
                function(errResponse){
                    console.error('Error while fetching Currencies');
                }
            );
    };

    self.getOne = function(id){
        UserService.getOne(id)
            .then(
                function(d) {
                    self.selected = d;
                    console.error(self.selected);
                },
                function(errResponse){
                    console.error('Error while fetching User.');
                }
            );
    };

    self.createUser = function(user){
        UserService.createUser(user)
            .then(
                self.fetchAllUsers,
                function(errResponse){
                    console.error('Error while creating User.');
                }
            );
    };

    self.updateUser = function(user, id){
        UserService.updateUser(user, id)
            .then(
                self.fetchAllUsers,
                function(errResponse){
                    console.error('Error while updating User.');
                }
            );
    };

    self.saveUser = function(user, id){
        UserService.saveUser(user, id)
            .then(
                self.fetchAllUsers,
                function(errResponse){
                    console.error('Error while updating User.');
                }
            );
    };

    self.deleteUser = function(id){
        UserService.deleteUser(id)
            .then(
                self.fetchAllUsers,
                function(errResponse){
                    console.error('Error while deleting User.');
                }
            );
    };

    self.submit = function() {
        if(self.user.id===null){
            console.log('Saving New User', self.user);
            self.createUser(self.user);
        }else{
            self.updateUser(self.user, self.user.id);
            console.log('User updated with id ', self.user.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.users.length; i++){
            if(self.users[i].id === id) {
                self.user = angular.copy(self.users[i]);

                self.fetchAllUserTypes();
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        if(self.user.id === id) {//clean form if the User to be deleted is shown there.
            self.reset();
        }
        self.deleteUser(id);
    };


    self.reset = function(){
        self.user={id:null, code:'', costAccount:'',financeAccount:'', firstName:'', lastName:'', username:'', password: '', email:'', phoneNumber:'',registrationNumber:'',faxNumber:''};
        $scope.myForm.$setPristine(); //reset Form
    };

    $scope.setPage = function (pageno) { // This would fetch the data on page change.
        self.pagingOptions.currentPage = pageno - 1;
        self.fetchAllUsers();
    };
    $scope.prevPage = function () {
        if (self.pagingOptions.currentPage > 0) {
            self.pagingOptions.currentPage--;
            self.fetchAllUsers();
        }
    };

    $scope.nextPage = function () {
        if (self.pagingOptions.currentPage < self.userPageData.totalPages - 1) {
            self.pagingOptions.currentPage++;
            self.fetchAllUsers();
        }
    };
    //$scope.setPage = function () {
    //    $scope.currentPage = this.n;
    //};
    //
    //// functions have been describe process the data for display
    //$scope.search();
    //
    //// change sorting order
    $scope.sort_by = function (newSortingOrder) {
        if ($scope.sortingOrder == newSortingOrder)
            $scope.reverse = !$scope.reverse;

        $scope.sortingOrder = newSortingOrder;

    };

    self.fetchAllUsers();

}]);
