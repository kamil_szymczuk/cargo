'use strict';

App.factory('UserService', ['$http', '$q', 'config', function($http, $q, config){

    self.pageRequest={};

    return {

        fetchPageOfUsers: function(pageRequest) {

            self.pageRequest.size = pageRequest.size;
            self.pageRequest.page = pageRequest.number;
            self.pageRequest.search = pageRequest.search;


            return $http.post(config.basePath + '/user/api/list',self.pageRequest)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching users');
                        return $q.reject(errResponse);
                    }
                );
        },

        fetchAllUsers: function() {
            return $http.get(config.basePath + '/user/api/list')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching users');
                        return $q.reject(errResponse);
                    }
                );
        },

        getOne: function(id){
            return $http.get(config.basePath + '/user/api/'+id)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while fetching user');
                        return $q.reject(errResponse);
                    }
                );
        },

        createUser: function(user){
            return $http.post(config.basePath + '/user/api/add', user)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating user');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateUser: function(user, id){
            return $http.post(config.basePath + '/user/api/'+id+'/edit', user)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating user');
                        return $q.reject(errResponse);
                    }
                );
        },

        saveUser: function(user){
            return $http.post(config.basePath + '/user/api/save', user)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while updating user');
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteUser: function(id){
            return $http.delete(config.basePath + '/user/api/'+id+'/remove')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting user');
                        return $q.reject(errResponse);
                    }
                );
        }

    };

}]);