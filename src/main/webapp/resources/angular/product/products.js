var App = angular.module('cargoApp', ['angularUtils.directives.dirPagination'])
    .factory('XSRFInterceptor', function ($cookies, $log) {
        var XSRFInterceptor = {

            request: function (config) {

                var token = $cookies.get('XSRF-TOKEN');

                if (token) {
                    config.headers['X-XSRF-TOKEN'] = token;
                    $log.info("X-XSRF-TOKEN: " + token);
                }

                return config;
            }
        };
        return XSRFInterceptor;
    }).controller('StateCtrl', function ($scope) {

        $scope.typeSearchChangeHandler = function typeSearchChangeHandler(val) {
            // set model
            console.log('typeSearchChangeHandler ', val);
        }
    })
    .directive('chosen', function () {
        var linker = function (scope, element, attr) {
            // update the select when data is loaded
            scope.$watch(attr.chosen, function (oldVal, newVal) {
                element.trigger('chosen:updated');
            });

            // update the select when the model changes
            scope.$watch(attr.ngModel, function () {
                element.trigger('chosen:updated');
            });

            element.chosen();
        };

        return {
            restrict: 'A',
            link: linker
        }
    });