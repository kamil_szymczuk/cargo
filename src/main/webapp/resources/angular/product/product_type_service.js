'use strict';

App.factory('ProductTypeService', ['$http', '$q','config', function ($http, $q, config) {

    self.pageRequest = {
        page: 0,
        size: 20,
        search: "",
        sort: null
    };

    return {

        fetchPageOfProductTypes: function (pageRequest) {

            self.pageRequest.size = pageRequest.size;
            self.pageRequest.page = pageRequest.number;
            self.pageRequest.search = pageRequest.search;

            return $http.post(config.basePath + '/productType/api/list', self.pageRequest)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching product types');
                        return $q.reject(errResponse);
                    }
                );
        },
        fetchAllProductTypes: function () {
            return $http.get(config.basePath + '/productType/api/list')
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching product typess');
                        return $q.reject(errResponse);
                    }
                );
        },
        getOne: function (id) {
            return $http.get(config.basePath + '/productType/api/' + id)
                .then(
                    function (response) {
                        return response;
                    },
                    function (errResponse) {
                        console.error('Error while fetching product type');
                        return $q.reject(errResponse);
                    }
                );
        },
        createProductType: function (productType) {
            return $http.post(config.basePath + '/productType/api/add', productType)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while creating product type');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateProductType: function (productType, id) {
            return $http.post(config.basePath + '/productType/api/' + id + '/edit', productType)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while updating product type');
                        return $q.reject(errResponse);
                    }
                );
        },

        saveProductType: function (productType) {
            return $http.post(config.basePath + '/productType/api/save', productType)
                .then(
                    function (response) {
                        return response;
                    },
                    function (errResponse) {
                        console.error('Error while updating product type');
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteProductType: function (id) {
            return $http.delete(config.basePath + '/productType/api/' + id + '/remove')
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while deleting product type');
                        return $q.reject(errResponse);
                    }
                );
        }

    };

}]);