'use strict';

App.factory('ProductService', ['$http','$q', 'config', function ($http, $q, config) {

    self.pageRequest = {
        page: 0,
        size: 20,
        sort: null,
        search: ''
    };

    return {

        fetchPageOfProducts: function (pageRequest) {

            self.pageRequest.size = pageRequest.size;
            self.pageRequest.page = pageRequest.number;
            self.pageRequest.search = pageRequest.search;
            self.pageRequest.sort = pageRequest.sort;


            // console.log(config.basePath);
            // console.log(config.basePath);
            
            return $http.post(config.basePath + '/product/api/list', self.pageRequest)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching products');
                        return $q.reject(errResponse);
                    }
                );
        },
        fetchAllProducts: function () {
            return $http.get(config.basePath + '/product/api/list')
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching products');
                        return $q.reject(errResponse);
                    }
                );
        },

        getOne: function (id) {
            return $http.get(config.basePath + '/product/api/' + id)
                .then(
                    function (response) {
                        return response;
                    },
                    function (errResponse) {
                        console.error('Error while fetching product');
                        return $q.reject(errResponse);
                    }
                );
        },
        createProduct: function (product) {
            return $http.post(config.basePath + '/product/api/add', product)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while creating product');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateProduct: function (product, id) {
            return $http.post(config.basePath + '/product/api/' + id + '/edit', product)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while updating product');
                        return $q.reject(errResponse);
                    }
                );
        },

        saveProduct: function (product) {
            return $http.post(config.basePath + '/product/api/save', product)
                .then(
                    function (response) {
                        return response;
                    },
                    function (errResponse) {
                        console.error('Error while updating product');
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteProduct: function (id) {
            return $http.delete(config.basePath + '/product/api/' + id + '/remove')
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while deleting product');
                        return $q.reject(errResponse);
                    }
                );
        }

    };

}]);