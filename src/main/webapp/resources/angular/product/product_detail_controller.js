'use strict';

App.controller('ProductDetailController', ['$scope', 'ProductService', 'ProductTypeService', function ($scope, ProductService, ProductTypeService) {
        var self = this;

        self.product = {
            id: null,
            subcode: '',
            description: '',
            weight: null,
            baseCode: '',
            productType: null
        };

        self.productType = {
            id: null,
            baseCode: '',
            name: ''
        };

        self.selected = {
            id: null,
            subcode: '',
            description: '',
            weight: null,
            baseCode: ''
        };

        self.pagingOptions = {
            pageSizes: [10, 25, 50, 100],
            pageSize: '10',
            currentPage: 0,
            search: ''
        };

        //self.products = [];


        self.typePageData = {
            content: [],
            first: null,
            last: null,
            number: 0,
            numberOfElements: 20,
            size: 20,
            sort: null,
            totalElements: null,
            totalPages: null,
            search: ''
        };

        self.productTypes = [];

        //region product

        self.getOne = function (id) {
            ProductService.getOne(id)
                .then(
                    function (d) {
                        self.selected = d;
                        console.error(self.selected);
                    },
                    function (errResponse) {
                        console.error('Error while fetching ProductType.');
                    }
                );
        };
        self.createProduct = function (product) {
            ProductService.createProduct(product)
                .then({},
                    //self.fetchAllProducts,
                    function (errResponse) {
                        console.error('Error while creating Product.');
                    }
                );
        };

        self.updateProduct = function (product, id) {
            ProductService.updateProduct(product, id)
                .then({},
                    //self.fetchAllProducts,
                    function (errResponse) {
                        console.error('Error while updating Product.');
                    }
                );
        };

        self.saveProduct = function (product, id) {
            ProductService.saveProduct(product, id)
                .then({},
                    //self.fetchAllProducts,
                    function (errResponse) {
                        console.error('Error while updating Product.');
                    }
                );
        };

        self.deleteProduct = function (id) {
            ProductService.deleteProduct(id)
                .then(
                    {},
                    //self.fetchAllProducts,
                    function (errResponse) {
                        console.error('Error while deleting Product.');
                    }
                );
        };

        self.submit = function () {
            if (self.product.id === null) {
                console.log('Saving New Product', self.product);
                self.createProduct(self.product);
            } else {
                self.saveProduct(self.product);
                //self.updateProduct(self.product, self.product.id);
                console.log('Product updated with id ', self.product.id);
            }
            self.reset();
        };

        self.edit = function (id) {
            console.log('id to be edited', id);
        };

        self.remove = function (id) {
            console.log('id to be deleted', id);
            if (self.product.id === id) {//clean form if the product to be deleted is shown there.
                self.reset();
            }
            self.deleteProduct(id);
        };


        self.reset = function () {
            self.product = {id: null, subcode: '', description: '', weight: null, productType: '', productType_id: ''};
            $scope.myForm.$setPristine(); //reset Form
        };

        $scope.setPage = function (pageno) { // This would fetch the data on page change.
            self.pagingOptions.currentPage = pageno - 1;
            self.fetchAllProductTypes();
        };
        $scope.prevPage = function () {
            if (self.pagingOptions.currentPage > 0) {
                self.pagingOptions.currentPage--;
                self.fetchAllProductTypes();
            }
        };

        $scope.nextPage = function () {
            if (self.pagingOptions.currentPage < self.typePageData.totalPages - 1) {
                self.pagingOptions.currentPage++;
                self.fetchAllProductTypes();
            }
        };

        $scope.sort_by = function (newSortingOrder) {
            if ($scope.sortingOrder == newSortingOrder)
                $scope.reverse = !$scope.reverse;

            $scope.sortingOrder = newSortingOrder;

        };

        //endregion product

        //region productType
        self.fetchAllProductTypes = function () {
            self.typePageData.search = self.product.baseCode;
            self.typePageData.size = self.pagingOptions.pageSize;
            self.typePageData.number = self.pagingOptions.currentPage;
            ProductTypeService.fetchPageOfProductTypes(self.typePageData)
                .then(
                    function (d) {
                        self.typePageData = d;
                        self.productTypes = d.content;
                        console.log('fetchAllProductTypes ', self.typePageData);
                        console.log('fetchAllProductTypes ', self.productTypes);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Currencies');
                    }
                );
        };
        $scope.productTypeChange = function (productType) {
            self.productType = productType;
            self.fetchAllProductTypes();
            console.log('productTypeChange');
            console.log(productType);
        };

        $scope.setType = function (productType) {
            self.productType = productType;
            self.product.productType = productType;
            self.product.baseCode = productType.baseCode;
            self.fetchAllProductTypes();
        };


        //endregion productType


        //region init
        self.fetchAllProductTypes();
        //endregion init


    }])
    .directive('chosen', function () {
        var linker = function (scope, element, attr) {
            // update the select when data is loaded
            scope.$watch(attr.chosen, function (oldVal, newVal) {
                console.log('update the select when data is loaded');
                element.trigger('chosen:updated');
                element.chosen();

            });

            // update the select when the model changes
            scope.$watch(attr.ngModel, function () {
                console.log('update the select when the model changes');
                console.log(self.productTypes);
                element.trigger('chosen:updated');
                element.chosen();
            });


            element.chosen();
        };

        return {
            restrict: 'A',
            link: linker
        }
    });

