'use strict';

App.controller('ProductTypeController', ['$scope', 'ProductTypeService', function($scope, ProductTypeService) {
    var self = this;

    self.productType={
        id:null,
        baseCode:'',
        name:''
    };

    self.selected={
        id:null,
        baseCode:'',
        name:''
    };

    self.pageData={
        content:[],
        first:null,
        last:null,
        number:0,
        numberOfElements:null,
        size:20,
        sort:null,
        totalElements:null,
        totalPages:null,
        search:""
    };

    self.productTypes=[];

    self.fetchAll = function(){
        console.error('fetchAll');
        console.error(self.pageData);
        ProductTypeService.fetchPageOfProductTypes(self.pageData)
            .then(
                function(d) {
                    self.pageData = d;
                    self.productTypes = d.content;
                },
                function(errResponse){
                    console.error('Error while fetching Currencies');
                }
            );
    };

    self.createProductType = function(productType){
        ProductTypeService.createProductType(productType)
            .then(
                self.fetchAll,
                function(errResponse){
                    console.error('Error while creating ProductType.');
                }
            );
    };


    self.getOne = function(id){
        ProductTypeService.getOne(id)
            .then(
                function(d) {
                    self.selected = d;
                    console.error(self.selected);
                },
                function(errResponse){
                    console.error('Error while fetching ProductType.');
                }
            );
    };

    self.updateProductType = function(productType, id){
        ProductTypeService.updateProductType(productType, id)
            .then(
                self.fetchAll,
                function(errResponse){
                    console.error('Error while updating ProductType.');
                }
            );
    };

    self.saveProductType = function(productType, id){
        ProductTypeService.saveProductType(productType, id)
            .then(
                self.fetchAll,
                function(errResponse){
                    console.error('Error while updating ProductType.');
                }
            );
    };

    self.deleteProductType = function(id){
        ProductTypeService.deleteProductType(id)
            .then(
                self.fetchAll,
                function(errResponse){
                    console.error('Error while deleting ProductType.');
                }
            );
    };

    self.fetchAll();
    //self.getOne(3);

    self.submit = function() {
        if(self.productType.id===null){
            console.log('Saving New ProductType', self.productType);
            self.createProductType(self.productType);
        }else{
            self.saveProductType(self.productType);
            //self.updateProduct(self.productType, self.productType.id);
            console.log('ProductType updated with id ', self.productType.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.productTypes.length; i++){
            if(self.productTypes[i].id === id) {
                self.productType = angular.copy(self.productTypes[i]);
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        if(self.productType.id === id) {//clean form if the product type to be deleted is shown there.
            self.reset();
        }
        self.deleteProductType(id);
    };


    self.reset = function(){
        self.productType={id:null, baseCode:'', name:''};
        $scope.myForm.$setPristine(); //reset Form
    };

    self.search = function(search){
        console.log(search);
        self.fetchAll();
    };

}]);