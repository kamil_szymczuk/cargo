'use strict';

App.controller('CustomerController', ['$scope', 'CustomerService', function($scope, CustomerService) {
    var self = this;

    self.customer={
        id:null,
        fullName:'',
        shortName:'',
        nipNumber:'',
        regonNumber:'',
        phoneNumber: '',
        email:'',
        notes:''};

    self.selected={
        id:null,
        fullName:'',
        shortName:'',
        nipNumber:'',
        regonNumber:'',
        phoneNumber: '',
        email:'',
        notes:'',
        search:''
    };

    self.customerPageData={
        content: [],
        first: null,
        last: null,
        number: 0,
        numberOfElements: 20,
        size: 20,
        sort: null,
        totalElements: null,
        totalPages: null,
        search: ''
    };

    self.pagingOptions = {
        pageSizes: [10, 25, 50, 100],
        pageSize: '25',
        currentPage: 0,
        search: ''
    };

    self.customers=[];
    self.typePageData = {};
    self.typeSearch = '';

    self.fetchAllCustomers = function(){
        self.customerPageData.search = self.pagingOptions.search;
        self.customerPageData.size = self.pagingOptions.pageSize;
        self.customerPageData.number = self.pagingOptions.currentPage;

        console.log("search: ",self.customerPageData.search);
        console.log(self.customerPageData);
        CustomerService.fetchPageOfCustomers(self.customerPageData)
            .then(
                function(d) {
                    self.customerPageData = d;
                    self.customers = d.content;
                },
                function(errResponse){
                    console.error('Error while fetching Currencies');
                }
            );
    };

    self.getOne = function(id){
        CustomerService.getOne(id)
            .then(
                function(d) {
                    self.selected = d;
                    console.error(self.selected);
                },
                function(errResponse){
                    console.error('Error while fetching Customer.');
                }
            );
    };

    self.createCustomer = function(customer){
        CustomerService.createCustomer(customer)
            .then(
                self.fetchAllCustomers,
                function(errResponse){
                    console.error('Error while creating Customer.');
                }
            );
    };

    self.updateCustomer = function(customer, id){
        CustomerService.updateCustomer(customer, id)
            .then(
                self.fetchAllCustomers,
                function(errResponse){
                    console.error('Error while updating Customer.');
                }
            );
    };

    self.saveCustomer = function(customer, id){
        CustomerService.saveProduct(customer, id)
            .then(
                self.fetchAllCustomers,
                function(errResponse){
                    console.error('Error while updating Customer.');
                }
            );
    };

    self.deleteCustomer = function(id){
        CustomerService.deleteCustomer(id)
            .then(
                self.fetchAllCustomers,
                function(errResponse){
                    console.error('Error while deleting Customer.');
                }
            );
    };

    self.submit = function() {
        if(self.customer.id===null){
            console.log('Saving New Customer', self.customer);
            self.createCustomer(self.customer);
        }else{
            self.saveCustomer(self.customer);
            //self.updateCustomer(self.customer, self.customer.id);
            console.log('Customer updated with id ', self.customer.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.customers.length; i++){
            if(self.customers[i].id === id) {
                self.customer = angular.copy(self.customers[i]);
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        if(self.customer.id === id) {//clean form if the customer to be deleted is shown there.
            self.reset();
        }
        self.deleteCustomer(id);
    };


    self.reset = function(){
        self.customer={id:null,name:'',shortName:'',phoneNumber: '',email:'', notes:''};
        $scope.myForm.$setPristine(); //reset Form
    };

    $scope.setPage = function (pageno) { // This would fetch the data on page change.
        self.pagingOptions.currentPage = pageno - 1;
        self.fetchAllCustomers();
    };

    $scope.prevPage = function () {
        if (self.pagingOptions.currentPage > 0) {
            self.pagingOptions.currentPage--;
            self.fetchAllCustomers();
        }
    };

    $scope.nextPage = function () {
        if (self.pagingOptions.currentPage < self.customerPageData.totalPages - 1) {
            self.pagingOptions.currentPage++;
            self.fetchAllCustomers();
        }
    };

    $scope.sort_by = function (newSortingOrder) {
        if ($scope.sortingOrder == newSortingOrder)
            $scope.reverse = !$scope.reverse;

        $scope.sortingOrder = newSortingOrder;

    };

    self.fetchAllCustomers();

}])

    //.directive('chosen', function () {
    //    var linker = function (scope, element, attr) {
    //        // update the select when data is loaded
    //        scope.$watch(attr.chosen, function (oldVal, newVal) {
    //            console.log('update the select when data is loaded');
    //            element.trigger('chosen:updated');
    //
    //        });
    //
    //        scope.$watch(attr.ngModel, function () {
    //            console.log('update the select when the model changes');
    //            console.log(self.productTypes);
    //            element.trigger('chosen:updated');
    //            element.chosen();
    //        });
    //
    //        element.chosen();
    //    };
    //
    //    return {
    //        restrict: 'A',
    //        link: linker
    //    }
    //});