'use strict';

App.factory('CustomerService', ['$http', '$q', 'config', function($http, $q, config){

    self.pageRequest={};

    return {

        fetchPageOfCustomers: function(pageRequest) {

            self.pageRequest.size = pageRequest.size;
            self.pageRequest.page = pageRequest.number;
            self.pageRequest.search = pageRequest.search;

            return $http.post(config.basePath + 'customer/api/list',self.pageRequest)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching customers');
                        return $q.reject(errResponse);
                    }
                );
        },
        fetchAllCustomers: function() {
            return $http.get(config.basePath + '/customer/api/list')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching customers');
                        return $q.reject(errResponse);
                    }
                );
        },

        getOne: function(id){
            return $http.get(config.basePath + '/customer/api/'+id)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while fetching product');
                        return $q.reject(errResponse);
                    }
                );
        },

        createCustomer: function(customer){
            return $http.post(config.basePath + '/customer/api/add', customer)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating customer');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateCustomer: function(customer, id){
            return $http.post(config.basePath + '/customer/api/'+id+'/edit', customer)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating customer');
                        return $q.reject(errResponse);
                    }
                );
        },

        saveCustomer: function(customer){
            return $http.post(config.basePath + '/customer/api/save', customer)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while updating customer');
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteCustomer: function(id){
            return $http.delete(config.basePath + '/customer/api/'+id+'remove')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting customer');
                        return $q.reject(errResponse);
                    }
                );
        }

    };

}]);