'use strict';

App.controller('IssueController', ['$scope', 'IssueService', function($scope, IssueService) {
    var self = this;

    self.issue={
        id:null,
        customer:null,
        issueNumber:'',
        issueStatus:'',
        departureDate:''
    };

    self.selected={
        id:null,
        customer:null,
        issueNumber:'',
        issueStatus:'',
        departureDate:'',
        search:''
    };


    self.issuePageData={
        content: [],
        first: null,
        last: null,
        number: 0,
        numberOfElements: 20,
        size: 20,
        sort: null,
        totalElements: null,
        totalPages: null,
        search: ''
    };

    self.pagingOptions = {
        pageSizes: [10, 25, 50, 100],
        pageSize: '25',
        currentPage: 0,
        search: ''
    };

    self.issues=[];
    self.typePageData = {};
    self.typeSearch = '';

    self.fetchAllIssues = function(){
        self.issuePageData.search = self.pagingOptions.search;
        self.issuePageData.size = self.pagingOptions.pageSize;
        self.issuePageData.number = self.pagingOptions.currentPage;


        console.log("search: ",self.issuePageData.search);
        console.log(self.issuePageData);
        IssueService.fetchPageOfIssues(self.issuePageData)
            .then(
                function(d) {
                    self.issuePageData = d;
                    self.issues = d.content;
                },
                function(errResponse){
                    console.error('Error while fetching Currencies');
                }
            );
    };

    self.getOne = function(id){
        IssueService.getOne(id)
            .then(
                function(d) {
                    self.selected = d;
                    console.error(self.selected);
                },
                function(errResponse){
                    console.error('Error while fetching Issue.');
                }
            );
    };

    self.createIssue = function(issue){
        IssueService.createIssue(issue)
            .then(
                self.fetchAllIssues,
                function(errResponse){
                    console.error('Error while creating Issue.');
                }
            );
    };

    self.updateIssue = function(issue, id){
        IssueService.updateIssue(issue, id)
            .then(
                self.fetchAllIssues,
                function(errResponse){
                    console.error('Error while updating Issue.');
                }
            );
    };

    self.saveIssue = function(issue, id){
        IssueService.saveIssue(issue, id)
            .then(
                self.fetchAllIssues,
                function(errResponse){
                    console.error('Error while updating Issue.');
                }
            );
    };

    self.deleteIssue = function(id){
        IssueService.deleteIssue(id)
            .then(
                self.fetchAllIssues,
                function(errResponse){
                    console.error('Error while deleting Issue.');
                }
            );
    };

    self.submit = function() {
        if(self.issue.id===null){
            console.log('Saving New issue', self.issue);
            self.createIssue(self.issue);
        }else{
            self.saveIssue(self.issue);
            //self.updateIssue(self.issue, self.issue.id);
            console.log('issue updated with id ', self.issue.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.issues.length; i++){
            if(self.issues[i].id === id) {
                self.issue = angular.copy(self.issues[i]);
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        if(self.issue.id === id) {//clean form if the issue to be deleted is shown there.
            self.reset();
        }
        self.deleteIssue(id);
    };


    self.reset = function(){
        self.issue={id:null, productId:null, productName:'', quantity:null, position:'', issueNumber:'', issueStatus:'', departureDate:''};
        $scope.myForm.$setPristine(); //reset Form
    };

    $scope.setPage = function (pageno) { // This would fetch the data on page change.
        self.pagingOptions.currentPage = pageno - 1;
        self.fetchAllIssues();

    };
    $scope.prevPage = function () {
        if (self.pagingOptions.currentPage > 0) {
            self.pagingOptions.currentPage--;
            self.fetchAllIssues();
        }
    };

    $scope.nextPage = function () {
        if (self.pagingOptions.currentPage < self.issuePageData.totalPages - 1) {
            self.pagingOptions.currentPage++;
            self.fetchAllIssues();
        }
    };

    $scope.sort_by = function (newSortingOrder) {
        if ($scope.sortingOrder == newSortingOrder)
            $scope.reverse = !$scope.reverse;

        $scope.sortingOrder = newSortingOrder;

    };

    //region init
    self.fetchAllIssues();
    //endregion init


}])

//    .directive('chosen', function () {
//        var linker = function (scope, element, attr) {
//            // update the select when data is loaded
//            scope.$watch(attr.chosen, function (oldVal, newVal) {
//                console.log('update the select when data is loaded');
//                element.trigger('chosen:updated');
////                                element.chosen();
//
//            });
//
//            //// update the select when the model changes
//            //scope.$watch(attr.ngModel, function () {
//            //    console.log('update the select when the model changes');
//            //    console.log(self.productTypes);
//            //    element.trigger('chosen:updated');
//            //    element.chosen();
//            //});
//
//            element.chosen();
//        };
//
//        return {
//            restrict: 'A',
//            link: linker
//        }
//    });