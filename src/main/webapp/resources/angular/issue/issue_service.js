'use strict';

App.factory('IssueService', ['$http', '$q', function($http, $q){

    self.pageRequest={};
    return {

        fetchPageOfIssues: function(pageRequest) {

            self.pageRequest.size = pageRequest.size;
            self.pageRequest.page = pageRequest.number;
            self.pageRequest.search = pageRequest.search;

            return $http.post('/issue/api/list', self.pageRequest)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching issues');
                        return $q.reject(errResponse);
                    }
                );
        },
        fetchAllIssues: function() {
            return $http.get('/issue/api/list')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching issues');
                        return $q.reject(errResponse);
                    }
                );
        },

        getOne: function(id){
            return $http.get('/issue/api/'+id)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while fetching issue');
                        return $q.reject(errResponse);
                    }
                );
        },

        createIssue: function(issue){
            return $http.post('/issue/api/add', issue)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating issue');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateIssue: function(issue, id){
            return $http.post('/issue/api/'+id+'/edit'+id, issue)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating issue');
                        return $q.reject(errResponse);
                    }
                );
        },

        saveIssue: function(issue){
            return $http.post('/issue/api/save', issue)
                .then(
                    function(response){
                        return response;
                    },
                    function(errResponse){
                        console.error('Error while updating issue');
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteIssue: function(id){
            return $http.delete('/issue/api/'+id+'/remove')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting issue');
                        return $q.reject(errResponse);
                    }
                );
        }

    };

}]);