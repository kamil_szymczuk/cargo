#CUSTOMER
INSERT INTO customer (`active`, `version`, `email`, `full_name`, `nip_number`, `notes`, `phone_number`, `regon_number`, `short_name`) VALUES (1, 0, 'adam.mickiewicz@gmail.com', 'adamm', '123-45-67-819', 'specjalny', '0857307995', '732065814', 'am');
INSERT INTO customer (`active`, `version`, `email`, `full_name`, `nip_number`, `notes`, `phone_number`, `regon_number`, `short_name`) VALUES (1, 0, 'jan.mateusz@gmail.com', 'Janm', '321-54-76-818', 'zwykly', '0857335955', '732062696', 'jm');
INSERT INTO customer (`active`, `version`, `email`, `full_name`, `nip_number`, `notes`, `phone_number`, `regon_number`, `short_name`) VALUES (1, 0, 'jan.kowalski@wp.com', 'jank', '132-42-17-869', 'specjalny', '0857309021', '732075423', 'jk');
INSERT INTO customer (`active`, `version`, `email`, `full_name`, `nip_number`, `notes`, `phone_number`, `regon_number`, `short_name`) VALUES (1, 0, 'zenon.martyniuk@gmail.com', 'zenonm', '333-55-78-123', 'klient specjalny', '4447225955', '731022491', 'zm');
INSERT INTO customer (`active`, `version`, `email`, `full_name`, `nip_number`, `notes`, `phone_number`, `regon_number`, `short_name`) VALUES (1, 0, 'pawel.nowak@gmail.com', 'paweln', '111-22-33-222', 'dostawca', '4447255951', '731068641', 'pn');

#LANE
INSERT INTO lane(version, name, short_name) VALUES (0, 'Alejka A','A'), (0, 'Alejka B','B'), (0, 'Alejka C','C'), (0, 'Alejka G','G');




#USER
INSERT INTO users
  (version, code, email, enabled, fax_number, first_name, last_name, password, phone_number, username) VALUES
  (0, '0', '', 1, '', 'Ihh', 'Developer', '0ee496089d6cfa39b352a10350794c382bd556d3dea823386d634fbdb353716c', '', 'ihhdev');
#login: ihhdev
#pass: ihhpass