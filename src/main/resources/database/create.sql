create database cargodb character set=utf8 collate=utf8_polish_ci;
create database cargodb_tmp character set=utf8 collate=utf8_polish_ci;
create user 'cargouser'@'%' identified by 'cargopass';
create user 'cargouser'@'localhost' identified by 'cargopass';
grant all on cargodb.* to 'cargouser'@'%';
grant all on cargodb_tmp.* to 'cargouser'@'%';