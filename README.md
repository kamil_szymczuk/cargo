Uruchomienie
------------
1. Sciągamy i instalujemy JDK Java8 
2. Ściągamy STSa
    * należy zwrócić uwagę czy pobrana wersja jest zgodna z wersją java (tu: 32 czy 64 bit)

Opis
---
1. Bazy danych towarów, lokalizacji, kontrahentów (dodawanie, wyświetlanie, edycja, usuwanie)

2. Przyjęcie towaru
Użytkownik rejestruje przyjęcie towaru z zewnątrz i przenosi go do wybranej lokalizacji.
Lokalizacja jest dostępna tylko dla jednego towaru (inny towar nie może być składowany w wybranej lokalizacji, do momentu jego "wybrania" z lokacji).

3. Wydanie towaru
Użytkownik wybiera towar, który ma opuścić magazyn.

    Generowany jest "list pakowy",
    Użytkownik drukuje list i przekazuje go magazynierowi.
    Magazynier lokalizuje towar i go wydaje.
    Użytkownik drukuje wz'tkę.
    Towar w lokalizacji zostaje pomniejszony o ilość wydanego towaru.

4. Blokada/rezerwacja
Klient rezerwuje towar.
Do czasu zwolnienia rezerwacji nie można wyjąć z lokalizacji zablokowanej ilości towaru.
Rezerwacja powinna być łatwo zmieniana na "list pakowy".

5. Import z excela:

    towarów
    lokalizacji
    kontrahentów

6. Export do excela:

    towarów
    lokalizacji
    lokalizacji wraz ze stanem towarów
    kontrahentów

7. Generowanie + drukowanie

    dokumentów wz
    listów pakowych

8. Cennik (punkt do przemyślenia)

Opis od klienta
---------------
Magazyn podzielony jest na sektory/lokalizacje oznaczone wg alejek A1 A2 A3 … B1 B2 B3 … C1 C2 C3…

Ideą programu jest to że wpisujemy  numer artykułu przy wprowadzaniu  a program automatycznie przypisuje wolną lokalizację informując którą przypisał i wprowadza na stan

Wprowadzamy program z ceną a on wylicza średnią ważoną
W przypadku gdy jest to nowa referencja program prosi o stworzenie kartoteki:
Kartotek zawiera następujące pozycje:
Numer oe ( po którym wpisujemy)
Symbol      ( jest to charakterystyczny numer po którym można filtrować różne  numery oe czyli kilka numerów oe może mieć ten sam symbol)
Nazwa
Waga
Zamienniki po których można filtrować
3 grupy cenowe sprzedaży

W przypadku zdjęcia wszystkich sztuk danego numeru oe program automatycznie zwalnia lokalizację


Wymagania:

1. Łatwy w obsłudze export do i import_ z exela numerów nazw symboli wag
2. Możliwość tworzenia rezerwacji z blokowaniem towaru i z przypisaną ceną narzuconą grupą cenową
3. Wydruk wz-ek do pakowania z lokalizacjami, ilością danej referencji, oraz wagą całkowitą
4. Filtrowanie stanu magazynowego do exela
5. Możliwość filtrowania statystyk obroty towarów, rotacja towarów, , / za dany okres
6. wydanie zewnętrzne, przyjęcie zewnętrzne
7. Możliwość ręcznej zmiany lokalizacji towaru
8. Program musi być zdalny, umieszczony na serwerze zewnętrznym, możliwość logowania z dowolnej jednostki po zainstalowaniu oprogramowania , działający on-line
9. Jeśli jest możliwość dorzucić oprócz tego klientów i automatycznie ich historię i przypisać im grupę cenową to by było super